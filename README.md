# Dreamcast Image Builder

This repository hosts a program that builds modified GDI or CDI images of Dreamcast games using patches (mods). See [the wiki](https://gitlab.com/PiKeyAr/dreamcast-image-builder/-/wikis/home) for more information on how to use the program or make mods for it.

The user interface of this tool is inspired by and built to resemble MainMemory's mod managers for Sonic games.

**PREREQUISITES**

- Windows 10 or later (x64). Windows 7 and 8.1 may or may not work.
- [.NET 8.0 Windows Desktop runtime](https://aka.ms/dotnet/8.0/windowsdesktop-runtime-win-x64.exe).
- GDI dump of the game you want to modify. TOSEC dumps are recommended. Redump CUE may work. CDI or CHD won't work. 
- (Optional) A Dreamcast emulator such as [flycast](https://flyinghead.github.io/flycast-builds/).

**DOWNLOAD**

[Download the latest version of the builder](https://gitlab.com/PiKeyAr/dreamcast-image-builder/-/raw/main/release/DreamcastImageBuilder.exe?ref_type=heads&inline=false)

**TODO LIST**

[See here](https://gitlab.com/PiKeyAr/dreamcast-image-builder/-/wikis/TODO)

**CREDITS**

BUILT-IN MODS
- woofmute: Mods for Rez and Rez prototype, Sonic Adventure analog drift fix
- OnVar: Sonic Adventure 2 analog drift fix.
- Exant: Several Sonic Adventure mods.
- SoloSlacker: Sonic Adventure Expanded Windy Valley mod.

TOOLS AND LIBRARIES
- [GDIbuilder by Sappharad](https://github.com/Sappharad/GDIbuilder)
- [DiscUtils](https://github.com/DiscUtils/DiscUtils) fork from [Sappharad's GDIbuilder](https://github.com/Sappharad/GDIbuilder)
- [PRS by Soleil Rojas](https://github.com/Solybum/PRS)
- [ini-parser by Ricardo Amores Hernandez](https://github.com/rickyah/ini-parser)
- [mkisofs from the cdrtools project](https://sourceforge.net/projects/cdrtools/)
- [cdi4dc by SiZiOUS](https://github.com/sizious/img4dc/tree/master)
- mkcdi from [LazyBoot by Lin](https://www.emu-land.net/forum/index.php?topic=76817.0) used for reference for CDI generation
- [Universal Dreamcast Patcher by Derek Pascarella](https://github.com/DerekPascarella/UniversalDreamcastPatcher)  used for reference to add compatibility for its DCP format
- [Xdelta 3.0.11 by Joshua MacDonald](https://github.com/jmacd/xdelta-gpl)