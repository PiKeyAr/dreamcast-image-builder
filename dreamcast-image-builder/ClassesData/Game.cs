﻿using System.Collections.Generic;
using System.IO;
using System.Drawing;
using IniParser;
using IniParser.Model;
using System.Text;
using DiscUtils.Gdrom;
using CueSharp;
using DreamcastImageBuilder.ClassesHelpers;

namespace DreamcastImageBuilder
{
    public class Game
    {
        public string Name; // Game title for patcher UI
        public string IniName; // Name of the INI file
        public string Location; // Location of the game folder
        public bool BuiltIn; // True if the game comes with the program
        public Icon Icon; // Game icon for patcher UI
        public string GameTitle; // Product ID in IP.BIN 0x40-0x5F, used for mod compatibility check
        public List<PatchData> PatchesCDI; // 1ST_READ.BIN and other binary patches for making bootable CDI
        public IniData Codes; // Cheat codes in flycast/RetroArch .cht format
        public int NumTracks; // Number of tracks (first line in the GDI file)
        public int Disc; // Game Disc ID in IP.BIN 0x2B, used for mod compatibility check
        public int BootLBA; // LBA to boot CDI

        public Game(string filename)
        {
            BuiltIn = false; // Can be overridden in MainForm
            IniName = Path.GetFileNameWithoutExtension(filename);
            Location = Path.GetDirectoryName(filename);
            string[] text = File.ReadAllLines(filename);
            if (File.Exists(Path.ChangeExtension(filename, ".ico")))
                Icon = new Icon(Path.ChangeExtension(filename, ".ico"));
            if (File.Exists(Path.ChangeExtension(filename, ".cht")))
                Codes = new FileIniDataParser().ReadFile(Path.ChangeExtension(filename, ".cht"));
            Name = text[0];
            GameTitle = text[1].Replace("\"", "");
            NumTracks = 3;
            BootLBA = -1;
            if (text.Length > 3)
            {
                if (int.TryParse(text[3], out BootLBA) == false)
                    BootLBA = -1;
                if (text.Length > 4)
                {
                    PatchesCDI = new();
                    for (int i = 4; i < text.Length; i++)
                    {
                        if (text[i].Length > 5 && text[i][0] != ';')
                            PatchesCDI.Add(new PatchData(text[i], Path.GetDirectoryName(filename)));
                    }
                }
            }
        }

        // Get game title from path
        public static string GetGameName(string path)
        {
            string gdi = GDITools.GetGDIPath(path);
            GDReader gdReader;
            if (Path.GetExtension(gdi).ToLowerInvariant() == ".cue")
                gdReader = GDReader.FromCueSheet(gdi);
            else
                gdReader = GDReader.FromGDIfile(gdi);
            byte[] ipbin = ((MemoryStream)gdReader.ReadIPBin()).ToArray();
            gdReader.Dispose();
            return Encoding.ASCII.GetString(ipbin, 0x80, 128).TrimEnd();
        }

        // Get game version from path
        public static string GetGameVersion(string path)
        {
            if (!Path.Exists(path))
                return "ERROR: FILE NOT FOUND";
            string gdi = GDITools.GetGDIPath(path);
            GDReader gdReader;
            if (Path.GetExtension(gdi).ToLowerInvariant() == ".cue")
                gdReader = GDReader.FromCueSheet(gdi);
            else
                gdReader = GDReader.FromGDIfile(gdi);
            byte[] ipbin = ((MemoryStream)gdReader.ReadIPBin()).ToArray();
            gdReader.Dispose();
            return Encoding.ASCII.GetString(ipbin, 0x40, 32).TrimEnd();
        }

        // Get game disc ID from path
        public static int GetGameDisc(string path)
        {
            string gdi = GDITools.GetGDIPath(path);
            GDReader gdReader;
            if (Path.GetExtension(gdi).ToLowerInvariant() == ".cue")
                gdReader = GDReader.FromCueSheet(gdi);
            else
                gdReader = GDReader.FromGDIfile(gdi);
            byte[] ipbin = ((MemoryStream)gdReader.ReadIPBin()).ToArray();
            gdReader.Dispose();
            if (int.TryParse(Encoding.ASCII.GetString(ipbin, 0x2B, 1), out int res2))
                return res2;
            else
                return -1;
        }

        // Get the number of tracks from the GDI file
        public static int GetNumTracksFromGDI(string gdifile)
        {
            // CUE
            if (Path.GetExtension(gdifile).ToLowerInvariant() == ".cue")
            {
                CueSheet sheet = new CueSheet(gdifile);
                return sheet.Tracks.Length;
            }
            // GDI
            else
            {
                GDITools.GDIFile file = new GDITools.GDIFile(gdifile, false);
                return file.Tracks.Count;
            }
        }
    }

    public class GameConfig
    {
        public string GameName;
        public string IniName;
        public string InputPath;
        public string OutputPath;
        public bool BuildCDI;
        public string[] Mods;
        public string[] Codes;
    }
}