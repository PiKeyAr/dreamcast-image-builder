﻿using IniParser;
using IniParser.Model;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System;
using System.Text;

namespace DreamcastImageBuilder
{
    public class Mod
    {
        public string Name; // Mod title (UI)
        public string Author; // Developer's name (UI)
        public string Version; // Mod version (UI)
        public string Description; // Mod description (UI)
        public string ModPath; // Folder where mod.ini is located
        public bool BuiltIn; // Indicates that the mod does not come from the external mods folder

        public IniData ModIni; // mod.ini as IniData
        public string[] GameVersions; // Compatible game version strings
        public string[] FileReplacements; // Files and folders in the mod's 'gdroot' folder; copied to disc root
        public string[] Xdeltas; // Files in the mod's 'xdelta' folder, applied before all other mods as xdelta patches
        public string[] IncludeFiles; // Files in the mod's 'include' folder; copied to disc root without folder structure
        public string[] DeleteFiles; // Files and folders to be deleted on disc
        public string[] RequiredCodes; // Codes required to be enabled by the mod
        public List<PatchData> Patches; // All file patches
        public List<string> SubMods; // Paths to submods' INI files
        public List<string> DefaultSubMods; // Names of submods active by default
        public List<int> Discs; // List of compatible game disc IDs (all discs if null)

        public enum ModResult
        {
            Success,
            Error
        }

        public static string[] GetCompatibleGameVersions(string filename)
        {
            string[] lines = File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                string[] linesplit = line.Split('=');
                if (linesplit == null || linesplit.Length <= 1)
                    continue;
                switch (linesplit[0].ToLowerInvariant())
                {
                    case "autodemo":
                        return new string[1] { (linesplit[1].ToLowerInvariant() == "true") ? "610-7029  V1.00019981016" : "MK-51000  V1.00519991005" };
                    case "gameversions":
                        return linesplit[1].Split(',');
                }
            }
            return null;
        }

        public Mod(string inifile, bool buildIn = false)
        {
            BuiltIn = buildIn;
            Patches = new List<PatchData>();
            ModPath = Path.GetFullPath(Path.GetDirectoryName(inifile));
            var ini = new FileIniDataParser();
            ModIni = ini.ReadFile(inifile);
            Name = ModIni.Global["Name"];
            Author = ModIni.Global["Author"];
            Version = ModIni.Global["Version"];
            Description = ModIni.Global["Description"];
            if (ModIni.Global["Discs"] != null)
            {
                Discs = new List<int>();
                string[] disclist = ModIni.Global["Discs"].Split(',');
                foreach (string dis in disclist)
                {
                    if (int.TryParse(dis, out int res))
                        Discs.Add(res);
                }
            }                    
            if (ModIni.Global["GameVersions"] != null)
            {
                GameVersions = ModIni.Global["GameVersions"].Split(',');
                for (int v = 0; v < GameVersions.Length; v++)
                {
                    GameVersions[v] = GameVersions[v].TrimEnd();
                }
            }
            else
            {
                GameVersions = new string[1];
                GameVersions[0] = "MK - 51000  V1.00519991005";
            }
            if (Directory.Exists(Path.Combine(ModPath, "gdroot")))
            {
                string[] filesfull = Directory.GetFiles(Path.Combine(ModPath, "gdroot"), "*.*", SearchOption.AllDirectories).ToArray();
                List<string> fileslist = new();
                List<string> deltaslist = new();
                foreach (string file in filesfull)
                {
                    if (Path.GetExtension(file).ToLowerInvariant() == ".xdelta")
                        deltaslist.Add(Path.GetRelativePath(Path.Combine(ModPath, "gdroot"), file));
                    else
                        fileslist.Add(Path.GetRelativePath(Path.Combine(ModPath, "gdroot"), file));
                }
                if (fileslist.Count > 0)
                    FileReplacements = fileslist.ToArray();
                if (deltaslist.Count > 0)
                    Xdeltas = deltaslist.ToArray();
            }
            if (ModIni.Global["RequiredCodes"] != null)
                RequiredCodes = ModIni.Global["RequiredCodes"].Split(',');
            // Include folders
            if (Directory.Exists(Path.Combine(ModPath, "include")))
                IncludeFiles = Directory.GetFiles(Path.Combine(ModPath, "include"), "*.*", SearchOption.AllDirectories).ToArray();
            else if (ModIni.Global["IncludeFolders"] != null)
            {
                List<string> inclFiles = new();
                foreach (string inclFolder in ModIni.Global["IncludeFolders"].Split(','))
                {
                    string[] files = Directory.GetFiles(Path.Combine(ModPath, inclFolder), "*.*", SearchOption.AllDirectories).ToArray();
                    inclFiles.AddRange(files);
                }
                IncludeFiles = inclFiles.ToArray();
            }
            // Deleted files
            if (ModIni.Global["DeleteFiles"] != null)
            {
                List<string> delFiles = new();
                foreach (string file in ModIni.Global["DeleteFiles"].Split(','))
                {
                    delFiles.Add(ClassesHelpers.Paths.NormalizePath(file));
                }
                DeleteFiles = delFiles.ToArray();
            }
            /*
            // Simple patches
            if (File.Exists(Path.Combine(ModPath, "patches.ini")))
            {
                string[] simplePatchers = File.ReadAllLines(Path.Combine(ModPath, "patches.ini"));
                foreach (string patch in simplePatchers)
                {
                    if (patch.Length > 5)
                        Patches.Add(new PatchData(patch));
                }
            }
            */
            // mod.ini patches
            foreach (SectionData section in ModIni.Sections)
            {
                foreach (KeyData key in section.Keys)
                {
                    bool multi = false;
                    string comment = (key.Comments != null && key.Comments.Count > 0 && key.Comments[0] != null) ? key.Comments[0].TrimStart() : "None";
                    PatchData.PatchType patchType = PatchData.PatchType.HexBytes;
                    string[] addrstrings = key.KeyName.Split(",");
                    int patchCount = 1;
                    // New format patch
                    if (addrstrings.Length > 1)
                    {
                        // Check how many patch types there are
                        string[] patchTypes = addrstrings[1].Split("/");
                        // Single format patch
                        if (patchTypes.Length < 2)
                        {
                            patchType = PatchData.ReadPatchType(addrstrings[1]);
                            if (addrstrings.Length > 2)
                                if (!int.TryParse(addrstrings[2], out patchCount))
                                    patchCount = 1;
                        }
                        // Multi format patch
                        else
                        {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append(comment + ","); // Description
                            stringBuilder.Append(ClassesHelpers.Paths.NormalizePath(section.SectionName) + ","); // Filename
                            stringBuilder.Append(addrstrings[0] + ","); // Address
                            stringBuilder.Append(addrstrings[1] + ","); // Types
                            stringBuilder.Append(key.Value); // Values
                            Patches.AddRange(PatchData.ParseMultiple(stringBuilder.ToString(), Path.GetDirectoryName(inifile), out string msg));
                            multi = true;
                        }
                    }
                    // Simple byte patch
                    else
                    {
                        addrstrings = new string[1];
                        addrstrings[0] = key.KeyName;
                    }
                    // Single patches (old and new)
                    if (!multi)
                    {
                        if (int.TryParse(addrstrings[0], System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out int address) == false)
                            continue;
                        string patchvalue = patchType == PatchData.PatchType.File ? Path.Combine(ModPath, key.Value) : key.Value;
                        string patch = comment + "," + ClassesHelpers.Paths.NormalizePath(section.SectionName) + "," + address.ToString("X") + "," + patchType.ToString() + ",1," + patchvalue;
                        Patches.Add(new PatchData(patch, Path.GetDirectoryName(inifile)));
                    }
                }
            }
            // Compatibility with old format
            if (bool.TryParse(ModIni.Global["Autodemo"], out bool autodemo) == true)
            {
                string ver = autodemo ? "610-7029  V1.00019981016" : "MK-51000  V1.00519991005";
                GameVersions = new List<string> { ver }.ToArray();
                string[] filesfull = Directory.GetFiles(ModPath, "*.*", SearchOption.TopDirectoryOnly).Where(name => !name.EndsWith(".ini", StringComparison.OrdinalIgnoreCase)).ToArray();
                List<string> fileslist = new();
                foreach (string file in filesfull)
                {
                    fileslist.Add(Path.GetRelativePath(ModPath, file));
                }
                FileReplacements = fileslist.ToArray();
            }
            // Pointer replacements
            if (ModIni.Global["ReplacePointers"] != null || ModIni.Global["PatternFiles"] != null)
            {
                bool stripext = false;
                string[] pointerFiles;
                if (ModIni.Global["PatternFiles"] != null)
                {
                    pointerFiles = ModIni.Global["PatternFiles"].Split(',');
                    stripext = true;
                }
                else
                    pointerFiles = ModIni.Global["ReplacePointers"].Split(',');
                for (int i = 0; i < pointerFiles.Length; i++)
                {
                    string patternfile = Path.Combine(ModPath, stripext ? Path.ChangeExtension(pointerFiles[i], ".ini") : pointerFiles[i] + ".ini");
                    // Try with a general file if the specific one doesn't exist
                    if (!File.Exists(patternfile))
                        patternfile = Path.Combine(ModPath, "pattern.ini");
                    if (File.Exists(patternfile))
                    {
                        string[] patternData = File.ReadAllLines(patternfile);
                        foreach (string pattern in patternData)
                        {
                            if (pattern.Length <= 2 || pattern[0] == ';')
                                continue;
                            string[] origlines = pattern.Split(',');
                            string line = origlines[0] + "," + pointerFiles[i] + "," + origlines[1] + ",Pointer," + origlines[2];
                            Patches.Add(new PatchData(line, Path.GetDirectoryName(inifile)));
                        }
                    }
                }
            }
            // Submods
            if (ModIni.Global["Submods"] != null)
            {
                SubMods = ModIni.Global["Submods"].Split(',').ToList();
            }
            if (ModIni.Global["DefaultSubmods"] != null)
            {
                DefaultSubMods = ModIni.Global["DefaultSubmods"].Split(',').ToList();
            }
        }

        public ModResult Apply(string dataPath, Dictionary<string, byte[]> dataFiles, out string outmsg)
        {
            ModResult modResult = ModResult.Success;
            StringBuilder outmsgBuilder = new StringBuilder();
            if (DeleteFiles != null)
                for (int i = 0; i < DeleteFiles.Length; i++)
                {
                    string delpath = Path.Combine(dataPath, DeleteFiles[i]);
                    if (Directory.Exists(delpath))
                    {
                        Directory.Delete(delpath, true);
                        outmsgBuilder.AppendLine("Deleted folder" + Path.GetFileName(DeleteFiles[i]));
                    }
                    else if (File.Exists(delpath))
                    {
                        File.Delete(delpath);
                        outmsgBuilder.AppendLine("Deleted file " + Path.GetFileName(DeleteFiles[i]));
                    }
                    else
                    {
                        outmsgBuilder.AppendLine("Could not find path to delete: " + delpath);
                    }
                }
            if (IncludeFiles != null)
                for (int i = 0; i < IncludeFiles.Length; i++)
                {
                    outmsgBuilder.AppendLine("Adding file " + Path.GetFileName(IncludeFiles[i]));
                    if (dataFiles.ContainsKey(Path.GetFileName(IncludeFiles[i])))
                        dataFiles[IncludeFiles[i]] = File.ReadAllBytes(Path.Combine(ModPath, IncludeFiles[i]));
                    else
                        File.Copy(IncludeFiles[i], Path.Combine(dataPath, Path.GetFileName(IncludeFiles[i])), true);
                }
            if (FileReplacements != null)
                for (int i = 0; i < FileReplacements.Length; i++)
                {
                    outmsgBuilder.AppendLine("Copying file " + FileReplacements[i]);
                    string file = Path.Combine(ModPath, "gdroot", FileReplacements[i]);
                    if (!File.Exists(file))
                        file = Path.Combine(ModPath, FileReplacements[i]);
                    if (dataFiles.ContainsKey(FileReplacements[i]))
                    {
                        byte[] datafile = File.ReadAllBytes(file);
                        if (Path.GetExtension(FileReplacements[i]).ToLowerInvariant() == ".prs")
                            datafile = PSO.PRS.PRS.Decompress(datafile);
                        dataFiles[FileReplacements[i]] = datafile;
                    }
                    else
                    {
                        if (!Directory.Exists(Path.Combine(dataPath, Path.GetDirectoryName(FileReplacements[i]))))
                            Directory.CreateDirectory(Path.Combine(dataPath, Path.GetDirectoryName(FileReplacements[i])));
                        File.Copy(file, Path.Combine(dataPath, FileReplacements[i]), true);
                    }
                }
            foreach (PatchData pdata in Patches)
            {
                PatchData.PatchResult result = pdata.Apply(dataPath, dataFiles, out string message);
                if (result == PatchData.PatchResult.Error)
                    modResult = ModResult.Error;
                outmsgBuilder.Append(message);
            }
            outmsg = outmsgBuilder.ToString();
            return modResult;
        }
    }
}