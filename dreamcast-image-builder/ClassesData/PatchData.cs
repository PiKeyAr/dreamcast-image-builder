﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace DreamcastImageBuilder
{
    public class PatchData
    {
        public enum PatchType
        {
            HexBytes,
            HexBytesBigEndian,
            Sint8,
            Uint8,
            Sint16,
            Uint16,
            Sint32,
            Uint32,
            Float32,
            File,
            FileInsert,
            String,
            StringNullTerminated,
            Pointer,
            Error
        }

        public enum PatchResult
        {
            Success,
            Error
        }

        public string Description; // Patch description
        public string Filename; // Name of the file to write to
        public uint Address; // Offset to write to or pointer to search for
        public PatchType Type; // Type of value
        public uint Count; // Number of times to write
        public byte[] Data; // Data to write
        private string DataText; // Unparsed data from original patch line

        public static PatchType ReadPatchType(string str)
        {
            switch (str.ToLowerInvariant())
            {
                case "hexbytes":
                case "hb":
                    return PatchType.HexBytes;
                case "hexbytesbigendian":
                case "hexbytesbe":
                case "hbbe":
                case "be":
                    return PatchType.HexBytesBigEndian;
                case "sint8":
                case "si8":
                case "s8":
                    return PatchType.Sint8;
                case "uint8":
                case "ui8":
                case "u8":
                    return PatchType.Uint8;
                case "sint16":
                case "si16":
                case "s16":
                    return PatchType.Sint16;
                case "uint16":
                case "ui16":
                case "u16":
                    return PatchType.Uint16;
                case "sint32":
                case "si32":
                case "s32":
                    return PatchType.Sint32;
                case "uint32":
                case "ui32":
                case "u32":
                    return PatchType.Uint32;
                case "float32":
                case "float":
                case "f32":
                    return PatchType.Float32;
                case "file":
                    return PatchType.File;
                case "filei":
                case "fileins":
                case "fileinsert":
                    return PatchType.FileInsert;
                case "pointer":
                case "p":
                    return PatchType.Pointer;
                case "string":
                case "str":
                    return PatchType.String;
                case "stringnullterminated":
                case "stringnull":
                case "strnull":
                    return PatchType.StringNullTerminated;       
                default:
                    return PatchType.Error;
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new();
            sb.Append(Description + ",");
            sb.Append(Filename + ",");
            sb.Append(Address.ToString("X") + ",");
            sb.Append(Type.ToString() + ",");
            sb.Append(Count.ToString() + ",");
            sb.Append(DataText);
            return sb.ToString();
        }

        public static PatchData[] ParseMultiple(string patchLine, string modFolder, out string message)
        {
            StringBuilder outmsg = new StringBuilder();
            List<PatchData> patchData = new List<PatchData>();
            string[] lines = patchLine.Split(',');
            if (lines.Length < 5)
            {
                message = "Insufficient data for parsing patch line";
                return null;
            }
            string desc = lines[0];
            string filename = lines[1];
            // Address
            bool res = uint.TryParse(lines[2], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out uint addr_src);
            if (!res)
            {
                message = "Unable to parse address";
                return null;
            }
            uint address = addr_src;
            string[] types_split = lines[3].Split("/");
            string[] values_split = lines[4].Split("/");
            if (types_split.Length != values_split.Length)
            {
                message = "Number of patch types doesn't match number of patch values";
                return null;
            }
            for (int i = 0; i < types_split.Length; i++)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(desc + ",");
                sb.Append(filename + ",");
                sb.Append(lines[2] + ",");
                sb.Append(types_split[i] + ",");
                PatchType type = ReadPatchType(types_split[i]);
                if (type == PatchType.Error)
                {
                    message = "Unable to parse patch type for subpatch ID " + i.ToString();
                    return null;
                }
                else if (type == PatchType.Pointer)
                {
                    message = "Pointer patch type not supported in multiple patches";
                    return null;
                }
                sb.Append("1,");
                sb.Append(values_split[i]);
                PatchData data = new PatchData(sb.ToString(), modFolder);
                if (data.Type == PatchType.Error)
                {
                    message = "Unable to create patch from line: " + sb.ToString();
                    return null;
                }
                patchData.Add(data);
            }
            // Adjust addresses for subsequent patches
            for (int p = 1; p < patchData.Count; p++)
            {
                uint origAddr = patchData[p-1].Address;
                uint addAddr = (uint)(patchData[p - 1].Data.Length);
                uint newAddr = origAddr+addAddr;
                patchData[p].Address = newAddr;
                outmsg.AppendLine("Multi-patch: " + patchData[p].ToString());
            }
            message = outmsg.ToString();
            return patchData.ToArray();
        }

        public PatchData(string patchLine, string modFolder)
        {
            string[] lines = patchLine.Split(',');
            if (lines.Length < 5)
            {
                Description = "Insufficient parameters, must have 5 or more comma-separated items: " + patchLine;
                Type = PatchType.Error;
                return;
            }
            // Description
            Description = lines[0];
            // Filename
            Filename = lines[1];
            // Address
            bool res = uint.TryParse(lines[2], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out uint addr_src);
            if (!res)
            {
                Description = "Could not read address (parameter index 2): " + patchLine;
                Type = PatchType.Error;
                return;
            }
            Address = addr_src;
            // Read patch type. There can be multiple patch types in a single line divided by `/`.
            string[] types_split = lines[3].Split("/");
            // Single patch type
            if (types_split.Length < 2)
            {
                Type = ReadPatchType(lines[3]);
                if (Type == PatchType.Error)
                {
                    Description = "Could not read patch type (parameter index 3): " + patchLine;
                    return;
                }
            }
            // Multiple patch types 
            else
            {
                Description = "Multiple patches detected in single line: " + patchLine;
                Type = PatchType.Error;
                return;
            }
            if (Type != PatchType.Pointer)
            {
                if (lines.Length < 6)
                {
                    Description = "Insufficient parameters, must have 6 or more comma-separated items: " + patchLine;
                    Type = PatchType.Error;
                    return;
                }
                // Count
                res = uint.TryParse(lines[4], NumberStyles.Number, CultureInfo.InvariantCulture, out uint cnt);
                if (!res)
                {
                    Description = "Could not read count (parameter index 4): " + patchLine;
                    Type = PatchType.Error;
                    return;
                }
                Count = cnt;
            }
            // Values
            int currentline = lines.Length <= 5 ? 4 : 5;
            DataText = lines[currentline];
            switch (Type)
            {
                case PatchType.HexBytes:
                case PatchType.HexBytesBigEndian:
                    bool big = Type == PatchType.HexBytesBigEndian;
                    string value = lines[5].Replace(" ", "");
                    if (value.Length % 2 == 0)
                    {
                        Data = new byte[value.Length / 2];
                        int repl_cnt = 0;
                        int address = 0;
                        for (int i = 0; i < value.Length; i += 2)
                        {
                            res = byte.TryParse(value.Substring(big ? value.Length - 2 - i : i, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out byte b_res);
                            if (!res)
                            {
                                Description = "Could not parse hex byte value for patch: " + patchLine;
                                Type = PatchType.Error;
                                break;
                            }
                            Data[address + repl_cnt] = b_res;
                            repl_cnt++;
                        }
                    }
                    break;
                case PatchType.Sint8:
                    res = sbyte.TryParse(lines[5], NumberStyles.Number, CultureInfo.InvariantCulture, out sbyte v);
                    if (!res)
                    {
                        Description = "Could not parse sint8 value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = new byte[1];
                    Data[0] = (byte)v;
                    break;
                case PatchType.Uint8:
                    res = byte.TryParse(lines[5], NumberStyles.Number, CultureInfo.InvariantCulture, out byte vu);
                    if (!res)
                    {
                        Description = "Could not parse uint8 value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = new byte[1];
                    Data[0] = vu;
                    break;
                case PatchType.Sint16:
                    res = short.TryParse(lines[5], NumberStyles.Number, CultureInfo.InvariantCulture, out short si16);
                    if (!res)
                    {
                        Description = "Could not parse sint16 value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = BitConverter.GetBytes(si16);
                    break;
                case PatchType.Uint16:
                    res = ushort.TryParse(lines[5], NumberStyles.Number, CultureInfo.InvariantCulture, out ushort ui16);
                    if (!res)
                    {
                        Description = "Could not parse uint16 value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = BitConverter.GetBytes(ui16);
                    break;
                case PatchType.Sint32:
                    res = int.TryParse(lines[5], NumberStyles.Number, CultureInfo.InvariantCulture, out int si32);
                    if (!res)
                    {
                        Description = "Could not parse sint32 value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = BitConverter.GetBytes(si32);
                    break;
                case PatchType.Uint32:
                    res = uint.TryParse(lines[5], NumberStyles.Number, CultureInfo.InvariantCulture, out uint ui32);
                    if (!res)
                    {
                        Description = "Could not parse uint32 value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = BitConverter.GetBytes(ui32);
                    break;
                case PatchType.Float32:
                    res = float.TryParse(lines[5], NumberStyles.Number, CultureInfo.InvariantCulture, out float f32);
                    if (!res)
                    {
                        Description = "Could not parse float value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = BitConverter.GetBytes(f32);
                    break;
                case PatchType.File:
                case PatchType.FileInsert:
                    string filepath = Path.Combine(modFolder, lines[5]);
                    if (!File.Exists(filepath))
                    {
                        Description = string.Format("Could not load file for patch '{0}': file {1} does not exist", patchLine, filepath);
                        Type = PatchType.Error;
                        return;
                    }
                    Data = File.ReadAllBytes(filepath);
                    if (Description == "None")
                        Description = filepath;
                    break;
                case PatchType.Pointer:
                    // In case the "count" value is specified, read the value after it as the replacement pointer
                    res = uint.TryParse(lines[currentline], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out uint p32);
                    if (!res)
                    {
                        Description = "Could not parse pointer value" + lines[currentline] + " for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    Data = BitConverter.GetBytes(p32);
                    break;
                case PatchType.String:
                    try
                    {
                        Data = System.Text.Encoding.ASCII.GetBytes(lines[5]);
                    }
                    catch
                    {
                        Description = "Could not parse string value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    break;
                case PatchType.StringNullTerminated:
                    try
                    {
                        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(lines[5]);
                        Data = new byte[bytes.Length + 1];
                        Array.Copy(bytes, Data, bytes.Length);
                    }
                    catch
                    {
                        Description = "Could not parse null terminated string value for patch: " + patchLine;
                        Type = PatchType.Error;
                        return;
                    }
                    break;
            }
        }

        public PatchResult Apply(string dataPath, Dictionary<string, byte[]> dataFiles, out string patchOutputMessage)
        {
            if (Type == PatchType.Error)
            {
                patchOutputMessage = string.Format("Patch with description '{0}' for address {1} contains invalid data" + System.Environment.NewLine, Description, Address.ToString("X"));
                return PatchResult.Error;
            }
            //MessageBox.Show(null, string.Format("Applying to {0} file {1} type {2}", dataPath, Filename, Type), "A", MessageBoxButtons.OK);
            bool fileAlreadyOpen = dataFiles.ContainsKey(Filename);
            bool isPRS = Path.GetExtension(Filename).ToLowerInvariant() == ".prs";
            byte[] datafile;
            string pathToFile = Path.Combine(dataPath, Filename);
            if (!fileAlreadyOpen)
            {
                if (!File.Exists(pathToFile))
                {
                    patchOutputMessage = string.Format("File {0} for patch with description {1} doesn't exist" + System.Environment.NewLine, pathToFile, Description);
                    return PatchResult.Error;
                }
                datafile = File.ReadAllBytes(pathToFile);
                if (isPRS)
                    datafile = PSO.PRS.PRS.Decompress(datafile);
            }
            else
                datafile = dataFiles[Filename];
            if (datafile == null)
            {
                patchOutputMessage = "Data to patch is null";
                return PatchResult.Error;
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("Applying patch to: " + Filename + ": " + Description);
            // Resize the data array if writing past the end of file
            if (Type != PatchType.Pointer && datafile.Length < Address + Data.Length * Count)
            {
                int newsize = (int)Address + Data.Length * (int)Count;
                stringBuilder.AppendLine("Resizing data to " + newsize.ToString() + " (0x" + newsize.ToString("X") + ")");
                if (newsize % 4 != 0)
                {
                    do
                        newsize++;
                    while (newsize % 4 != 0);
                }
                Array.Resize(ref datafile, newsize);
            }
            string valuerepr; // Value representation as a string
            switch (Type)
            {
                case PatchType.Error:
                    patchOutputMessage = "Invalid patch data";
                    return PatchResult.Error;
                case PatchType.Sint8:
                case PatchType.Uint8:
                    valuerepr = Data[0].ToString("X2");
                    for (int i = 0; i < Count; i++)
                    {
                        stringBuilder.AppendLine(Address.ToString("X") + ": " + datafile[Address + i].ToString("X2") + " -> " + valuerepr);
                        datafile[Address + i] = Data[0];
                    }
                    break;
                case PatchType.Sint16:
                case PatchType.Uint16:
                    valuerepr = Type == PatchType.Uint16 ? BitConverter.ToUInt16(Data).ToString() : BitConverter.ToInt16(Data).ToString();
                    for (int i = 0; i < Count; i++)
                    {
                        stringBuilder.AppendLine(string.Format("{0}: Writing {1} value {2}: {3}", (Address + i * 2).ToString("X"), Type.ToString(), valuerepr, datafile[Address + i * 2].ToString("X2") + " " + datafile[Address + 1 + i * 2].ToString("X2") + " -> " + Data[0].ToString("X2") + " " + Data[1].ToString("X2")));
                        datafile[Address + i * 2] = Data[0];
                        datafile[Address + 1 + i * 2] = Data[1];
                    }
                    break;
                case PatchType.Sint32:
                case PatchType.Uint32:
                case PatchType.Float32:
                    if (Type == PatchType.Float32)
                        valuerepr = BitConverter.ToSingle(Data).ToString() + "f";
                    else
                        valuerepr = Type == PatchType.Uint32 ? BitConverter.ToUInt32(Data).ToString() : BitConverter.ToInt32(Data).ToString();
                    for (int i = 0; i < Count; i++)
                    {
                        stringBuilder.AppendLine(
                            string.Format("{0}: Writing {1} value: {2}: {3}",
                            Address.ToString("X"),
                            Type.ToString(),
                            valuerepr,
                            datafile[Address + i * 4].ToString("X2") + " " +
                            datafile[Address + 1 + i * 4].ToString("X2") + " " +
                            datafile[Address + 2 + i * 4].ToString("X2") + " " +
                            datafile[Address + 3 + i * 4].ToString("X2") + " " +
                            " -> " +
                            Data[0].ToString("X2") + " " +
                            Data[1].ToString("X2") + " " +
                            Data[2].ToString("X2") + " " +
                            Data[3].ToString("X2")
                            ));
                        datafile[Address + i * 4] = Data[0];
                        datafile[Address + 1 + i * 4] = Data[1];
                        datafile[Address + 2 + i * 4] = Data[2];
                        datafile[Address + 3 + i * 4] = Data[3];
                    }
                    break;
                case PatchType.File:
                    stringBuilder.AppendLine(string.Format("{0}: Writing a block of {1} bytes", Address.ToString("X"), Data.Length.ToString()));
                    Array.Copy(Data, 0, datafile, Address, Data.Length);
                    break;
                case PatchType.FileInsert:
                    for (int i = 0; i < Count; i++)
                    {
                        stringBuilder.AppendLine(string.Format("{0}: Inserting a block of {1} bytes", (Address + i * Data.Length).ToString("X"), Data.Length.ToString()));
                        byte[] res = new byte[datafile.Length + Data.Length];
                        // Copy data before inserted bytes
                        Array.Copy(datafile, 0, res, 0, Address);
                        // Copy inserted bytes
                        Array.Copy(Data, 0, res, Address, Data.Length);
                        // Copy data after inserted bytes
                        Array.Copy(datafile, Address, res, Address + Data.Length, datafile.Length - Data.Length);
                        datafile = res;
                    }
                    break;
                case PatchType.Pointer:
                    for (int addr = 0; addr < datafile.Length - 4; addr += 4)
                    {
                        //stringBuilder.AppendLine(string.Format("{0}: checking value {1} at address {2}", isPRS ? Path.ChangeExtension(Path.GetFileName(Filename), ".bin") : Path.GetFileName(Filename), Address.ToString("X"), addr.ToString("X"));
                        if (BitConverter.ToUInt32(datafile, addr) == Address)
                        {
                            stringBuilder.AppendLine(string.Format("{0}: writing pointer value {1} to address {2}",
                                Path.GetFileName(Filename) + (isPRS ? " (decompressed)" : ""),
                                Data[3].ToString("X2") +
                                Data[2].ToString("X2") +
                                Data[1].ToString("X2") +
                                Data[0].ToString("X2"),
                                addr.ToString("X")));
                            Data.CopyTo(datafile, addr);
                        }
                    }
                    break;
                case PatchType.HexBytes:
                case PatchType.HexBytesBigEndian:
                    StringBuilder src = new StringBuilder();
                    StringBuilder dst = new StringBuilder();
                    stringBuilder.Append(Address.ToString("X") + ": ");
                    int patchDataIndex = 0;
                    for (int i = 0; i < Data.Length * Count; i++)
                    {
                        src.Append(datafile[Address + i].ToString("X2") + " "); // Source byte
                        dst.Append(Data[patchDataIndex].ToString("X2") + " "); // Destination byte
                        datafile[Address + i] = Data[patchDataIndex];
                        patchDataIndex++;
                        if (patchDataIndex >= Data.Length)
                            patchDataIndex = 0;
                    }
                    stringBuilder.AppendLine(src.ToString() + " -> " + dst.ToString());
                    break;
                case PatchType.String:
                case PatchType.StringNullTerminated:
                    for (int i = 0; i < Count; i++)
                    {   
                        // List of original bytes to output
                        StringBuilder originalBytesText = new();
                        for (int b = 0; b < Data.Length; b++)
                            originalBytesText.Append(datafile[Address + b].ToString("X2") + " ");

                        // List of replacement bytes to output
                        StringBuilder replacedBytesText = new();
                        for (int b = 0; b < Data.Length; b++)
                            replacedBytesText.Append(Data[b].ToString("X2") + " ");

                        // Write "Writing string"
                        stringBuilder.AppendLine((Address + i * Data.Length).ToString("X") + ": Writing " +
                            (Type == PatchType.StringNullTerminated ? "null-terminated " : "") + "string '" +
                            System.Text.Encoding.ASCII.GetString(Data, 0, (Type == PatchType.StringNullTerminated ? Data.Length - 1 : Data.Length)) + "':");
                        
                        // Write replacement bytes
                        stringBuilder.AppendLine(originalBytesText.ToString() + " - > " + replacedBytesText.ToString()); ;;
                        
                        // Do the write operation
                        Array.Copy(Data, 0, datafile, Address + i * Data.Length, Data.Length);
                    }
                    break;
            }
            if (fileAlreadyOpen)
                dataFiles[Filename] = datafile;
            else
                dataFiles.Add(Filename, datafile);
            patchOutputMessage = stringBuilder.ToString();
            return PatchResult.Success;
        }
    }
}