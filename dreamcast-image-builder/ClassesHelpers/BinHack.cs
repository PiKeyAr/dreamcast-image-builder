﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DreamcastImageBuilder.ClassesHelpers
{
    // Injects ECHELON's selfboot code into IP.BIN but preserves original bootstraps
    public static class BinHack
    {
        // Checks if there are empty bytes in the specified range in IP.bin
        private static bool CheckEmptySpace(byte[] ipbin, uint start, uint end)
        {
            for (uint i = start; i < end; i++)
                if (ipbin[i] != 0)
                    return false;
            return true;
        }

        // Finds the pointer to 1ST_READ which would be replaced with a pointer to the scrambling code
        private static int SearchForFstReadPointer(byte[] ipbin, int start, int end)
        {
            for (int i = start; i < end; i += 4)
                if (BitConverter.ToUInt32(ipbin, i) == 0xAC010000)
                    return i;
            return -1;
        }

        // Injects selfboot code into the specified IP.BIN, needs the size of 1ST_READ.BIN in bytes
        public static void InsertSelfBoot(string ipbinpath, int fstreadsize, uint offset, uint buffer)
        {
            byte[] selfbootcode = Properties.Resources.selfboot;
            // Set 1ST_READ size
            Array.Copy(BitConverter.GetBytes(fstreadsize), 0, selfbootcode, 0x1FC, 4);
            // Set buffer location (default 8CE00000/8CE11000) / 0x20000000 is added to convert 8C to AC
            Array.Copy(BitConverter.GetBytes(buffer + 0x20000000 + 0x20000), 0, selfbootcode, 0x1F4, 4);
            // Set buffer location 2 (default 8CDE0000/8CDF1000)
            Array.Copy(BitConverter.GetBytes(buffer + 0x20000000), 0, selfbootcode, 0x1F8, 4);
            // Load IP.BIN
            byte[] ipbin = File.ReadAllBytes(ipbinpath);
            // Nop the instructions in bootstrap 1
            IpProtHack(ipbin);
            // Find 1ST_READ pointer in bootstrap 2 
            int pointer = SearchForFstReadPointer(ipbin, 0x6000, 0x7FD0);
            // Check if the pointer was found and if 592 (0x250) bytes in bootstrap 1 are empty
            if (pointer == -1 || !CheckEmptySpace(ipbin, offset, offset + 0x250))
            {
                Logger.WriteMessage("Unknown IP.BIN found! Please send it to me so I could analyze it.");
            }
            // Insert selfboot code (0x5500-0x5750 is always empty from what I've seen)
            Array.Copy(selfbootcode, 0, ipbin, offset, selfbootcode.Length);
            // Set selfboot code pointer in bootstrap 2
            uint sbpointer = 0xAC008000 + offset; // IP.BIN memory address without cache + offset to scramble code
            Array.Copy(BitConverter.GetBytes(sbpointer), 0, ipbin, pointer, 4);
            // Write IP.BIN
            File.WriteAllBytes(ipbinpath, ipbin);
        }

        // Common GDROM protections in boot binaries. Most of this info is from forum posts by MILF and atreyu187.
        private static Dictionary<byte[], byte[]> GdromProts = new Dictionary<byte[], byte[]>
        {
            // Super Street Fighter II X, Super Puzzle Fighter II X Ooga Booga
            { new byte[] { 0x10, 0x32, 0x0D, 0x8B }, new byte[] { 0x08, 0x00, 0x0D, 0x8B } }, 
            // Confidential Mission, Metropolis Street Racer, Rez, KOF 1999-2002
            { new byte[] { 0xCD, 0xE4, 0x43, 0x6A }, new byte[] { 0x09, 0x00, 0x09, 0x00 } }, 
            // Evil Dead Hail to the King
            { new byte[] { 0xCD, 0xEB, 0x22, 0xD1 }, new byte[] { 0x09, 0x00, 0x22, 0xD1 } }, 
            // ?
            { new byte[] { 0x02, 0xE0, 0x04, 0x6A }, new byte[] { 0x00, 0xE0, 0x04, 0xA0 } }, 
            // ?
            { new byte[] { 0x13, 0xE1, 0x10, 0x22 }, new byte[] { 0x09, 0xE1, 0x10, 0x22 } }, 
            // ?
            { new byte[] { 0x03, 0x89, 0x26, 0xD3, 0x24, 0xD4, 0x0B, 0x43 }, new byte[] { 0x09, 0x00, 0x09, 0x00, 0x09, 0x00, 0x09, 0x00 } }, 
            // Sonic Adventure 2 (megavolt85?)
            { new byte[] { 0x12, 0xd1, 0x11, 0xd0, 0x02, 0x62, 0x19, 0x42 }, new byte[] { 0x09, 0x00, 0x09, 0x00, 0x0b, 0x00, 0x09, 0x00 } }, 
            // De La Jet Set Radio, Jet Set Radio
            { new byte[] { 0x0B, 0xD2, 0x37, 0x32, 0x02, 0x8B }, new byte[] { 0x0B, 0xD2, 0x08, 0x00, 0x02, 0x8B } }, 
            // Virtua Striker ver.2000.1 (JP only tested so far)
            { new byte[] { 0x78, 0xA0, 0x1A, 0x0C, 0x8E, 0x5D, 0x00, 0x00, 0x70, 0xA3, 
            0x23, 0x0C, 0x7A, 0xC9, 0x04, 0x00, 0x58, 0xC9 }, new byte[] { 0xD8, 0xA8, 
            0x1A, 0x0C, 0xB4, 0x40, 0x02, 0x00, 0xC0, 0xAB, 0x23, 0x0C, 0x63, 0xA6, 
            0x04, 0x00, 0x4C, 0x45 } }
        };

        // Drive media check that is present in some versions of IP.BIN
        private static Dictionary<byte[], byte[]> IpProts = new Dictionary<byte[], byte[]>
        {
            // SG_INI bootstrap 90405 (0, 3, 9) and 00623 (1, 10)
            { new byte[] { 0x09, 0x00, 0x08, 0xD0, 0x0B, 0x40, 0x09, 0x00, 0x08, 0xD0, 0x0B, 0x40 }, new byte[] { 0x09, 0x00, 0x08, 0xD0, 0x0B, 0x40, 0x09, 0x00, 0x09, 0x00, 0x09, 0x00 } } 
        };

        // Region patch for IP.BIN
        public static void RegionHack(byte[] ipbin)
        {
            ipbin[0x30] = (byte)'J';
            ipbin[0x31] = (byte)'U';
            ipbin[0x32] = (byte)'E';
            Array.Copy(System.Text.Encoding.ASCII.GetBytes("For JAPAN,TAIWAN,PHILIPINES."), 0, ipbin, 0x3704, 28);
            Array.Copy(System.Text.Encoding.ASCII.GetBytes("For USA and CANADA.         "), 0, ipbin, 0x3724, 28);
            Array.Copy(System.Text.Encoding.ASCII.GetBytes("For EUROPE.                 "), 0, ipbin, 0x3744, 28);
        }

        // Searches and replaces a byte pattern
        public static void ReplacePattern(byte[] file, Dictionary<byte[], byte[]> pats, int start = 0, int end = 0)
        {
            if (end == 0)
                end = file.Length;
            for (int i = start; i < end; i++)
            {
                foreach (var prot in pats)
                {
                    if (i + prot.Key.Length > file.Length - 1)
                    {
                        continue;
                    }
                    bool match = false;
                    for (int u = 0; u < prot.Key.Length; u++)
                    {
                        if (file[i + u] != prot.Key[u])
                        {
                            match = false;
                            break;
                        }
                        match = true;
                    }
                    if (match)
                    {
                        StringBuilder src = new StringBuilder();
                        StringBuilder dst = new StringBuilder();
                        for (int u = 0; u < prot.Value.Length; u++)
                        {
                            src.Append(prot.Key[u].ToString("X2") + " "); // Source byte
                            dst.Append(prot.Value[u].ToString("X2") + " "); // Destination byte
                        }
                        Logger.WriteMessage(string.Format("Patching protection at 0x{0}: {1} -> {2}", i.ToString("X"), src.ToString(), dst.ToString()));
                        Array.Copy(prot.Value, 0, file, i, prot.Key.Length);
                    }
                }
            }
        }

        // Patches out common GDROM protections
        public static void GdromHack(byte[] fstread)
        {
            Logger.WriteMessage("Patching GDROM protections...");
            ReplacePattern(fstread, GdromProts);
        }

        // Patches out common IP.BIN protections
        public static void IpProtHack(byte[] ipbin)
        {
            Logger.WriteMessage("Patching bootstrap protections...");
            ReplacePattern(ipbin, IpProts, 0x3800, 0x5FFF);
        }

        // Changes the Boot LBA value in 1ST_READ.BIN
        public static void LbaHack(byte[] fstread, ushort lba)
        {
            Logger.WriteMessage("Patching Boot LBA...");
            for (int i = 0; i < fstread.Length - 5; i++)
            {
                if (System.Text.Encoding.ASCII.GetString(fstread, i, 5) == "CD001")
                {
                    Logger.WriteMessage("CD001 string at 0x" + i.ToString("X"));
                    if (BitConverter.ToUInt16(fstread, i - 8) == 45166)
                    {
                        Logger.WriteMessage(string.Format("Patched value at 0x{0}: 6E B0 -> {1} {2}", (i - 8).ToString("X"),
                            BitConverter.GetBytes(lba + 166)[0].ToString("X"), BitConverter.GetBytes(lba + 166)[1].ToString("X")));
                        Array.Copy(BitConverter.GetBytes(lba + 166), 0, fstread, i - 8, 2);
                    }
                    else
                    {
                        Logger.WriteMessage("Boot LBA value wrong: expected 45166, got " + BitConverter.ToUInt16(fstread, i - 8).ToString());
                    }
                }
            }
        }
    }
}