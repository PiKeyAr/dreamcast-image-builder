﻿using DreamcastImageBuilder.Properties;
using System;
using System.IO;
using System.Threading;

// This is basically a port of LazyBoot's mkcdi to C# with some hacks.
// TODO: Do the CDI format properly instead of writing hardcoded byte arrays.

namespace DreamcastImageBuilder.ClassesHelpers
{
    public static class CDITools
    {
        public static bool isCancelled;

        public static void CreateCdiImage(string inputFile, string outputFile, int lba)
        {            
            try
            {
                using (FileStream f = new FileStream(inputFile, FileMode.Open, FileAccess.Read))
                {
                    f.Seek(0, SeekOrigin.End);
                    long fileSize = f.Position;
                    int sectorCount = (int)(fileSize / 2048);
                    f.Seek(0, SeekOrigin.Begin);

                    using (FileStream g = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
                    {
                        g.Write(new byte[1063104], 0, 1063104);
                        byte[] temp = Resources.cdi_start;
                        g.Write(temp, 0, temp.Length);

                        for (int i = 0; i < sectorCount; i++)
                        {
                            if (isCancelled)
                            {
                                return;
                            }
                            temp = new byte[0x800];
                            f.Read(temp, 0, 0x800);
                            g.Write(new byte[8], 0, 8);
                            g.Write(temp, 0, temp.Length);
                            g.Write(new byte[280], 0, 280);
                        }

                        temp = Resources.cdi_end;
                        g.Write(temp, 0, temp.Length);

                        g.Seek(-158, SeekOrigin.End);
                        g.Write(BitConverter.GetBytes(lba), 0, 4);
                        g.Seek(-277, SeekOrigin.End);
                        g.Write(BitConverter.GetBytes(sectorCount + 152), 0, 4);
                        g.Seek(-310, SeekOrigin.End);
                        g.Write(BitConverter.GetBytes(lba), 0, 4);
                        g.Write(BitConverter.GetBytes(sectorCount + 152), 0, 4);
                        g.Seek(-336, SeekOrigin.End);
                        g.Write(BitConverter.GetBytes(sectorCount + 2), 0, 4);
                    }
                    f.Close();
                    Logger.WriteMessage($"CDI image created: {outputFile}", true);
                    File.Delete(inputFile);
                }
            }
            catch (FileNotFoundException)
            {
                Logger.WriteMessage($"Error: Input file '{inputFile}' not found.", true);
            }
            catch (Exception e)
            {
                Logger.WriteMessage($"CDI build error: {e}", true);
            }
        }
    }
}