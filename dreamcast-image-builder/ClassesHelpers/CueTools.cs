﻿using System;
using System.IO;
using System.Linq;
using CueSharp;

// Functions to help deal with .cue files. Mostly from https://github.com/AwfulBear/RedumpCUE2GDI

namespace DreamcastImageBuilder.ClassesHelpers
{
    public static class CueTools
    {
        public static void ConvertCUEtoGDI(string cuePath, string workdir, bool skiplast)
        {
            CueSheet cueSheet = new CueSheet(cuePath);
            string workingDirectory = Path.GetDirectoryName(cuePath);
            int currentSector = 0;
            StringWriter gdiOutput = new StringWriter();
            gdiOutput.WriteLine(cueSheet.Tracks.Length.ToString());
            for (int i = 0; i < cueSheet.Tracks.Length; i++)
            {
                if (ImageBuilder.status == ImageBuilder.BuildStatus.Cancelling)
                    return;
                Track currentTrack = cueSheet.Tracks[i];
                string inputTrackFilePath = Path.Combine(workingDirectory, currentTrack.DataFile.Filename);
                bool canPerformFullCopy = currentTrack.Indices.Length == 1;
                string outputTrackFileName = string.Format(
                    "track{0}.{1}",
                    currentTrack.TrackNumber.ToString("D2"),
                    currentTrack.TrackDataType == DataType.AUDIO ? "raw" : "bin");
                string outputTrackFilePath = Path.Combine(workdir, outputTrackFileName);

                Logger.WriteMessage(string.Format("Creating track file {0} of {1}: {2} as {3}...", i, cueSheet.Tracks.Length, currentTrack.DataFile.Filename, outputTrackFileName));

                int sectorAmount;
                if (canPerformFullCopy)
                {
                    if (!skiplast || i != cueSheet.Tracks.Length - 1)
                        File.Copy(inputTrackFilePath, outputTrackFilePath);
                    sectorAmount = (int)(new FileInfo(inputTrackFilePath).Length / 2352);
                }
                else
                {
                    int gapOffset = CountIndexFrames(currentTrack.Indices[1]);
                    sectorAmount = CopyFileWithGapOffset(inputTrackFilePath, outputTrackFilePath, gapOffset, skiplast && i == cueSheet.Tracks.Length - 1);
                    currentSector += gapOffset;
                }

                if (ImageBuilder.status == ImageBuilder.BuildStatus.Cancelling)
                    return;

                int gap = 0;

                gdiOutput.WriteLine("{0} {1} {2} 2352 {3} {4}",
                    currentTrack.TrackNumber,
                    currentSector,
                    currentTrack.TrackDataType == DataType.AUDIO ? "0" : "4",
                    outputTrackFileName,
                    gap);

                currentSector += sectorAmount;

                if (currentTrack.Comments.Contains("HIGH-DENSITY AREA"))
                    if (currentSector < 45000)
                        currentSector = 45000;
            }

            string gdiOutputPath = Path.Combine(workdir, "disc.gdi");

            Logger.WriteMessage("Generating GDI file:" + gdiOutputPath);

            File.WriteAllText(gdiOutputPath, gdiOutput.ToString());
        }

        private static int CountIndexFrames(CueSharp.Index index)
        {
            int result = index.Frames;
            result += (index.Seconds * 75);
            result += ((index.Minutes * 60) * 75);
            return result;
        }

        private static int CopyFileWithGapOffset(string inputFile, string outputFile, int frames, bool skip)
        {
            Stream infile = File.OpenRead(inputFile);
            Stream outfile = null;
            if (!skip)
                outfile = File.OpenWrite(outputFile);
            int blockSize = 2352;
            infile.Position = frames * blockSize;
            int result = (int)((infile.Length - infile.Position) / blockSize);
            byte[] buffer = new byte[blockSize];
            while (blockSize > 0)
            {
                if (ImageBuilder.status == ImageBuilder.BuildStatus.Cancelling)
                {
                    outfile.Flush();
                    outfile.Close();
                    infile.Close();
                    return 0;
                }
                blockSize = infile.Read(buffer, 0, blockSize);
                if (!skip)
                    outfile.Write(buffer, 0, blockSize);
            }
            if (!skip)
            {
                outfile.Flush();
                outfile.Close();
            }
            infile.Close();
            return result;
        }
    }
}
