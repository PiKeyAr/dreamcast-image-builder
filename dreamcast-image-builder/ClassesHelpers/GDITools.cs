﻿using System;
using System.Collections.Generic;
using DiscUtils.Gdrom;
using System.IO;
using static DreamcastImageBuilder.ImageBuilder;
using System.Threading;
using System.Text;

namespace DreamcastImageBuilder.ClassesHelpers
{
    public static class GDITools
    {
        public class GDITrack
        {
            public int TrackNumber;
            public int LBA;
            public int TrackType;
            public int SectorSize;
            public string TrackFilename;
            public int Offset;

            // TODO: Make this prettier...
            public GDITrack(string track, bool verbose)
            {
                int stringIndex = 0;

                // Get track number
                StringBuilder temp = new StringBuilder();
                for (int i = stringIndex; i < track.Length; i++)
                {
                    if (track[i] == ' ')
                        break;
                    temp.Append(track[i]);
                    stringIndex++;
                }
                TrackNumber = int.Parse(temp.ToString());
                // Skip spaces afterwards
                do { stringIndex++; }
                while (track[stringIndex] == ' ');

                // Get start LBA
                temp = new StringBuilder();
                for (int i = stringIndex; i < track.Length; i++)
                {
                    if (track[i] == ' ')
                        break;
                    temp.Append(track[i]);
                    stringIndex++;
                }
                LBA = int.Parse(temp.ToString());
                // Skip spaces afterwards
                do { stringIndex++; }
                while (track[stringIndex] == ' ');

                // Get track type
                temp = new StringBuilder();
                for (int i = stringIndex; i < track.Length; i++)
                {
                    if (track[i] == ' ')
                        break;
                    temp.Append(track[i]);
                    stringIndex++;
                }
                TrackType = int.Parse(temp.ToString());
                // Skip spaces afterwards
                do { stringIndex++; }
                while (track[stringIndex] == ' ');

                // Get sector size
                temp = new StringBuilder();
                for (int i = stringIndex; i < track.Length; i++)
                {
                    if (track[i] == ' ')
                        break;
                    temp.Append(track[i]);
                    stringIndex++;
                }
                SectorSize = int.Parse(temp.ToString());
                // Skip spaces afterwards
                do { stringIndex++; }
                while (track[stringIndex] == ' ');

                // Get filename
                temp = new StringBuilder();
                char endChar = ' ';
                if (track[stringIndex] == '\"')
                {
                    endChar = '\"';
                    stringIndex++;
                }
                for (int i = stringIndex; i < track.Length; i++)
                {
                    if (track[i] == endChar)
                        break;
                    temp.Append(track[i]);
                    stringIndex++;
                }
                // Skip spaces afterwards
                do { stringIndex++; }
                while (track[stringIndex] == ' ');
                TrackFilename = temp.ToString();

                // Get offset
                temp = new StringBuilder();
                for (int i = stringIndex; i < track.Length; i++)
                {
                    if (track[i] == ' ')
                        break;
                    temp.Append(track[i]);
                    stringIndex++;
                }
                Offset = int.Parse(temp.ToString());
                if (verbose)
                    Logger.WriteMessage(string.Format("GDI track {0}: LBA {1} type {2} sector size {3} filename '{4}' offset {5}", TrackNumber, LBA, TrackType, SectorSize, TrackFilename, Offset));
            }
        }

        public class GDIFile
        {
            public List<GDITrack> Tracks;

            public GDIFile(string gdipath, bool verbose)
            {
                string[] gdilines = File.ReadAllLines(gdipath);
                // Get track count
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < gdilines[0].Length; i++)
                {
                    if (gdilines[0][i] == ' ')
                        break;
                    sb.Append(gdilines[0][i]);
                }
                int numTracks = int.Parse(sb.ToString());
                if (verbose)
                    Logger.WriteMessage("Number of tracks: " +  numTracks.ToString());
                Tracks = new List<GDITrack>();
                for (int i = 0; i < numTracks; i++)
                {
                    Tracks.Add(new GDITrack(gdilines[i+1], verbose));
                }
            }

            public void Save(string outFile, bool standardTrackNames)
            {
                using (TextWriter writer = File.CreateText(outFile))
                {
                    writer.WriteLine(Tracks.Count.ToString());
                    foreach (var track in Tracks)
                    {
                        string trackFile;
                        if (!standardTrackNames)
                        {
                            trackFile = track.TrackFilename;
                            if (trackFile.Contains(' '))
                                trackFile = "\"" + trackFile + "\"";
                        }
                        else
                        {
                            string trackExt = (track.TrackType == 4) ? ".bin" : ".raw";
                            trackFile = "track" + track.TrackNumber.ToString("D2") + trackExt;
                        }
                        writer.WriteLine(track.TrackNumber.ToString() + " " +
                            track.LBA.ToString() + " " +
                            track.TrackType.ToString() + " " +
                            track.SectorSize.ToString() + " " +
                            trackFile + " " +
                            track.Offset
                            );
                    }
                    writer.Flush();
                    writer.Close();
                }
            }
        }

        public static string GetVolumeLabel(string gdi)
        {
            string result;
            GDReader gdReader;
            if (Path.GetExtension(gdi).ToLowerInvariant() == ".cue")
                gdReader = GDReader.FromCueSheet(gdi);
            else
                gdReader = GDReader.FromGDIfile(gdi);
            result = gdReader.VolumeLabel;
            gdReader.Dispose();
            return result;
        }

        public static void ExtractIP(string gdi, string workdir_data)
        {
            GDReader gdReader;
            if (Path.GetExtension(gdi).ToLowerInvariant() == ".cue")
                gdReader = GDReader.FromCueSheet(gdi);
            else
                gdReader = GDReader.FromGDIfile(gdi);
            MemoryStream ipbin = (MemoryStream)gdReader.ReadIPBin();
            File.WriteAllBytes(Path.Combine(workdir_data, "IP_GDI.BIN"), ipbin.ToArray());
            gdReader.Dispose();
        }

        public static void ExtractGDI(string gdi, string workdir_data)
        {
            GDReader gdReader;
            if (Path.GetExtension(gdi).ToLowerInvariant() == ".cue")
            {
                gdReader = GDReader.FromCueSheet(gdi);
            }
            else
            {
                GDIFile gd = new GDIFile(gdi, true);
                gdReader = GDReader.FromGDIfile(gdi);
            }
            var allFiles = gdReader.GetFiles("", "*.*", SearchOption.AllDirectories);
            foreach (var file in allFiles)
            {
                if (status == BuildStatus.Cancelling)
                {
                    break;
                }
                string infile = file;
                if (infile.StartsWith("\\"))
                    infile = infile.Substring(1);
                Logger.WriteMessage(string.Format("Extracting file: {0}", infile));
                string fileOutPath = Path.Combine(workdir_data, infile);
                if (!Directory.Exists(Path.GetDirectoryName(fileOutPath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(fileOutPath));
                using (Stream fileStream = gdReader.OpenFile(infile, FileMode.Open, FileAccess.Read))
                {
                    using (FileStream outfileStream = new FileStream(fileOutPath, FileMode.Create))
                    {
                        fileStream.CopyTo(outfileStream);
                        outfileStream.Flush();
                        outfileStream.Close();
                    }
                }
            }
            gdReader.Dispose();
        }

        public static void CreateGDI(string data, string ipBin, List<string> outPath, List<string> cdda, string gdiPath, string volume, bool raw, bool truncate, CancellationTokenSource tokenSource)
        {
            CancellationToken token = tokenSource.Token;
            GDromBuilder builder = new GDromBuilder(ipBin, cdda);
            builder.ReportProgress += ProgressReport;
            builder.RawMode = raw;
            builder.TruncateData = truncate;
            if (volume != null)
            {
                builder.VolumeIdentifier = volume;
            }
            List<DiscTrack> tracks = null;
            bool fileOutput = false;
            if (CheckArguments(data, ipBin, outPath, cdda, truncate, out fileOutput) == false)
            {
                return;
            }
            if (fileOutput)
            {
                builder.Track03Path = Path.GetFullPath(outPath[0]);
                if (outPath.Count == 2 && (cdda.Count > 0 || builder.TruncateData))
                {
                    builder.LastTrackPath = Path.GetFullPath(outPath[1]);
                }
                builder.ImportFolder(data, token: token);
                tracks = builder.BuildGDROM(token);
            }
            else
            {
                builder.ImportFolder(data, token: token);
                tracks = builder.BuildGDROM(outPath[0], token); 
            }
            if (gdiPath != null)
            {
                builder.UpdateGdiFile(tracks, gdiPath);
            }
            else
            {
                Logger.WriteMessage(builder.GetGDIText(tracks));
            }
        }

        private static void ProgressReport(int amount)
        {
            if (amount % 10 == 0)
            {
                Logger.WriteMessage(amount + "%...", true, false);
            }
        }

        private static bool CheckArguments(string data, string ipBin, List<string> outPath, List<string> cdda, bool truncate, out bool fileOutput)
        {
            fileOutput = false;
            if (data == null || ipBin == null || outPath.Count == 0)
            {
                Console.WriteLine("The required fields have not been provided.");
                return false;
            }
            if (!Directory.Exists(data))
            {
                Console.WriteLine("The specified data directory does not exist!");
                return false;
            }
            if (!File.Exists(ipBin))
            {
                Console.WriteLine("The specified IP.BIN file does not exist!");
                return false;
            }
            foreach (string track in cdda)
            {
                if (!File.Exists(track))
                {
                    Console.WriteLine("The CDDA track " + track + " does not exist!");
                    return false;
                }
            }
            if (outPath.Count > 2)
            {
                Console.WriteLine("Too many output paths specified.");
                return false;
            }
            else if (outPath.Count == 2)
            {
                fileOutput = true;
                if (!Path.HasExtension(outPath[0]) || !Path.HasExtension(outPath[1]))
                {
                    Console.WriteLine("Output filenames are not valid!");
                    return false;
                }
            }
            else
            {
                string path = outPath[0];
                if (path.EndsWith(Path.DirectorySeparatorChar.ToString()) || !Path.HasExtension(path))
                {
                    fileOutput = false;
                }
                else
                {
                    fileOutput = true;
                }
                if (truncate && fileOutput)
                {
                    Console.WriteLine("Can't output a single data track in truncated data mode.");
                    Console.WriteLine("Please provide two different output tracks.");
                    return false;
                }
                if (cdda.Count > 0 && fileOutput)
                {
                    Console.WriteLine("Can't output a single track when CDDA is specified.");
                    return false;
                }
            }
            return true;
        }

        // Get GDI or CDI file name from path
        public static string GetGDIPath(string path)
        {
            if (!Directory.Exists(path))
                return null;
            string[] gdiList = Directory.GetFiles(path, "*.gdi");
            string[] cueList = Directory.GetFiles(path, "*.cue");
            string[] gdis = gdiList.Length != 0 ? gdiList : cueList;
            if (gdis.Length == 0)
            {
                return null;
            }
            else if (gdis.Length > 1)
            {
                Logger.WriteMessage("Multiple GDI/CUE files found.");
                return null;
            }
            return gdis[0];
        }

    }
}