﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DreamcastImageBuilder.ClassesHelpers
{
    public static class Logger
    {
        static string StepTitle; // Progress step
        static TextWriter Log;

        public static void InitializeLogger(string logFile)
        {
            // Delete log if it exists
            if (File.Exists(logFile))
            {
                try
                {
                    File.Delete(logFile);
                }
                catch
                {
                    Console.WriteLine("Unable to delete log file.");
                    Log = null;
                }
            }
            // Create log
            try
            {
                Log = File.CreateText(logFile);
            }
            catch
            {
                Console.WriteLine("Unable to open log file.");
                Log = null;
            }
        }

        public static void WriteMessage(string message, bool display = true, bool file = true)
        {
            if (file)
            {
                if (Log != null)
                {
                    Log.WriteLine(message, display);
                    Log.Flush();
                }
            }
            if (display)
                Console.WriteLine(message);
        }

        public static void WriteStatusMessage(string message)
        {
            StepTitle = message;
        }

        public static void CloseLogger()
        {
            if (Log != null)
            {
                Log.Flush();
                Log.Close();
                Log = null;
            }
        }
    }
}