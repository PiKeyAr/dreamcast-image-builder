﻿// Just some tools to fix paths

namespace DreamcastImageBuilder.ClassesHelpers
{
    public static class Paths
    {
        public static string NormalizePath(string path)
        {
            if (path == null)
                return null;
            return path.Replace("/", "\\").Replace("\\\\", "\\").ToUpperInvariant();
        }
    }
}
