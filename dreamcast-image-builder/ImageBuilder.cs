﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DreamcastImageBuilder.ClassesHelpers;
using IniParser;
using IniParser.Model;

namespace DreamcastImageBuilder
{
    public static class ImageBuilder
    {
        // Enums
        
        public enum BuildStatus
        {
            Stopped,
            Building,
            Cancelling,
            Cancelled,
        }

        // Variables

        public static CancellationTokenSource tokenSource;

        public static bool autoRun; // Run the emulator on build finish
        public static Form sourceForm; // Form of DC Image Builder main window
        public static IniData settings; // Settings loaded from settings.ini

        public static BuildStatus status; // Current state of build, checked during some operations for cancelling

        public static string sourcePath; // Game GDI source folder
        public static string currentDirAss; // Image Builder EXE assembly location (for built-in mods/games)
        public static string emuPath;

        public static string workdir_main; // Output path
        private static string workdir_data; // Output path + "data" (used at buildtime)
        private static string workdir_data_orig;  // Output path + "data_orig" (used at buildtime)

        public static uint binhackOffset;
        public static uint binhackBuffer;
        public static bool regionHack;
        public static bool gdromProts;
        public static bool autoLBA;
        public static bool vga;

        public static bool pauseBeforeBuild;
        public static bool useCDI;

        public static bool keepOriginalDataFolder;
        public static bool keepWorkDataFolder;

        public static List<Mod> mods; // Mods for all games
        public static IniData codes; // codes.cht for current game

        public static List<Game> games; // All games
        public static List<GameConfig> gameConfigs; // Settings for all games
        public static List<string> xDeltas;
        public static Game currentGame; // Selected game
        public static Mod currentMod; // Selected mod

        public static List<string> activeMods; // Active mods for current game
        public static List<string> activeCodes; // Active codes for current game
        public static List<string> blacklistMods; // Mod INI files that are ignored due to an error etc.
        public static Dictionary<string, byte[]> filesToPatch = new(); // List of files+data to patch during build process

        public static void RefreshProgress(string step, bool console = true)
        {
            Logger.WriteStatusMessage(step);
            if (console)
            {
                Logger.WriteMessage("", true);
                Logger.WriteMessage(step, true);
            }
        }

        public static void EndBuild()
        {
            string codesOutPath = Path.Combine(workdir_main, "cheats.cht");
            if (status == BuildStatus.Cancelling)
            {
                Logger.WriteMessage("Cancelling build...", true, false);
                try
                {
                    // Delete all BIN files
                    string[] bins = Directory.GetFiles(workdir_main, "*.BIN", SearchOption.TopDirectoryOnly);
                    foreach (string file in bins)
                        File.Delete(file);
                    // Delete all RAW files
                    string[] raws = Directory.GetFiles(workdir_main, "*.raw", SearchOption.TopDirectoryOnly);
                    foreach (string file in raws)
                        File.Delete(file);
                    // Delete GDI file
                    bool deleted = File.Exists(Path.Combine(workdir_main, "disc.gdi")) || File.Exists(Path.Combine(workdir_main, "disc.cdi"));
                    if (!useCDI && File.Exists(Path.Combine(workdir_main, "disc.gdi")))
                        File.Delete(Path.Combine(workdir_main, "disc.gdi"));
                    // Delete CDI file
                    if (useCDI && File.Exists(Path.Combine(workdir_main, "disc.cdi")))
                        File.Delete(Path.Combine(workdir_main, "disc.cdi"));
                    // Delete ISO file
                    if (File.Exists(Path.Combine(workdir_main, "disc.iso")))
                        File.Delete(Path.Combine(workdir_main, "disc.iso"));
                    // Finish
                    if (deleted)
                        Logger.WriteMessage("Output image deleted.", true, true);
                }
                catch
                {
                    Logger.WriteMessage("Unable to remove output image.", true, true);
                }
            }
            else
            {
                // Step 5 - apply codes
                // Apply codes
                if (codes != null)
                {
                    RefreshProgress("Step 5/6: Applying codes");
                    for (int c = 0; c < 99; c++)
                    {
                        string keyName = "cheat" + c.ToString() + "_desc";
                        if (!codes.Global.ContainsKey(keyName))
                            break;
                        string codeDesc = codes.Global[keyName].Replace("\"", "");
                        foreach (string code in activeCodes)
                            if (codeDesc == code)
                            {
                                codes.Global["cheat" + c.ToString() + "_enable"] = "\"true\"";
                                Logger.WriteMessage(string.Format("Code: {0}", codeDesc));
                                continue;
                            }
                    }
                    var saveCodes = new FileIniDataParser();
                    saveCodes.WriteFile(codesOutPath, codes, Encoding.ASCII);
                }
            }
            // Step 6 - Cleanup
            if (status != BuildStatus.Cancelling)
                RefreshProgress("Step 6/6: Cleaning up");

            // Delete data_orig folder
            string directoryPath = Path.Combine(workdir_main, "data_orig");
            if (!keepOriginalDataFolder && Directory.Exists(directoryPath))
            {
                int retries = 0;
                while (true)
                    try
                    {
                        Directory.Delete(directoryPath, true);
                        Logger.WriteMessage("Original data folder deleted successfully.", false, true);
                        break;
                    }
                    catch (Exception ex)
                    {
                        if (retries > 19)
                        {
                            Logger.WriteMessage(string.Format("Could not delete original data folder: {0}", ex.Message.ToString()));
                            goto delete_data;
                        }
                        retries++;
                        Logger.WriteMessage(string.Format("Unable to delete original data folder. Trying again (attempt {0} of 20)", retries.ToString()));
                        Thread.Sleep(1000);
                    }
            }
        delete_data:
            // Delete data folder
            directoryPath = Path.Combine(workdir_main, "data");
            if (!keepWorkDataFolder && Directory.Exists(directoryPath))
            {
                int retries = 0;
                while (true)
                    try
                    {
                        File.Delete(Path.Combine(workdir_main, "IP_GDI.BIN"));
                        File.Delete(Path.Combine(workdir_main, "IP_CDI.BIN"));
                        Directory.Delete(directoryPath, true);
                        Logger.WriteMessage("Data folder deleted successfully.", false, true);
                        break;
                    }
                    catch (Exception ex)
                    {
                        if (retries > 19)
                        {
                            Logger.WriteMessage(string.Format("Could not delete data folder or IP.BIN: {0}", ex.Message.ToString()));
                            goto delete_end;
                        }
                        retries++;
                        Logger.WriteMessage(string.Format("Unable to delete data folder or IP.BIN. Trying again (attempt {0} of 20)", retries.ToString()), true, false);
                        Thread.Sleep(1000);
                    }
            }
        delete_end:
            Logger.WriteMessage("");
            if (status != BuildStatus.Cancelling)
            {
                Logger.WriteMessage(string.Format("The modified image is located at: {0}", workdir_main));
                if (File.Exists(codesOutPath))
                    Logger.WriteMessage(string.Format("Cheat file: {0}", codesOutPath));

                // Finish
                Logger.WriteMessage("DONE!\n");
            }
            else
                Logger.WriteMessage("Build cancelled");
            RefreshProgress("Click Build to rebuild a modified GDI or CDI image. Progress will be shown below.", false);
            if (status != BuildStatus.Cancelling && autoRun)
            {
                RunEmulator();
                autoRun = false;
            }
            status = BuildStatus.Cancelled;
            Logger.CloseLogger();
        }

        public static void RunEmulator()
        {
            TerminateEmu();
            Logger.WriteMessage("Running emulator...", true, false);
            string args;
            string imgname = useCDI ? "disc.cdi" : "disc.gdi";
            string emuname = Path.GetFileNameWithoutExtension(emuPath).ToLowerInvariant();
            switch (emuname)
            {
                case "flycast":
                case "redream":
                    args = "\"" + Path.Combine(workdir_main, imgname) + "\"";
                    break;
                case "demul":
                default:
                    args = "-run=dc -image=\"" + Path.Combine(workdir_main, imgname) + "\"";
                    break;
            }
            try
            {
                _ = ProcessStuff.StartProcess(emuPath, args, new CancellationTokenSource(), Path.GetDirectoryName(emuname), null, null, null);
            }
            catch (TaskCanceledException)
            {
                Logger.WriteMessage("Process killed: " + emuname, true, false);
            }
        }

        public static async void StartBuildAsync()
        {
            status = BuildStatus.Building;
            workdir_data = Path.Combine(workdir_main, "data");
            workdir_data_orig = Path.Combine(workdir_main, "data_orig");

            // Create output folder
            Directory.CreateDirectory(workdir_main);

            // Logger
            Logger.InitializeLogger(Path.Combine(workdir_main, "BuildLog.txt"));

            // Update UI
            RefreshProgress("Step 1/6: Extracting original data");

            // Clean working folder
            DeleteDataFolder();

            if (status == BuildStatus.Cancelling)
            {
                //Console.WriteLine("1");
                EndBuild();
                return;
            }

            // Reuse data_orig folder if it exists
            if (Directory.Exists(workdir_data_orig))
            {
                Logger.WriteMessage("Original data folder found, copying files...", true);
                // Copy files to the working folder
                CopyDirectory(workdir_data_orig, workdir_data, true);
                if (status == BuildStatus.Building)
                    Logger.WriteMessage("Files copied");
                else
                    DeleteDataFolder();
            }
            // Otherwise extract the GDI
            else
            {
                Directory.CreateDirectory(workdir_data);
                string gdi = GDITools.GetGDIPath(sourcePath);
                if (string.IsNullOrEmpty(gdi))
                {
                    Logger.WriteMessage("GDI/CUE file not found! Aborting.", true);
                    EndBuild();
                    return;
                }
                try
                {
                    Logger.WriteMessage(string.Format("Extracting {0}...", gdi));
                    GDITools.ExtractGDI(gdi, workdir_data);
                }
                catch (Exception ex)
                {
                    Logger.WriteMessage(string.Format("Error extracting GDI: " + ex.ToString()), true);
                }
                if (status == BuildStatus.Cancelling)
                {
                    Logger.WriteMessage("GDI extraction cancelled");
                    DeleteDataFolder();
                    EndBuild();
                    return;
                }
            }

            // Get volume label from the GDI
            string volumelabel = GDITools.GetVolumeLabel(GDITools.GetGDIPath(sourcePath));

            // If the orig data folder doesn't exist, create it and copy files there for next use
            if (!Directory.Exists(workdir_data_orig))
            {
                // Copy files to the original data folder to reduce build time next time
                Logger.WriteMessage("Creating original data folder");
                CopyDirectory(workdir_data, workdir_data_orig, true);
            }

            if (status == BuildStatus.Cancelling)
            {
                //Console.WriteLine("3");
                DeleteDataFolderOrig();
                EndBuild();
                return;
            }

            // Check if data extracted correctly (1ST_READ.BIN or 0WINCEOS.BIN exists)
            if (!File.Exists(Path.Combine(workdir_data, "1ST_READ.BIN")) && !File.Exists(Path.Combine(workdir_data, "0WINCEOS.BIN")))
            {
                Logger.WriteMessage("Error: Startup binary file not found");
                status = BuildStatus.Cancelled;
                return;
            }

            if (status == BuildStatus.Cancelling)
            {
                //Console.WriteLine("5");
                EndBuild();
                return;
            }

            // Update UI
            RefreshProgress("Step 2/6: Patching files for mods");

            // Select active mods to apply
            List<Mod> applyMods = new List<Mod>();
            StringBuilder activeModsListSb = new();
            activeModsListSb.Append(Environment.NewLine);
            for (int u = 0; u < activeMods.Count; u++)
            {
                for (int i = 0; i < mods.Count; i++)
                {
                    if (mods[i].Name == activeMods[u] && mods[i].GameVersions.Contains(currentGame.GameTitle))
                    {
                        activeModsListSb.Append(mods[i].Name + Environment.NewLine);
                        List<Mod> subMods = GetActiveSubmods(mods[i]);
                        foreach (Mod smod in subMods)
                            activeModsListSb.Append(smod.Name + Environment.NewLine);
                        applyMods.Add(mods[i]);
                        applyMods.AddRange(subMods);
                        break;
                    }
                }
            }
            Logger.WriteMessage(string.Format("Active mods: {0}", activeModsListSb.ToString()));

            if (status == BuildStatus.Cancelling)
            {
                //Console.WriteLine("4");
                EndBuild();
                return;
            }

            bool editedIP = false; // True if mods edit IP.BIN

            // Make a list of files/data changed by mods
            Logger.WriteMessage("Listing files to be modified...");
            foreach (Mod mod in applyMods)
            {
                if (File.Exists(Path.Combine(mod.ModPath, "IP.BIN")))
                {
                    Logger.WriteMessage(string.Format("WARNING! Mod '{0}' makes changes to IP.BIN", mod.Name));
                    File.Copy(Path.Combine(mod.ModPath, "IP.BIN"), Path.Combine(workdir_main, "IP_GDI.BIN"), true);
                    editedIP = true;
                }
                if (mod.Xdeltas != null)
                {
                    foreach (string xdelta in mod.Xdeltas)
                    {
                        if (status == BuildStatus.Cancelling)
                        {
                            EndBuild();
                            return;
                        }
                        Logger.WriteMessage("Adding xdelta patch for " + xdelta.Substring(0, xdelta.Length - 7) + "...");
                        try
                        {
                            string deltaFile = Path.Combine(mod.ModPath, "gdroot", xdelta);
                            string sourceFile = Path.Combine(workdir_data, xdelta.Substring(0, xdelta.Length - 7));
                            string destFile = Path.Combine(workdir_main, "tempxdelta.bin");
                            if (!File.Exists(deltaFile))
                            {
                                Logger.WriteMessage("Xdelta patch missing:" + deltaFile);
                                continue;
                            }
                            if (!File.Exists(sourceFile))
                            {
                                Logger.WriteMessage("Xdelta source file missing:" + sourceFile);
                                continue;
                            }
                            StringBuilder args = new StringBuilder();
                            args.Append("-d -f -s");
                            args.Append(" \"" + sourceFile + "\"");
                            args.Append(" \"" + deltaFile + "\"");
                            args.Append(" \"" + destFile + "\"");
                            Task tc = ProcessStuff.StartProcess(Path.Combine(currentDirAss, "utils", "xdelta3.exe"), args.ToString(), new CancellationTokenSource(), Path.Combine(currentDirAss, "utils"), null, Console.Out, Console.Out, showargs: false);
                            await tc;
                            if (File.Exists(destFile))
                            {
                                File.Copy(destFile, sourceFile, true);
                                File.Delete(destFile);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteMessage("Error applying xdelta patch: " + ex.Message.ToString());
                            break;
                        }
                    }
                }
                foreach (PatchData patch in mod.Patches)
                {
                    if (!File.Exists(Path.Combine(workdir_data, patch.Filename)))
                    {
                        Logger.WriteMessage(string.Format("File doesn't exist: {0}", Path.Combine(workdir_data, patch.Filename)));
                        continue;
                    }
                    if (patch.Type == PatchData.PatchType.Error)
                        continue;
                    if (filesToPatch.ContainsKey(patch.Filename))
                        continue;
                    if (status == BuildStatus.Cancelling)
                    {
                        //Console.WriteLine("6");
                        EndBuild();
                        return;
                    }
                    byte[] fileData = File.ReadAllBytes(Path.Combine(workdir_data, patch.Filename));
                    if (Path.GetExtension(patch.Filename).ToLowerInvariant() == ".prs")
                        fileData = PSO.PRS.PRS.Decompress(fileData);
                    filesToPatch.Add(patch.Filename, fileData);
                    Logger.WriteMessage(string.Format("Added file {0}", patch.Filename));
                }
            }
            Logger.WriteMessage(Environment.NewLine + string.Format("Applying mods..."));
            // Apply mods
            bool errors = false;
            foreach (Mod mod in applyMods)
            {
                if (status == BuildStatus.Cancelling)
                {
                    //Console.WriteLine("7");
                    EndBuild();
                    return;
                }
                Logger.WriteMessage(string.Format("Applying mod: {0}", mod.Name));
                Mod.ModResult result = mod.Apply(workdir_data, filesToPatch, out string msg);
                Logger.WriteMessage(msg);
                if (result == Mod.ModResult.Error)
                {
                    errors = true;
                    Logger.WriteMessage(string.Format("Error installing mod {0}", mod.Name));
                }
            }
            // Apply CDI patches
            if (useCDI)
            {
                if (currentGame.PatchesCDI != null && currentGame.PatchesCDI.Count > 0)
                {
                    Logger.WriteMessage("Applying CDI patches...");
                    foreach (PatchData cdiPatchData in currentGame.PatchesCDI)
                    {
                        cdiPatchData.Apply(workdir_data, filesToPatch, out string cdiMsg);
                        Logger.WriteMessage(cdiMsg);
                    }
                }
            }
            if (errors)
            {
                Logger.WriteMessage("Error applying mods. Check the log and try again.");
                EndBuild();
                return;
            }

            // Recompress PRS files
            Logger.WriteMessage("Finalizing changes...");
            foreach (KeyValuePair<string, byte[]> filed in filesToPatch)
            {
                if (status == BuildStatus.Cancelling)
                {
                    //Console.WriteLine("8");
                    EndBuild();
                    return;
                }
                bool prs = Path.GetExtension(filed.Key).ToLowerInvariant() == ".prs";
                Logger.WriteMessage(string.Format("Writing{0} file: {1}...", prs ? " compressed" : "", filed.Key), true);
                byte[] writeData = filed.Value;
                string fileFullPath = Path.Combine(workdir_data, filed.Key);
                if (prs)
                    writeData = PSO.PRS.PRS.Compress(writeData, 255);
                File.WriteAllBytes(fileFullPath, writeData);
            }

            // Copy orig tracks
            RefreshProgress("Step 3/6: Copying original data tracks");
            if (status == BuildStatus.Cancelling)
            {
                //Console.WriteLine("9");
                EndBuild();
                return;
            }
            if (!useCDI)
            {
                string gdi = GDITools.GetGDIPath(sourcePath);
                if (Path.GetExtension(gdi).ToLowerInvariant() == ".cue")
                {
                    CueTools.ConvertCUEtoGDI(gdi, workdir_main, true);
                }
                else
                {
                    Logger.WriteMessage(string.Format("Copying GDI file {0} as disc.gdi", gdi));
                    GDITools.GDIFile gdiFile = new GDITools.GDIFile(gdi, false);
                    gdiFile.Save(Path.Combine(workdir_main, "disc.gdi"), true);
                    foreach (var track in gdiFile.Tracks)
                    {
                        if (status == BuildStatus.Cancelling)
                        {
                            //Console.WriteLine("9");
                            EndBuild();
                            return;
                        }
                        string originalFileSrc = Path.Combine(sourcePath, track.TrackFilename);
                        string trackExt = (track.TrackType == 4) ? ".bin" : ".raw";
                        string originalFileDst = "track" + track.TrackNumber.ToString("D2") + trackExt;
                        if (track.TrackNumber < gdiFile.Tracks.Count) // Skip last track because it's built by GDBuilder
                        {
                            Logger.WriteMessage(string.Format("Copying track {0}: {1} as {2}", track.TrackNumber, track.TrackFilename, originalFileDst));
                            File.Copy(originalFileSrc, Path.Combine(workdir_main, originalFileDst), true);
                        }
                    }
                }
                if (status == BuildStatus.Cancelling)
                {
                    //Console.WriteLine("9");
                    EndBuild();
                    return;
                }
            }
            else
                Logger.WriteMessage("Not required for CDI");

            // Next step
            // Variables
            string pfilename; // Process filename
            StringBuilder pargs = new StringBuilder(); // Command line argument string
            bool cdi4dc = false; // Whether to run cdi4dc or mkcdi (cdi4dc can build LBA 0 but not 45000, mkcdi is the other way around)

            // Update UI
            RefreshProgress("Step 4/6: Building a modified image, mode: " + (useCDI ? "CDI" : "GDI"));

            // Stop emulator if running
            TerminateEmu();

            // IP_GDI.BIN must be present to build GDI or CDI
            if (!editedIP || !File.Exists(Path.Combine(workdir_main, "IP_GDI.BIN")))
            {
                // Extract IP.BIN from the GDI
                Logger.WriteMessage("Extracting IP.BIN from the GDI file...");
                string gdi = GDITools.GetGDIPath(sourcePath);
                if (string.IsNullOrEmpty(gdi))
                {
                    Logger.WriteMessage("GDI file not found! Aborting.", true);
                    EndBuild();
                    return;
                }
                try
                {
                    GDITools.ExtractIP(gdi, workdir_main);
                }
                catch (Exception ex)
                {
                    Logger.WriteMessage(string.Format("Error extracting IP.BIN: " + ex.Message.ToString()), true);
                }
            }
        
            // Build CDI
            if (useCDI)
            {
                // Delete old sort file
                if (File.Exists(Path.Combine(workdir_main, "sortfile.str")))
                {
                    Logger.WriteMessage("Deleting old sort file");
                    File.Delete(Path.Combine(workdir_main, "sortfile.str"));
                }
                // Copy new sort file
                string sortfile = Path.Combine(currentGame.Location, currentGame.IniName + ".str");
                if (File.Exists(sortfile))
                {
                    File.Copy(sortfile, Path.Combine(workdir_main, "sortfile.str"), true);
                    Logger.WriteMessage(string.Format("Sort file {0} copied", Path.GetFileName(sortfile)));
                }

                if (status == BuildStatus.Cancelling)
                {
                    //Console.WriteLine("10");
                    EndBuild();
                    return;
                }

                Logger.WriteMessage("");

                // Create a CDI version of IP.BIN
                File.Copy(Path.Combine(workdir_main, "IP_GDI.BIN"), Path.Combine(workdir_main, "IP_CDI.BIN"), true);

                // Cut 0x800 bytes from 0WINCEOS.BIN and set flag in IP.BIN
                if (File.Exists(Path.Combine(workdir_data, "0WINCEOS.BIN")))
                {
                    Logger.WriteMessage("Cutting the first 0x800 bytes from 0WINCEOS.BIN...");
                    byte[] wincebytes_old = File.ReadAllBytes(Path.Combine(workdir_data, "0WINCEOS.BIN"));
                    byte[] wincebytes_new = new byte[wincebytes_old.Length - 0x800];
                    Array.Copy(wincebytes_old, 0x800, wincebytes_new, 0, wincebytes_old.Length - 0x800);
                    File.WriteAllBytes(Path.Combine(workdir_data, "0WINCEOS.BIN"), wincebytes_new);
                    byte[] ipBin = File.ReadAllBytes(Path.Combine(workdir_main, "IP_CDI.BIN"));
                    ipBin[0x3E] = 0x30;
                    File.WriteAllBytes(Path.Combine(workdir_main, "IP_CDI.BIN"), ipBin);
                    Logger.WriteMessage("IP.BIN flag updated");
                }

                // Retrieve 1ST_READ.BIN size (needed for selfboot hack)
                string binpath = Path.Combine(workdir_data, "1ST_READ.BIN");
                if (!File.Exists(binpath))
                    binpath = Path.Combine(workdir_data, "0WINCEOS.BIN");
                long freadsize = new FileInfo(binpath).Length;
                Logger.WriteMessage(string.Format("Boot binary size: {0} (0x{1}) bytes", freadsize.ToString(), freadsize.ToString("X")));
                Logger.WriteMessage("Selfboot LBA: " + currentGame.BootLBA.ToString());

                // Hack IP.BIN for CDI
                Logger.WriteMessage(string.Format("Injecting selfboot code into IP.BIN: offset {0}, buffer addresses: {1} and {2}",
                    binhackOffset.ToString("X"),
                    binhackBuffer.ToString("X"),
                    (binhackBuffer + 0x20000).ToString("X")));
                BinHack.InsertSelfBoot(Path.Combine(workdir_main, "IP_CDI.BIN"), (int)freadsize, binhackOffset, binhackBuffer);

                // Region and VGA patches
                if (regionHack || vga)
                {
                    byte[] ipbin = File.ReadAllBytes(Path.Combine(workdir_main, "IP_CDI.BIN"));
                    if (regionHack)
                    {
                        Logger.WriteMessage("Applying region patch to IP.BIN...");
                        BinHack.RegionHack(ipbin);
                    }
                    if (vga)
                    {
                        Logger.WriteMessage("Setting VGA flag in IP.BIN...");
                        ipbin[0x3D] = 0x31;
                    }
                    File.WriteAllBytes(Path.Combine(workdir_main, "IP_CDI.BIN"), ipbin);
                }

                // Hack common GDROM protections
                if (gdromProts)
                {
                    byte[] fstread = File.ReadAllBytes(binpath);
                    BinHack.GdromHack(fstread);
                    File.WriteAllBytes(binpath, fstread);
                }
                // Hack LBA
                if (autoLBA)
                {
                    byte[] fstread = File.ReadAllBytes(binpath);
                    BinHack.LbaHack(fstread, (ushort)currentGame.BootLBA);
                    File.WriteAllBytes(binpath, fstread);
                }

                if (pauseBeforeBuild)
                    MessageBox.Show(sourceForm, "You can now make final changes to game data or IP.BIN.\nThe image will be built after you click the OK button.", "Dreamcast Image Builder", MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Create CDI
                switch (currentGame.BootLBA)
                {
                    case 11700:
                    case 11702:
                        // Info from: https://dcemulation.org/phpBB/viewtopic.php?style=38&p=1035005#p1035005
                        // Selfboot method 1: Classic MIL-CD hack (AUDIO/DATA mode, LBA 11700/11702).
                        // Writes a fake 300 or 302 sectors audio track in Session 1 (low density area).
                        // Writes the data in Session 2 (high density area) starting at LBA 11700/11702.
                        // Pros:
                        // 1) Classic and tested method, many games have been cracked with this;
                        // 2) Compatible with CDDA and WINCE?
                        // 3) Seemed to boot better than DATA-DATA on my DC before it broke, exact opposite of the reports I've read.
                        // Cons:
                        // 1) LBA hacks required, not all games are compatible with them or need even more hacks;
                        // 2) Method may not work on some models of the Dreamcast.
                        // Implementation: 1) Create an ISO with mkisofs with LBA 11702 + IP.BIN, 2) cdi4dc without the -d option to build with LBA 11702.
                        cdi4dc = true;
                        break;
                    case 0:
                        // Selfboot method 2: DATA/DATA mode, LBA 0 (Xeal/img4dc)
                        // All data is written in Session 1 (low density area).
                        // Session 2 contains nothing but IP.BIN.
                        // Pros
                        // Cons:
                        // 1) LBA hacks required, not all games are compatible with them or need even more hacks;
                        // 2) Method may not work on some models of the Dreamcast (not sure about this).
                        // Implementation:
                        // 1) Create an ISO with mkisofs with LBA 0 + IP.BIN, 2) cdi4dc with the -d option to build with LBA 0.
                        cdi4dc = true;
                        break;
                    case 45000:
                        // Selfboot method 3: DATA/DATA mode, LBA 45000 (FamilyGuy selfboot pack)
                        // Writes parts of game data to Session 1 (low density area).
                        // Writes most of game data to Session 2 (high density area).
                        // Pros: 
                        // 1) LBA hacks not required
                        // 2) Can fit more data on the disc if Session 1 is used for parts of game data.
                        // Cons:
                        // 1) Need to pick data for Session 1 manually (not supported here) or waste disc space.
                        // Implementation: mkcdi with -l 45000
                        cdi4dc = false;
                        break;
                }
                // Create ISO using mkisofs
                Logger.WriteMessage("Creating ISO...");
                pfilename = "mkisofs.exe";
                pargs.Clear();
                pargs.Append("-C 0," + currentGame.BootLBA.ToString()); // LBA
                pargs.Append(" -G \"" + Path.Combine(workdir_main, "IP_CDI.BIN") + "\""); // Boot sector
                pargs.Append(" -V " + volumelabel + " -joliet -rock -l"); // Volume label and stuff (unnecessary?)
                if (File.Exists(Path.Combine(workdir_main, "sortfile.str")))
                    pargs.Append(" -sort " + "\"" + Path.Combine(workdir_main, "sortfile.str") + "\""); // Sort file
                pargs.Append(" -o " + "\"" + Path.Combine(workdir_main, "disc.iso") + "\" "); // Output ISO filename
                pargs.Append("\"" + workdir_data + "\""); // Input folder
                try
                {
                    Task f = ProcessStuff.StartProcess(Path.Combine(currentDirAss, "utils", pfilename), pargs.ToString(), new CancellationTokenSource(), Path.Combine(currentDirAss, "utils"), null, Console.Out, Console.Out);
                    f.Wait();
                    Logger.WriteMessage(string.Format("Process {0} exited", pfilename));
                }
                catch (TaskCanceledException)
                {
                    Logger.WriteMessage(string.Format("Process {0} killed", pfilename));
                }
                if (status == BuildStatus.Cancelling)
                {
                    //Console.WriteLine("10");
                    EndBuild();
                    return;
                }

                // Create CDI from ISO
                Logger.WriteMessage("");
                Logger.WriteMessage("Creating CDI...");
                // Using cdi4dc in case of LBA 0
                if (cdi4dc)
                {
                    pfilename = "cdi4dc.exe";
                    pargs.Clear();
                    pargs.Append("\"" + Path.Combine(workdir_main, "disc.iso") + "\" ");
                    pargs.Append("\"" + Path.Combine(workdir_main, "disc.cdi") + "\" ");
                    pargs.Append(currentGame.BootLBA == 0 ? "-d" : "");
                    try
                    {
                        Task f = ProcessStuff.StartProcess(Path.Combine(currentDirAss, "utils", pfilename), pargs.ToString(), new CancellationTokenSource(), Path.Combine(currentDirAss, "utils"), null, TextWriter.Null, TextWriter.Null);
                        f.Wait();
                        Logger.WriteMessage(string.Format("Process {0} exited", pfilename));
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.WriteMessage(string.Format("Process {0} killed", pfilename));
                    }
                    if (status == BuildStatus.Cancelling)
                    {
                        //Console.WriteLine("10");
                        EndBuild();
                        return;
                    }
                }
                // Using mkcdi in other cases
                else
                {
                    CDITools.CreateCdiImage(Path.Combine(workdir_main, "disc.iso"), Path.Combine(workdir_main, "disc.cdi"), currentGame.BootLBA);
                    if (status == BuildStatus.Cancelling)
                    {
                        //Console.WriteLine("10");
                        EndBuild();
                        return;
                    }
                }
                // Check if the CDI was created successfully
                if (File.Exists(Path.Combine(workdir_main, "disc.cdi")))
                {
                    if (File.Exists(Path.Combine(workdir_main, "disc.iso")))
                    {
                        Logger.WriteMessage("Deleting ISO...");
                        File.Delete(Path.Combine(workdir_main, "disc.iso"));
                    }
                }
                else
                {
                    Logger.WriteMessage("CDI creation failed as the CDI file doesn't exist.");
                    EndBuild();
                    return;
                }
            }
            // Build GDI
            else
            {
                if (pauseBeforeBuild)
                    MessageBox.Show(sourceForm, "You can now make final changes to game data or IP.BIN.\nThe image will be built after you click the OK button.", "Dreamcast Image Builder", MessageBoxButtons.OK, MessageBoxIcon.Information);

                if (status == BuildStatus.Cancelling)
                {
                    //Console.WriteLine("11");
                    EndBuild();
                    return;
                }
                // Set arguments for buildGDI
                string g_data = workdir_data;
                string g_ip = Path.Combine(workdir_main, "IP_GDI.BIN");
                string g_volume = currentGame.IniName;
                List<string> g_outPath = new List<string>();
                List<string> g_cdda = new List<string>();
                g_outPath.Add(Path.Combine(workdir_main, "track03.bin"));
                bool g_raw = true;
                string g_gdi = Path.Combine(workdir_main, "disc.gdi");
                if (currentGame.NumTracks > 3)
                {
                    g_outPath.Add(Path.Combine(workdir_main, "track" + currentGame.NumTracks.ToString("D2") + ".bin"));
                    for (int t = 4; t < currentGame.NumTracks; t++)
                    {
                        string tname = "track" + t.ToString("D2") + ".raw";
                        g_cdda.Add(Path.Combine(workdir_main, tname));
                    }
                }
                // Run buildGDI
                Logger.WriteMessage("Creating GDI...");
                tokenSource = new CancellationTokenSource();
                GDITools.CreateGDI(g_data, g_ip, g_outPath, g_cdda, g_gdi, g_volume, g_raw, false, tokenSource);
            }
            EndBuild();
        }

        public static List<Mod> GetActiveSubmods(Mod mod)
        {
            if (mod.SubMods == null || mod.SubMods.Count < 1)
                return new List<Mod>();

            bool allmods = false; // If true, enable all submods
            List<Mod> result = new(); // List of active submods (final)
            List<string> configmods = new(); // List of active submods according to settings.ini or mod.ini's defaults

            // If settings.ini has an entry for the current mod's submods, get the list of active submods from there
            if (settings["Submods"] != null && settings["Submods"].GetKeyData(mod.Name) != null)
                configmods = settings["Submods"].GetKeyData(mod.Name).Value.Split(',').ToList();

            // Otherwise check if the mod has a list of default active submods
            else if (mod.DefaultSubMods != null)
                configmods = mod.DefaultSubMods;

            // If that also fails, enable all submods
            else
                allmods = true;

            // Load each submod and check whether its name is on the active submods list
            foreach (string subModRelPath in mod.SubMods)
            {
                string subModIniPath = Path.Combine(mod.ModPath, subModRelPath);
                if (File.Exists(subModIniPath))
                {
                    Mod subMod = new Mod(subModIniPath);
                    if (allmods || configmods.Contains(subMod.Name))
                        result.Add(subMod);
                }
            }
            return result;
        }

        static void CopyDirectory(string sourceDir, string destinationDir, bool recursive)
        {
            // https://learn.microsoft.com/en-us/dotnet/standard/io/how-to-copy-directories

            // Cancel if required
            if (ImageBuilder.status == BuildStatus.Cancelling)
                return;

            // Get information about the source directory
            var dir = new DirectoryInfo(sourceDir);

            // Check if the source directory exists
            if (!dir.Exists)
                throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

            // Cache directories before we start copying
            DirectoryInfo[] dirs = dir.GetDirectories();

            // Create the destination directory
            Directory.CreateDirectory(destinationDir);

            // Get the files in the source directory and copy to the destination directory
            foreach (FileInfo file in dir.GetFiles())
            {
                if (ImageBuilder.status == BuildStatus.Cancelling)
                    return;
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
                Logger.WriteMessage(string.Format("Copying file {0}", file.Name), false);
            }

            // If recursive and copying subdirectories, recursively call this method
            if (recursive)
            {
                foreach (DirectoryInfo subDir in dirs)
                {
                    if (ImageBuilder.status == BuildStatus.Cancelling)
                        return;
                    string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                    CopyDirectory(subDir.FullName, newDestinationDir, true);
                }
            }
        }

        // Kill emulator process
        public static void TerminateEmu()
        {
            if (string.IsNullOrEmpty(emuPath))
                return;
            string emuname = Path.GetFileNameWithoutExtension(emuPath).ToLowerInvariant();
            if (!string.IsNullOrEmpty(emuname))
                ProcessStuff.TerminateProcess(emuname);
        }

        private static void DeleteDataFolderOrig()
        {
            Logger.WriteMessage("Cleaning up the original data folder...");
            DeleteFolder(workdir_data_orig);
        }

        private static void DeleteDataFolder()
        {
            Logger.WriteMessage("Cleaning up the data folder...");
            DeleteFolder(workdir_data);
        }

        private static void DeleteFolder(string folder)
        {
            if (Directory.Exists(folder))
            {
                goto trydelete;
            trydelete:
                try
                {
                    Directory.Delete(folder, true);
                    Logger.WriteMessage("Folder deleted");
                }
                catch
                {
                    DialogResult res = MessageBox.Show(sourceForm, "Unable to clean up the data folder at " + folder + ". Try again?", "Dreamcast Image Builder", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
                    switch (res)
                    {
                        case DialogResult.Retry:
                            goto trydelete;
                        case DialogResult.Abort:
                            Logger.WriteMessage(string.Format("Unable to clean up data folder {0}", folder), true);
                            EndBuild();
                            return;
                        case DialogResult.Ignore:
                            break;
                    }
                }
            }
        }
    }
}