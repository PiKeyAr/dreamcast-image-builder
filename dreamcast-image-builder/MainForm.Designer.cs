﻿
namespace DreamcastImageBuilder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            modDescription = new System.Windows.Forms.TextBox();
            tabControl1 = new System.Windows.Forms.TabControl();
            tabMods = new System.Windows.Forms.TabPage();
            buttonDeleteMod = new System.Windows.Forms.Button();
            buttonNewMod = new System.Windows.Forms.Button();
            buttonModConfig = new System.Windows.Forms.Button();
            buttonBottom = new System.Windows.Forms.Button();
            buttonDown = new System.Windows.Forms.Button();
            buttonUp = new System.Windows.Forms.Button();
            buttonTop = new System.Windows.Forms.Button();
            modListView = new System.Windows.Forms.ListView();
            columnName = new System.Windows.Forms.ColumnHeader();
            columnAuthor = new System.Windows.Forms.ColumnHeader();
            columnVersion = new System.Windows.Forms.ColumnHeader();
            columnChecked = new System.Windows.Forms.ColumnHeader();
            columnBuiltIn = new System.Windows.Forms.ColumnHeader();
            tabCodes = new System.Windows.Forms.TabPage();
            codesListView = new System.Windows.Forms.ListView();
            tabGames = new System.Windows.Forms.TabPage();
            buttonCopyGameID = new System.Windows.Forms.Button();
            buttonGameEdit = new System.Windows.Forms.Button();
            buttonGameRemove = new System.Windows.Forms.Button();
            buttonGameAdd = new System.Windows.Forms.Button();
            labelSelectGame = new System.Windows.Forms.Label();
            listBoxGames = new System.Windows.Forms.ListBox();
            labelGameCheckResult = new System.Windows.Forms.Label();
            buttonBrowse = new System.Windows.Forms.Button();
            textBoxOriginalPath = new System.Windows.Forms.TextBox();
            labelSelectnputPath = new System.Windows.Forms.Label();
            labelSelectOutput = new System.Windows.Forms.Label();
            buttonBrowseOutput = new System.Windows.Forms.Button();
            textBoxOutputPath = new System.Windows.Forms.TextBox();
            tabOptions = new System.Windows.Forms.TabPage();
            groupBoxUpdate = new System.Windows.Forms.GroupBox();
            buttonCheckUpdateNow = new System.Windows.Forms.Button();
            checkBoxCheckForUpdates = new System.Windows.Forms.CheckBox();
            groupBoxBinhackSettings = new System.Windows.Forms.GroupBox();
            checkBoxBinhackAutoLBA = new System.Windows.Forms.CheckBox();
            checkBoxBinhackVGA = new System.Windows.Forms.CheckBox();
            checkBoxBinhackCommonProts = new System.Windows.Forms.CheckBox();
            checkBoxBinhackRegion = new System.Windows.Forms.CheckBox();
            buttonBinhackReset = new System.Windows.Forms.Button();
            buttonCopyBinhackRam = new System.Windows.Forms.Button();
            labelBinhackRam = new System.Windows.Forms.Label();
            labelBinhackUsed = new System.Windows.Forms.Label();
            numericUpDownBinhackBuffer = new System.Windows.Forms.NumericUpDown();
            labelDefaultBinhackBuffer = new System.Windows.Forms.Label();
            labelDefaultBinhackOffset = new System.Windows.Forms.Label();
            numericUpDownBinhackOffset = new System.Windows.Forms.NumericUpDown();
            groupBoxBuildSettings = new System.Windows.Forms.GroupBox();
            checkBoxKeepOrigDataFolder = new System.Windows.Forms.CheckBox();
            checkBoxKeepWorkDataFolder = new System.Windows.Forms.CheckBox();
            checkBoxAddGameName = new System.Windows.Forms.CheckBox();
            buttonBrowseDefaultOutput = new System.Windows.Forms.Button();
            checkBoxPauseBeforeBuild = new System.Windows.Forms.CheckBox();
            labelDefaultOutput = new System.Windows.Forms.Label();
            textBoxDefaultOutput = new System.Windows.Forms.TextBox();
            groupBoxEmulatorSettings = new System.Windows.Forms.GroupBox();
            radioButtonEmulator1 = new System.Windows.Forms.RadioButton();
            radioButtonEmulator2 = new System.Windows.Forms.RadioButton();
            labelEmu1Location = new System.Windows.Forms.Label();
            textBoxEmuLocation = new System.Windows.Forms.TextBox();
            buttonBrowseEmulator = new System.Windows.Forms.Button();
            tabBuild = new System.Windows.Forms.TabPage();
            labelStatus = new System.Windows.Forms.Label();
            textBoxLog = new System.Windows.Forms.TextBox();
            buttonBuild = new System.Windows.Forms.Button();
            buttonSaveSettings = new System.Windows.Forms.Button();
            timer1 = new System.Windows.Forms.Timer(components);
            buttonRunEmu = new System.Windows.Forms.Button();
            buttonBuildRun = new System.Windows.Forms.Button();
            buttonReload = new System.Windows.Forms.Button();
            buttonStop = new System.Windows.Forms.Button();
            checkBoxCDI = new System.Windows.Forms.CheckBox();
            tabControl1.SuspendLayout();
            tabMods.SuspendLayout();
            tabCodes.SuspendLayout();
            tabGames.SuspendLayout();
            tabOptions.SuspendLayout();
            groupBoxUpdate.SuspendLayout();
            groupBoxBinhackSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDownBinhackBuffer).BeginInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDownBinhackOffset).BeginInit();
            groupBoxBuildSettings.SuspendLayout();
            groupBoxEmulatorSettings.SuspendLayout();
            tabBuild.SuspendLayout();
            SuspendLayout();
            // 
            // modDescription
            // 
            modDescription.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            modDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            modDescription.Location = new System.Drawing.Point(0, 359);
            modDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            modDescription.Multiline = true;
            modDescription.Name = "modDescription";
            modDescription.ReadOnly = true;
            modDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            modDescription.Size = new System.Drawing.Size(527, 144);
            modDescription.TabIndex = 4;
            modDescription.TabStop = false;
            modDescription.Text = "Mod description.";
            // 
            // tabControl1
            // 
            tabControl1.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            tabControl1.Controls.Add(tabMods);
            tabControl1.Controls.Add(tabCodes);
            tabControl1.Controls.Add(tabGames);
            tabControl1.Controls.Add(tabOptions);
            tabControl1.Controls.Add(tabBuild);
            tabControl1.Location = new System.Drawing.Point(9, 9);
            tabControl1.Margin = new System.Windows.Forms.Padding(2);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new System.Drawing.Size(540, 537);
            tabControl1.TabIndex = 0;
            // 
            // tabMods
            // 
            tabMods.BackColor = System.Drawing.SystemColors.Control;
            tabMods.Controls.Add(buttonDeleteMod);
            tabMods.Controls.Add(buttonNewMod);
            tabMods.Controls.Add(buttonModConfig);
            tabMods.Controls.Add(buttonBottom);
            tabMods.Controls.Add(buttonDown);
            tabMods.Controls.Add(buttonUp);
            tabMods.Controls.Add(buttonTop);
            tabMods.Controls.Add(modListView);
            tabMods.Controls.Add(modDescription);
            tabMods.Location = new System.Drawing.Point(4, 24);
            tabMods.Margin = new System.Windows.Forms.Padding(2);
            tabMods.Name = "tabMods";
            tabMods.Padding = new System.Windows.Forms.Padding(2);
            tabMods.Size = new System.Drawing.Size(532, 509);
            tabMods.TabIndex = 0;
            tabMods.Text = "Mods";
            // 
            // buttonDeleteMod
            // 
            buttonDeleteMod.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonDeleteMod.Enabled = false;
            buttonDeleteMod.Location = new System.Drawing.Point(497, 322);
            buttonDeleteMod.Name = "buttonDeleteMod";
            buttonDeleteMod.Size = new System.Drawing.Size(30, 30);
            buttonDeleteMod.TabIndex = 13;
            buttonDeleteMod.Text = "-";
            buttonDeleteMod.UseVisualStyleBackColor = true;
            buttonDeleteMod.Click += buttonDeleteMod_Click;
            // 
            // buttonNewMod
            // 
            buttonNewMod.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonNewMod.Location = new System.Drawing.Point(497, 286);
            buttonNewMod.Name = "buttonNewMod";
            buttonNewMod.Size = new System.Drawing.Size(30, 30);
            buttonNewMod.TabIndex = 12;
            buttonNewMod.Text = "+";
            buttonNewMod.UseVisualStyleBackColor = true;
            buttonNewMod.Click += buttonNewMod_Click;
            // 
            // buttonModConfig
            // 
            buttonModConfig.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonModConfig.Location = new System.Drawing.Point(497, 149);
            buttonModConfig.Name = "buttonModConfig";
            buttonModConfig.Size = new System.Drawing.Size(30, 30);
            buttonModConfig.TabIndex = 11;
            buttonModConfig.Text = "...";
            buttonModConfig.UseVisualStyleBackColor = true;
            buttonModConfig.Visible = false;
            buttonModConfig.Click += buttonModConfig_Click;
            // 
            // buttonBottom
            // 
            buttonBottom.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBottom.Location = new System.Drawing.Point(497, 113);
            buttonBottom.Name = "buttonBottom";
            buttonBottom.Size = new System.Drawing.Size(30, 30);
            buttonBottom.TabIndex = 10;
            buttonBottom.Text = "↓↓";
            buttonBottom.UseVisualStyleBackColor = true;
            buttonBottom.Click += buttonBottom_Click;
            // 
            // buttonDown
            // 
            buttonDown.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonDown.Location = new System.Drawing.Point(497, 77);
            buttonDown.Name = "buttonDown";
            buttonDown.Size = new System.Drawing.Size(30, 30);
            buttonDown.TabIndex = 9;
            buttonDown.Text = "↓";
            buttonDown.UseVisualStyleBackColor = true;
            buttonDown.Click += buttonDown_Click;
            // 
            // buttonUp
            // 
            buttonUp.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonUp.Location = new System.Drawing.Point(497, 41);
            buttonUp.Name = "buttonUp";
            buttonUp.Size = new System.Drawing.Size(30, 30);
            buttonUp.TabIndex = 8;
            buttonUp.Text = "↑";
            buttonUp.UseVisualStyleBackColor = true;
            buttonUp.Click += buttonUp_Click;
            // 
            // buttonTop
            // 
            buttonTop.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonTop.Location = new System.Drawing.Point(497, 5);
            buttonTop.Name = "buttonTop";
            buttonTop.Size = new System.Drawing.Size(30, 30);
            buttonTop.TabIndex = 7;
            buttonTop.Text = "↑↑";
            buttonTop.UseVisualStyleBackColor = true;
            buttonTop.Click += buttonTop_Click;
            // 
            // modListView
            // 
            modListView.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            modListView.CheckBoxes = true;
            modListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { columnName, columnAuthor, columnVersion, columnChecked, columnBuiltIn });
            modListView.FullRowSelect = true;
            modListView.Location = new System.Drawing.Point(2, 2);
            modListView.Margin = new System.Windows.Forms.Padding(2);
            modListView.Name = "modListView";
            modListView.Size = new System.Drawing.Size(490, 351);
            modListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            modListView.TabIndex = 6;
            modListView.UseCompatibleStateImageBehavior = false;
            modListView.View = System.Windows.Forms.View.Details;
            modListView.ColumnClick += modListView_ColumnClick;
            modListView.ItemChecked += modListView_ItemChecked;
            modListView.SelectedIndexChanged += modListView_SelectedIndexChanged;
            // 
            // columnName
            // 
            columnName.Text = "Name";
            columnName.Width = 250;
            // 
            // columnAuthor
            // 
            columnAuthor.Text = "Author";
            columnAuthor.Width = 145;
            // 
            // columnVersion
            // 
            columnVersion.Text = "Version";
            columnVersion.Width = 99;
            // 
            // columnChecked
            // 
            columnChecked.Text = "Active";
            columnChecked.Width = 90;
            // 
            // columnBuiltIn
            // 
            columnBuiltIn.Text = "Built-In";
            columnBuiltIn.Width = 90;
            // 
            // tabCodes
            // 
            tabCodes.BackColor = System.Drawing.SystemColors.Control;
            tabCodes.Controls.Add(codesListView);
            tabCodes.Location = new System.Drawing.Point(4, 24);
            tabCodes.Margin = new System.Windows.Forms.Padding(2);
            tabCodes.Name = "tabCodes";
            tabCodes.Padding = new System.Windows.Forms.Padding(2);
            tabCodes.Size = new System.Drawing.Size(532, 509);
            tabCodes.TabIndex = 3;
            tabCodes.Text = "Codes";
            // 
            // codesListView
            // 
            codesListView.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            codesListView.CheckBoxes = true;
            codesListView.FullRowSelect = true;
            codesListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            codesListView.Location = new System.Drawing.Point(2, 2);
            codesListView.Margin = new System.Windows.Forms.Padding(2);
            codesListView.MultiSelect = false;
            codesListView.Name = "codesListView";
            codesListView.Size = new System.Drawing.Size(526, 503);
            codesListView.TabIndex = 7;
            codesListView.UseCompatibleStateImageBehavior = false;
            codesListView.View = System.Windows.Forms.View.List;
            // 
            // tabGames
            // 
            tabGames.BackColor = System.Drawing.SystemColors.Control;
            tabGames.Controls.Add(buttonCopyGameID);
            tabGames.Controls.Add(buttonGameEdit);
            tabGames.Controls.Add(buttonGameRemove);
            tabGames.Controls.Add(buttonGameAdd);
            tabGames.Controls.Add(labelSelectGame);
            tabGames.Controls.Add(listBoxGames);
            tabGames.Controls.Add(labelGameCheckResult);
            tabGames.Controls.Add(buttonBrowse);
            tabGames.Controls.Add(textBoxOriginalPath);
            tabGames.Controls.Add(labelSelectnputPath);
            tabGames.Controls.Add(labelSelectOutput);
            tabGames.Controls.Add(buttonBrowseOutput);
            tabGames.Controls.Add(textBoxOutputPath);
            tabGames.Location = new System.Drawing.Point(4, 24);
            tabGames.Margin = new System.Windows.Forms.Padding(2);
            tabGames.Name = "tabGames";
            tabGames.Padding = new System.Windows.Forms.Padding(2);
            tabGames.Size = new System.Drawing.Size(532, 509);
            tabGames.TabIndex = 1;
            tabGames.Text = "Games";
            // 
            // buttonCopyGameID
            // 
            buttonCopyGameID.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonCopyGameID.Location = new System.Drawing.Point(439, 390);
            buttonCopyGameID.Name = "buttonCopyGameID";
            buttonCopyGameID.Size = new System.Drawing.Size(79, 28);
            buttonCopyGameID.TabIndex = 3;
            buttonCopyGameID.Text = "Copy ID";
            buttonCopyGameID.UseVisualStyleBackColor = true;
            buttonCopyGameID.Click += buttonCopyGameID_Click;
            // 
            // buttonGameEdit
            // 
            buttonGameEdit.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonGameEdit.Enabled = false;
            buttonGameEdit.Location = new System.Drawing.Point(497, 99);
            buttonGameEdit.Name = "buttonGameEdit";
            buttonGameEdit.Size = new System.Drawing.Size(30, 30);
            buttonGameEdit.TabIndex = 12;
            buttonGameEdit.Text = "...";
            buttonGameEdit.UseVisualStyleBackColor = true;
            buttonGameEdit.Click += buttonGameEdit_Click;
            // 
            // buttonGameRemove
            // 
            buttonGameRemove.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonGameRemove.Enabled = false;
            buttonGameRemove.Location = new System.Drawing.Point(497, 63);
            buttonGameRemove.Name = "buttonGameRemove";
            buttonGameRemove.Size = new System.Drawing.Size(30, 30);
            buttonGameRemove.TabIndex = 11;
            buttonGameRemove.Text = "-";
            buttonGameRemove.UseVisualStyleBackColor = true;
            buttonGameRemove.Click += buttonGameRemove_Click;
            // 
            // buttonGameAdd
            // 
            buttonGameAdd.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonGameAdd.Location = new System.Drawing.Point(497, 27);
            buttonGameAdd.Name = "buttonGameAdd";
            buttonGameAdd.Size = new System.Drawing.Size(30, 30);
            buttonGameAdd.TabIndex = 10;
            buttonGameAdd.Text = "+";
            buttonGameAdd.UseVisualStyleBackColor = true;
            buttonGameAdd.Click += buttonGameAdd_Click;
            // 
            // labelSelectGame
            // 
            labelSelectGame.AutoSize = true;
            labelSelectGame.Location = new System.Drawing.Point(8, 8);
            labelSelectGame.Name = "labelSelectGame";
            labelSelectGame.Size = new System.Drawing.Size(141, 15);
            labelSelectGame.TabIndex = 6;
            labelSelectGame.Text = "Select the game to patch:";
            // 
            // listBoxGames
            // 
            listBoxGames.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            listBoxGames.FormattingEnabled = true;
            listBoxGames.ItemHeight = 15;
            listBoxGames.Location = new System.Drawing.Point(12, 27);
            listBoxGames.Name = "listBoxGames";
            listBoxGames.Size = new System.Drawing.Size(479, 244);
            listBoxGames.TabIndex = 0;
            listBoxGames.SelectedIndexChanged += listBoxGames_SelectedIndexChanged;
            // 
            // labelGameCheckResult
            // 
            labelGameCheckResult.AutoSize = true;
            labelGameCheckResult.Location = new System.Drawing.Point(15, 397);
            labelGameCheckResult.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labelGameCheckResult.Name = "labelGameCheckResult";
            labelGameCheckResult.Size = new System.Drawing.Size(246, 15);
            labelGameCheckResult.TabIndex = 3;
            labelGameCheckResult.Text = "The result of the check will be displayed here.";
            // 
            // buttonBrowse
            // 
            buttonBrowse.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBrowse.Location = new System.Drawing.Point(439, 291);
            buttonBrowse.Margin = new System.Windows.Forms.Padding(2);
            buttonBrowse.Name = "buttonBrowse";
            buttonBrowse.Size = new System.Drawing.Size(79, 28);
            buttonBrowse.TabIndex = 2;
            buttonBrowse.Text = "Browse...";
            buttonBrowse.UseVisualStyleBackColor = true;
            buttonBrowse.Click += buttonBrowse_Click;
            // 
            // textBoxOriginalPath
            // 
            textBoxOriginalPath.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxOriginalPath.Location = new System.Drawing.Point(15, 293);
            textBoxOriginalPath.Margin = new System.Windows.Forms.Padding(2);
            textBoxOriginalPath.Name = "textBoxOriginalPath";
            textBoxOriginalPath.Size = new System.Drawing.Size(403, 23);
            textBoxOriginalPath.TabIndex = 1;
            textBoxOriginalPath.TextChanged += textBoxOriginalPath_TextChanged;
            // 
            // labelSelectnputPath
            // 
            labelSelectnputPath.AutoSize = true;
            labelSelectnputPath.Location = new System.Drawing.Point(15, 276);
            labelSelectnputPath.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labelSelectnputPath.Name = "labelSelectnputPath";
            labelSelectnputPath.Size = new System.Drawing.Size(264, 15);
            labelSelectnputPath.TabIndex = 0;
            labelSelectnputPath.Text = "Select the input path containing the original GDI:";
            // 
            // labelSelectOutput
            // 
            labelSelectOutput.AutoSize = true;
            labelSelectOutput.Location = new System.Drawing.Point(17, 327);
            labelSelectOutput.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labelSelectOutput.Name = "labelSelectOutput";
            labelSelectOutput.Size = new System.Drawing.Size(247, 15);
            labelSelectOutput.TabIndex = 9;
            labelSelectOutput.Text = "Select the output path for the patched image:";
            // 
            // buttonBrowseOutput
            // 
            buttonBrowseOutput.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBrowseOutput.Location = new System.Drawing.Point(439, 342);
            buttonBrowseOutput.Margin = new System.Windows.Forms.Padding(2);
            buttonBrowseOutput.Name = "buttonBrowseOutput";
            buttonBrowseOutput.Size = new System.Drawing.Size(79, 28);
            buttonBrowseOutput.TabIndex = 5;
            buttonBrowseOutput.Text = "Browse...";
            buttonBrowseOutput.UseVisualStyleBackColor = true;
            buttonBrowseOutput.Click += buttonBrowseOutput_Click;
            // 
            // textBoxOutputPath
            // 
            textBoxOutputPath.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxOutputPath.Location = new System.Drawing.Point(15, 344);
            textBoxOutputPath.Margin = new System.Windows.Forms.Padding(2);
            textBoxOutputPath.Name = "textBoxOutputPath";
            textBoxOutputPath.Size = new System.Drawing.Size(403, 23);
            textBoxOutputPath.TabIndex = 4;
            textBoxOutputPath.TextChanged += textBoxOutputPath_TextChanged;
            // 
            // tabOptions
            // 
            tabOptions.BackColor = System.Drawing.SystemColors.Control;
            tabOptions.Controls.Add(groupBoxUpdate);
            tabOptions.Controls.Add(groupBoxBinhackSettings);
            tabOptions.Controls.Add(groupBoxBuildSettings);
            tabOptions.Controls.Add(groupBoxEmulatorSettings);
            tabOptions.Location = new System.Drawing.Point(4, 24);
            tabOptions.Name = "tabOptions";
            tabOptions.Padding = new System.Windows.Forms.Padding(3);
            tabOptions.Size = new System.Drawing.Size(532, 509);
            tabOptions.TabIndex = 4;
            tabOptions.Text = "Options";
            // 
            // groupBoxUpdate
            // 
            groupBoxUpdate.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            groupBoxUpdate.Controls.Add(buttonCheckUpdateNow);
            groupBoxUpdate.Controls.Add(checkBoxCheckForUpdates);
            groupBoxUpdate.Location = new System.Drawing.Point(6, 412);
            groupBoxUpdate.Name = "groupBoxUpdate";
            groupBoxUpdate.Size = new System.Drawing.Size(520, 91);
            groupBoxUpdate.TabIndex = 3;
            groupBoxUpdate.TabStop = false;
            groupBoxUpdate.Text = "Update Settings";
            // 
            // buttonCheckUpdateNow
            // 
            buttonCheckUpdateNow.Location = new System.Drawing.Point(6, 47);
            buttonCheckUpdateNow.Name = "buttonCheckUpdateNow";
            buttonCheckUpdateNow.Size = new System.Drawing.Size(84, 28);
            buttonCheckUpdateNow.TabIndex = 1;
            buttonCheckUpdateNow.Text = "Check Now";
            buttonCheckUpdateNow.UseVisualStyleBackColor = true;
            buttonCheckUpdateNow.Click += buttonCheckUpdateNow_Click;
            // 
            // checkBoxCheckForUpdates
            // 
            checkBoxCheckForUpdates.AutoSize = true;
            checkBoxCheckForUpdates.Checked = true;
            checkBoxCheckForUpdates.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxCheckForUpdates.Location = new System.Drawing.Point(6, 22);
            checkBoxCheckForUpdates.Name = "checkBoxCheckForUpdates";
            checkBoxCheckForUpdates.Size = new System.Drawing.Size(177, 19);
            checkBoxCheckForUpdates.TabIndex = 0;
            checkBoxCheckForUpdates.Text = "Check for Updates at Startup";
            checkBoxCheckForUpdates.UseVisualStyleBackColor = true;
            // 
            // groupBoxBinhackSettings
            // 
            groupBoxBinhackSettings.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            groupBoxBinhackSettings.Controls.Add(checkBoxBinhackAutoLBA);
            groupBoxBinhackSettings.Controls.Add(checkBoxBinhackVGA);
            groupBoxBinhackSettings.Controls.Add(checkBoxBinhackCommonProts);
            groupBoxBinhackSettings.Controls.Add(checkBoxBinhackRegion);
            groupBoxBinhackSettings.Controls.Add(buttonBinhackReset);
            groupBoxBinhackSettings.Controls.Add(buttonCopyBinhackRam);
            groupBoxBinhackSettings.Controls.Add(labelBinhackRam);
            groupBoxBinhackSettings.Controls.Add(labelBinhackUsed);
            groupBoxBinhackSettings.Controls.Add(numericUpDownBinhackBuffer);
            groupBoxBinhackSettings.Controls.Add(labelDefaultBinhackBuffer);
            groupBoxBinhackSettings.Controls.Add(labelDefaultBinhackOffset);
            groupBoxBinhackSettings.Controls.Add(numericUpDownBinhackOffset);
            groupBoxBinhackSettings.Location = new System.Drawing.Point(6, 272);
            groupBoxBinhackSettings.Name = "groupBoxBinhackSettings";
            groupBoxBinhackSettings.Size = new System.Drawing.Size(520, 134);
            groupBoxBinhackSettings.TabIndex = 2;
            groupBoxBinhackSettings.TabStop = false;
            groupBoxBinhackSettings.Text = "Default Binhack Settings";
            // 
            // checkBoxBinhackAutoLBA
            // 
            checkBoxBinhackAutoLBA.AutoSize = true;
            checkBoxBinhackAutoLBA.Checked = true;
            checkBoxBinhackAutoLBA.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxBinhackAutoLBA.Location = new System.Drawing.Point(185, 109);
            checkBoxBinhackAutoLBA.Name = "checkBoxBinhackAutoLBA";
            checkBoxBinhackAutoLBA.Size = new System.Drawing.Size(106, 19);
            checkBoxBinhackAutoLBA.TabIndex = 6;
            checkBoxBinhackAutoLBA.Text = "Auto LBA Hack";
            checkBoxBinhackAutoLBA.UseVisualStyleBackColor = true;
            // 
            // checkBoxBinhackVGA
            // 
            checkBoxBinhackVGA.AutoSize = true;
            checkBoxBinhackVGA.Checked = true;
            checkBoxBinhackVGA.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxBinhackVGA.Location = new System.Drawing.Point(105, 109);
            checkBoxBinhackVGA.Name = "checkBoxBinhackVGA";
            checkBoxBinhackVGA.Size = new System.Drawing.Size(74, 19);
            checkBoxBinhackVGA.TabIndex = 5;
            checkBoxBinhackVGA.Text = "VGA Flag";
            checkBoxBinhackVGA.UseVisualStyleBackColor = true;
            // 
            // checkBoxBinhackCommonProts
            // 
            checkBoxBinhackCommonProts.AutoSize = true;
            checkBoxBinhackCommonProts.Checked = true;
            checkBoxBinhackCommonProts.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxBinhackCommonProts.Location = new System.Drawing.Point(297, 109);
            checkBoxBinhackCommonProts.Name = "checkBoxBinhackCommonProts";
            checkBoxBinhackCommonProts.Size = new System.Drawing.Size(216, 19);
            checkBoxBinhackCommonProts.TabIndex = 7;
            checkBoxBinhackCommonProts.Text = "Hack Common GDROM Protections";
            checkBoxBinhackCommonProts.UseVisualStyleBackColor = true;
            // 
            // checkBoxBinhackRegion
            // 
            checkBoxBinhackRegion.AutoSize = true;
            checkBoxBinhackRegion.Checked = true;
            checkBoxBinhackRegion.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxBinhackRegion.Location = new System.Drawing.Point(6, 109);
            checkBoxBinhackRegion.Name = "checkBoxBinhackRegion";
            checkBoxBinhackRegion.Size = new System.Drawing.Size(93, 19);
            checkBoxBinhackRegion.TabIndex = 4;
            checkBoxBinhackRegion.Text = "Region Hack";
            checkBoxBinhackRegion.UseVisualStyleBackColor = true;
            // 
            // buttonBinhackReset
            // 
            buttonBinhackReset.Location = new System.Drawing.Point(430, 54);
            buttonBinhackReset.Name = "buttonBinhackReset";
            buttonBinhackReset.Size = new System.Drawing.Size(84, 28);
            buttonBinhackReset.TabIndex = 3;
            buttonBinhackReset.Text = "Reset";
            buttonBinhackReset.UseVisualStyleBackColor = true;
            buttonBinhackReset.Click += buttonBinhackReset_Click;
            // 
            // buttonCopyBinhackRam
            // 
            buttonCopyBinhackRam.Location = new System.Drawing.Point(430, 20);
            buttonCopyBinhackRam.Name = "buttonCopyBinhackRam";
            buttonCopyBinhackRam.Size = new System.Drawing.Size(84, 28);
            buttonCopyBinhackRam.TabIndex = 1;
            buttonCopyBinhackRam.Text = "Copy";
            buttonCopyBinhackRam.UseVisualStyleBackColor = true;
            buttonCopyBinhackRam.Click += buttonCopyBinhackRam_Click;
            // 
            // labelBinhackRam
            // 
            labelBinhackRam.AutoSize = true;
            labelBinhackRam.Location = new System.Drawing.Point(257, 27);
            labelBinhackRam.Name = "labelBinhackRam";
            labelBinhackRam.Size = new System.Drawing.Size(136, 15);
            labelBinhackRam.TabIndex = 5;
            labelBinhackRam.Text = "RAM Address: 8C00D500";
            // 
            // labelBinhackUsed
            // 
            labelBinhackUsed.AutoSize = true;
            labelBinhackUsed.Location = new System.Drawing.Point(6, 81);
            labelBinhackUsed.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            labelBinhackUsed.Name = "labelBinhackUsed";
            labelBinhackUsed.Size = new System.Drawing.Size(68, 15);
            labelBinhackUsed.TabIndex = 4;
            labelBinhackUsed.Text = "Used RAM: ";
            // 
            // numericUpDownBinhackBuffer
            // 
            numericUpDownBinhackBuffer.Hexadecimal = true;
            numericUpDownBinhackBuffer.Location = new System.Drawing.Point(147, 54);
            numericUpDownBinhackBuffer.Maximum = new decimal(new int[] { -1, 0, 0, 0 });
            numericUpDownBinhackBuffer.Minimum = new decimal(new int[] { -1, 0, 0, int.MinValue });
            numericUpDownBinhackBuffer.Name = "numericUpDownBinhackBuffer";
            numericUpDownBinhackBuffer.Size = new System.Drawing.Size(104, 23);
            numericUpDownBinhackBuffer.TabIndex = 2;
            numericUpDownBinhackBuffer.Value = new decimal(new int[] { -1931538432, 0, 0, 0 });
            numericUpDownBinhackBuffer.ValueChanged += numericUpDownBinhackBuffer_ValueChanged;
            // 
            // labelDefaultBinhackBuffer
            // 
            labelDefaultBinhackBuffer.AutoSize = true;
            labelDefaultBinhackBuffer.Location = new System.Drawing.Point(6, 58);
            labelDefaultBinhackBuffer.Margin = new System.Windows.Forms.Padding(3, 16, 3, 0);
            labelDefaultBinhackBuffer.Name = "labelDefaultBinhackBuffer";
            labelDefaultBinhackBuffer.Size = new System.Drawing.Size(133, 15);
            labelDefaultBinhackBuffer.TabIndex = 2;
            labelDefaultBinhackBuffer.Text = "Buffer Location in RAM:";
            // 
            // labelDefaultBinhackOffset
            // 
            labelDefaultBinhackOffset.AutoSize = true;
            labelDefaultBinhackOffset.Location = new System.Drawing.Point(6, 27);
            labelDefaultBinhackOffset.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
            labelDefaultBinhackOffset.Name = "labelDefaultBinhackOffset";
            labelDefaultBinhackOffset.Size = new System.Drawing.Size(135, 15);
            labelDefaultBinhackOffset.TabIndex = 1;
            labelDefaultBinhackOffset.Text = "Binhack Offset in IP.BIN:";
            // 
            // numericUpDownBinhackOffset
            // 
            numericUpDownBinhackOffset.Hexadecimal = true;
            numericUpDownBinhackOffset.Location = new System.Drawing.Point(147, 25);
            numericUpDownBinhackOffset.Maximum = new decimal(new int[] { 32176, 0, 0, 0 });
            numericUpDownBinhackOffset.Minimum = new decimal(new int[] { 14336, 0, 0, 0 });
            numericUpDownBinhackOffset.Name = "numericUpDownBinhackOffset";
            numericUpDownBinhackOffset.Size = new System.Drawing.Size(104, 23);
            numericUpDownBinhackOffset.TabIndex = 0;
            numericUpDownBinhackOffset.Value = new decimal(new int[] { 21760, 0, 0, 0 });
            numericUpDownBinhackOffset.ValueChanged += numericUpDownBinhackOffset_ValueChanged;
            // 
            // groupBoxBuildSettings
            // 
            groupBoxBuildSettings.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            groupBoxBuildSettings.Controls.Add(checkBoxKeepOrigDataFolder);
            groupBoxBuildSettings.Controls.Add(checkBoxKeepWorkDataFolder);
            groupBoxBuildSettings.Controls.Add(checkBoxAddGameName);
            groupBoxBuildSettings.Controls.Add(buttonBrowseDefaultOutput);
            groupBoxBuildSettings.Controls.Add(checkBoxPauseBeforeBuild);
            groupBoxBuildSettings.Controls.Add(labelDefaultOutput);
            groupBoxBuildSettings.Controls.Add(textBoxDefaultOutput);
            groupBoxBuildSettings.Location = new System.Drawing.Point(6, 6);
            groupBoxBuildSettings.Name = "groupBoxBuildSettings";
            groupBoxBuildSettings.Size = new System.Drawing.Size(520, 159);
            groupBoxBuildSettings.TabIndex = 0;
            groupBoxBuildSettings.TabStop = false;
            groupBoxBuildSettings.Text = "Build Settings";
            // 
            // checkBoxKeepOrigDataFolder
            // 
            checkBoxKeepOrigDataFolder.AutoSize = true;
            checkBoxKeepOrigDataFolder.Checked = true;
            checkBoxKeepOrigDataFolder.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxKeepOrigDataFolder.Location = new System.Drawing.Point(6, 105);
            checkBoxKeepOrigDataFolder.Name = "checkBoxKeepOrigDataFolder";
            checkBoxKeepOrigDataFolder.Size = new System.Drawing.Size(243, 19);
            checkBoxKeepOrigDataFolder.TabIndex = 5;
            checkBoxKeepOrigDataFolder.Text = "Reuse Original Data Folder for new Builds";
            checkBoxKeepOrigDataFolder.UseVisualStyleBackColor = true;
            // 
            // checkBoxKeepWorkDataFolder
            // 
            checkBoxKeepWorkDataFolder.AutoSize = true;
            checkBoxKeepWorkDataFolder.Checked = true;
            checkBoxKeepWorkDataFolder.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxKeepWorkDataFolder.Location = new System.Drawing.Point(6, 130);
            checkBoxKeepWorkDataFolder.Name = "checkBoxKeepWorkDataFolder";
            checkBoxKeepWorkDataFolder.Size = new System.Drawing.Size(220, 19);
            checkBoxKeepWorkDataFolder.TabIndex = 6;
            checkBoxKeepWorkDataFolder.Text = "Keep Working Data Folder after Build";
            checkBoxKeepWorkDataFolder.UseVisualStyleBackColor = true;
            // 
            // checkBoxAddGameName
            // 
            checkBoxAddGameName.AutoSize = true;
            checkBoxAddGameName.Checked = true;
            checkBoxAddGameName.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxAddGameName.Location = new System.Drawing.Point(6, 55);
            checkBoxAddGameName.Name = "checkBoxAddGameName";
            checkBoxAddGameName.Size = new System.Drawing.Size(331, 19);
            checkBoxAddGameName.TabIndex = 3;
            checkBoxAddGameName.Text = "Add game name to Output Path when adding new games";
            checkBoxAddGameName.UseVisualStyleBackColor = true;
            // 
            // buttonBrowseDefaultOutput
            // 
            buttonBrowseDefaultOutput.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBrowseDefaultOutput.Location = new System.Drawing.Point(430, 23);
            buttonBrowseDefaultOutput.Name = "buttonBrowseDefaultOutput";
            buttonBrowseDefaultOutput.Size = new System.Drawing.Size(84, 28);
            buttonBrowseDefaultOutput.TabIndex = 2;
            buttonBrowseDefaultOutput.Text = "Browse...";
            buttonBrowseDefaultOutput.UseVisualStyleBackColor = true;
            buttonBrowseDefaultOutput.Click += buttonBrowseDefaultOutput_Click;
            // 
            // checkBoxPauseBeforeBuild
            // 
            checkBoxPauseBeforeBuild.AutoSize = true;
            checkBoxPauseBeforeBuild.Location = new System.Drawing.Point(6, 80);
            checkBoxPauseBeforeBuild.Name = "checkBoxPauseBeforeBuild";
            checkBoxPauseBeforeBuild.Size = new System.Drawing.Size(380, 19);
            checkBoxPauseBeforeBuild.TabIndex = 4;
            checkBoxPauseBeforeBuild.Text = "Pause before Build (for manual changes before building the image)";
            checkBoxPauseBeforeBuild.UseVisualStyleBackColor = true;
            // 
            // labelDefaultOutput
            // 
            labelDefaultOutput.AutoSize = true;
            labelDefaultOutput.Location = new System.Drawing.Point(6, 29);
            labelDefaultOutput.Name = "labelDefaultOutput";
            labelDefaultOutput.Size = new System.Drawing.Size(125, 15);
            labelDefaultOutput.TabIndex = 0;
            labelDefaultOutput.Text = "Default Output Folder:";
            // 
            // textBoxDefaultOutput
            // 
            textBoxDefaultOutput.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxDefaultOutput.Location = new System.Drawing.Point(137, 25);
            textBoxDefaultOutput.Name = "textBoxDefaultOutput";
            textBoxDefaultOutput.Size = new System.Drawing.Size(287, 23);
            textBoxDefaultOutput.TabIndex = 1;
            textBoxDefaultOutput.TextChanged += textBoxDefaultOutput_TextChanged;
            // 
            // groupBoxEmulatorSettings
            // 
            groupBoxEmulatorSettings.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            groupBoxEmulatorSettings.Controls.Add(radioButtonEmulator1);
            groupBoxEmulatorSettings.Controls.Add(radioButtonEmulator2);
            groupBoxEmulatorSettings.Controls.Add(labelEmu1Location);
            groupBoxEmulatorSettings.Controls.Add(textBoxEmuLocation);
            groupBoxEmulatorSettings.Controls.Add(buttonBrowseEmulator);
            groupBoxEmulatorSettings.Location = new System.Drawing.Point(6, 171);
            groupBoxEmulatorSettings.Name = "groupBoxEmulatorSettings";
            groupBoxEmulatorSettings.Size = new System.Drawing.Size(520, 95);
            groupBoxEmulatorSettings.TabIndex = 1;
            groupBoxEmulatorSettings.TabStop = false;
            groupBoxEmulatorSettings.Text = "Emulator Settings";
            // 
            // radioButtonEmulator1
            // 
            radioButtonEmulator1.AutoSize = true;
            radioButtonEmulator1.Checked = true;
            radioButtonEmulator1.Location = new System.Drawing.Point(9, 22);
            radioButtonEmulator1.Name = "radioButtonEmulator1";
            radioButtonEmulator1.Size = new System.Drawing.Size(82, 19);
            radioButtonEmulator1.TabIndex = 0;
            radioButtonEmulator1.TabStop = true;
            radioButtonEmulator1.Text = "Emulator 1";
            radioButtonEmulator1.UseVisualStyleBackColor = true;
            radioButtonEmulator1.CheckedChanged += radioButtonEmulator1_CheckedChanged;
            // 
            // radioButtonEmulator2
            // 
            radioButtonEmulator2.AutoSize = true;
            radioButtonEmulator2.Location = new System.Drawing.Point(147, 22);
            radioButtonEmulator2.Name = "radioButtonEmulator2";
            radioButtonEmulator2.Size = new System.Drawing.Size(82, 19);
            radioButtonEmulator2.TabIndex = 1;
            radioButtonEmulator2.Text = "Emulator 2";
            radioButtonEmulator2.UseVisualStyleBackColor = true;
            // 
            // labelEmu1Location
            // 
            labelEmu1Location.AutoSize = true;
            labelEmu1Location.Location = new System.Drawing.Point(9, 55);
            labelEmu1Location.Name = "labelEmu1Location";
            labelEmu1Location.Size = new System.Drawing.Size(56, 15);
            labelEmu1Location.TabIndex = 9;
            labelEmu1Location.Text = "Location:";
            // 
            // textBoxEmuLocation
            // 
            textBoxEmuLocation.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxEmuLocation.Location = new System.Drawing.Point(71, 52);
            textBoxEmuLocation.Name = "textBoxEmuLocation";
            textBoxEmuLocation.Size = new System.Drawing.Size(353, 23);
            textBoxEmuLocation.TabIndex = 2;
            textBoxEmuLocation.TextChanged += textBoxEmuLocation_TextChanged;
            // 
            // buttonBrowseEmulator
            // 
            buttonBrowseEmulator.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBrowseEmulator.Location = new System.Drawing.Point(430, 49);
            buttonBrowseEmulator.Name = "buttonBrowseEmulator";
            buttonBrowseEmulator.Size = new System.Drawing.Size(84, 28);
            buttonBrowseEmulator.TabIndex = 3;
            buttonBrowseEmulator.Text = "Browse...";
            buttonBrowseEmulator.UseVisualStyleBackColor = true;
            buttonBrowseEmulator.Click += buttonBrowseEmulator_Click;
            // 
            // tabBuild
            // 
            tabBuild.BackColor = System.Drawing.SystemColors.Control;
            tabBuild.Controls.Add(labelStatus);
            tabBuild.Controls.Add(textBoxLog);
            tabBuild.Location = new System.Drawing.Point(4, 24);
            tabBuild.Margin = new System.Windows.Forms.Padding(2);
            tabBuild.Name = "tabBuild";
            tabBuild.Padding = new System.Windows.Forms.Padding(2);
            tabBuild.Size = new System.Drawing.Size(532, 509);
            tabBuild.TabIndex = 2;
            tabBuild.Text = "Build Image";
            // 
            // labelStatus
            // 
            labelStatus.AutoSize = true;
            labelStatus.Location = new System.Drawing.Point(8, 8);
            labelStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labelStatus.Name = "labelStatus";
            labelStatus.Size = new System.Drawing.Size(423, 15);
            labelStatus.TabIndex = 6;
            labelStatus.Text = "Click Build to create a modified GD-ROM image. Progress will be shown below.";
            // 
            // textBoxLog
            // 
            textBoxLog.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxLog.Location = new System.Drawing.Point(6, 27);
            textBoxLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            textBoxLog.Multiline = true;
            textBoxLog.Name = "textBoxLog";
            textBoxLog.ReadOnly = true;
            textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            textBoxLog.Size = new System.Drawing.Size(520, 476);
            textBoxLog.TabIndex = 2;
            // 
            // buttonBuild
            // 
            buttonBuild.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonBuild.Enabled = false;
            buttonBuild.Location = new System.Drawing.Point(371, 549);
            buttonBuild.Margin = new System.Windows.Forms.Padding(2);
            buttonBuild.Name = "buttonBuild";
            buttonBuild.Size = new System.Drawing.Size(84, 28);
            buttonBuild.TabIndex = 6;
            buttonBuild.Text = "Build";
            buttonBuild.UseVisualStyleBackColor = true;
            buttonBuild.Click += buttonBuild_Click;
            // 
            // buttonSaveSettings
            // 
            buttonSaveSettings.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonSaveSettings.Location = new System.Drawing.Point(89, 549);
            buttonSaveSettings.Margin = new System.Windows.Forms.Padding(2);
            buttonSaveSettings.Name = "buttonSaveSettings";
            buttonSaveSettings.Size = new System.Drawing.Size(72, 28);
            buttonSaveSettings.TabIndex = 2;
            buttonSaveSettings.Text = "Save";
            buttonSaveSettings.UseVisualStyleBackColor = true;
            buttonSaveSettings.Click += buttonSaveSettings_Click;
            // 
            // timer1
            // 
            timer1.Interval = 60;
            timer1.Tick += timer1_Tick;
            // 
            // buttonRunEmu
            // 
            buttonRunEmu.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonRunEmu.Enabled = false;
            buttonRunEmu.Location = new System.Drawing.Point(166, 549);
            buttonRunEmu.Name = "buttonRunEmu";
            buttonRunEmu.Size = new System.Drawing.Size(72, 28);
            buttonRunEmu.TabIndex = 3;
            buttonRunEmu.Text = "Run";
            buttonRunEmu.UseVisualStyleBackColor = true;
            buttonRunEmu.Click += buttonRunEmu_Click;
            // 
            // buttonBuildRun
            // 
            buttonBuildRun.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonBuildRun.Enabled = false;
            buttonBuildRun.Location = new System.Drawing.Point(460, 549);
            buttonBuildRun.Name = "buttonBuildRun";
            buttonBuildRun.Size = new System.Drawing.Size(84, 28);
            buttonBuildRun.TabIndex = 7;
            buttonBuildRun.Text = "Build && Run";
            buttonBuildRun.UseVisualStyleBackColor = true;
            buttonBuildRun.Click += buttonBuildRun_Click;
            // 
            // buttonReload
            // 
            buttonReload.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonReload.Location = new System.Drawing.Point(12, 549);
            buttonReload.Name = "buttonReload";
            buttonReload.Size = new System.Drawing.Size(72, 28);
            buttonReload.TabIndex = 1;
            buttonReload.Text = "Reload";
            buttonReload.UseVisualStyleBackColor = true;
            buttonReload.Click += buttonReload_Click;
            // 
            // buttonStop
            // 
            buttonStop.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonStop.Location = new System.Drawing.Point(244, 549);
            buttonStop.Name = "buttonStop";
            buttonStop.Size = new System.Drawing.Size(72, 28);
            buttonStop.TabIndex = 4;
            buttonStop.Text = "Stop";
            buttonStop.UseVisualStyleBackColor = true;
            buttonStop.Click += buttonStop_Click;
            // 
            // checkBoxCDI
            // 
            checkBoxCDI.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            checkBoxCDI.AutoSize = true;
            checkBoxCDI.Location = new System.Drawing.Point(322, 555);
            checkBoxCDI.Name = "checkBoxCDI";
            checkBoxCDI.Size = new System.Drawing.Size(45, 19);
            checkBoxCDI.TabIndex = 5;
            checkBoxCDI.Text = "CDI";
            checkBoxCDI.UseVisualStyleBackColor = true;
            checkBoxCDI.CheckedChanged += radioButtonCDI_CheckedChanged;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            ClientSize = new System.Drawing.Size(556, 589);
            Controls.Add(checkBoxCDI);
            Controls.Add(buttonStop);
            Controls.Add(buttonReload);
            Controls.Add(buttonBuildRun);
            Controls.Add(buttonRunEmu);
            Controls.Add(buttonBuild);
            Controls.Add(tabControl1);
            Controls.Add(buttonSaveSettings);
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Margin = new System.Windows.Forms.Padding(2);
            Name = "MainForm";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "Dreamcast Image Builder";
            FormClosing += MainForm_FormClosing;
            tabControl1.ResumeLayout(false);
            tabMods.ResumeLayout(false);
            tabMods.PerformLayout();
            tabCodes.ResumeLayout(false);
            tabGames.ResumeLayout(false);
            tabGames.PerformLayout();
            tabOptions.ResumeLayout(false);
            groupBoxUpdate.ResumeLayout(false);
            groupBoxUpdate.PerformLayout();
            groupBoxBinhackSettings.ResumeLayout(false);
            groupBoxBinhackSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)numericUpDownBinhackBuffer).EndInit();
            ((System.ComponentModel.ISupportInitialize)numericUpDownBinhackOffset).EndInit();
            groupBoxBuildSettings.ResumeLayout(false);
            groupBoxBuildSettings.PerformLayout();
            groupBoxEmulatorSettings.ResumeLayout(false);
            groupBoxEmulatorSettings.PerformLayout();
            tabBuild.ResumeLayout(false);
            tabBuild.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.TextBox modDescription;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabMods;
        private System.Windows.Forms.TabPage tabGames;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.TextBox textBoxOriginalPath;
        private System.Windows.Forms.Label labelSelectnputPath;
        private System.Windows.Forms.TabPage tabBuild;
        private System.Windows.Forms.Button buttonBrowseOutput;
        private System.Windows.Forms.TextBox textBoxOutputPath;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Label labelGameCheckResult;
        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.Label labelSelectOutput;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ListView modListView;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnAuthor;
        private System.Windows.Forms.ColumnHeader columnVersion;
        private System.Windows.Forms.Button buttonSaveSettings;
        private System.Windows.Forms.TabPage tabCodes;
        private System.Windows.Forms.ListView codesListView;
        private System.Windows.Forms.TextBox textBoxEmuLocation;
        private System.Windows.Forms.Button buttonRunEmu;
        private System.Windows.Forms.Button buttonBuildRun;
        private System.Windows.Forms.ColumnHeader columnChecked;
        private System.Windows.Forms.Button buttonReload;
        private System.Windows.Forms.Button buttonBottom;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonTop;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.CheckBox checkBoxCDI;
        private System.Windows.Forms.Label labelSelectGame;
        private System.Windows.Forms.ListBox listBoxGames;
        private System.Windows.Forms.ColumnHeader columnBuiltIn;
        private System.Windows.Forms.Button buttonModConfig;
        private System.Windows.Forms.Button buttonGameEdit;
        private System.Windows.Forms.Button buttonGameRemove;
        private System.Windows.Forms.Button buttonGameAdd;
        private System.Windows.Forms.Button buttonDeleteMod;
        private System.Windows.Forms.Button buttonNewMod;
        private System.Windows.Forms.TabPage tabOptions;
        private System.Windows.Forms.Button buttonBrowseDefaultOutput;
        private System.Windows.Forms.TextBox textBoxDefaultOutput;
        private System.Windows.Forms.Label labelDefaultOutput;
        private System.Windows.Forms.RadioButton radioButtonEmulator2;
        private System.Windows.Forms.RadioButton radioButtonEmulator1;
        private System.Windows.Forms.GroupBox groupBoxEmulatorSettings;
        private System.Windows.Forms.Label labelEmu1Location;
        private System.Windows.Forms.Button buttonBrowseEmulator;
        private System.Windows.Forms.GroupBox groupBoxBuildSettings;
        private System.Windows.Forms.CheckBox checkBoxPauseBeforeBuild;
        private System.Windows.Forms.GroupBox groupBoxBinhackSettings;
        private System.Windows.Forms.Label labelDefaultBinhackBuffer;
        private System.Windows.Forms.Label labelDefaultBinhackOffset;
        private System.Windows.Forms.NumericUpDown numericUpDownBinhackOffset;
        private System.Windows.Forms.NumericUpDown numericUpDownBinhackBuffer;
        private System.Windows.Forms.Label labelBinhackUsed;
        private System.Windows.Forms.CheckBox checkBoxAddGameName;
        private System.Windows.Forms.Label labelBinhackRam;
        private System.Windows.Forms.Button buttonCopyGameID;
        private System.Windows.Forms.Button buttonCopyBinhackRam;
        private System.Windows.Forms.Button buttonBinhackReset;
        private System.Windows.Forms.CheckBox checkBoxKeepOrigDataFolder;
        private System.Windows.Forms.CheckBox checkBoxKeepWorkDataFolder;
        private System.Windows.Forms.GroupBox groupBoxUpdate;
        private System.Windows.Forms.Button buttonCheckUpdateNow;
        private System.Windows.Forms.CheckBox checkBoxCheckForUpdates;
        private System.Windows.Forms.CheckBox checkBoxBinhackCommonProts;
        private System.Windows.Forms.CheckBox checkBoxBinhackRegion;
        private System.Windows.Forms.CheckBox checkBoxBinhackVGA;
        private System.Windows.Forms.CheckBox checkBoxBinhackAutoLBA;
    }
}

