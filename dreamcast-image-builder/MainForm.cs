﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DreamcastImageBuilder.ClassesHelpers;
using DreamcastImageBuilder.Properties;
using IniParser;
using IniParser.Model;

namespace DreamcastImageBuilder
{
    public partial class MainForm : Form
    {
        readonly ListViewColumnSorter lvwColumnSorter;     
        HttpClient UpdateChecker;

        TextBoxWriter writer; // Console redirection

        string CurrentVersion; // Shown in title bar

        string emuLocation1; // Emulator path 1
        string emuLocation2; // Emulator path 2
        string defOutPath; // Default output path

        string lastCheckedGameVersion; // Product ID string for the game that was last checked

        public MainForm()
        {
            InitializeComponent();
            // Apply update
            if (Path.GetFileName(Application.ExecutablePath) == "DreamcastImageBuilder_update.exe")
                FinalizeUpdate();
            // Or clean up after update
            else
                CleanupUpdate();
            UpdateChecker = new HttpClient();
            // ListView stuff
            lvwColumnSorter = new ListViewColumnSorter();
            modListView.ListViewItemSorter = lvwColumnSorter;
            // Get the path where the assembly is loaded with built-in stuff
            ImageBuilder.currentDirAss = Path.GetFullPath(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
            // Clear lists and UI
            listBoxGames.SelectedIndex = -1;
            ImageBuilder.mods = new List<Mod>();
            lastCheckedGameVersion = "None";
            defOutPath = "";
            emuLocation1 = emuLocation2 = "";
            ImageBuilder.activeMods = new List<string>();
            ImageBuilder.activeCodes = new List<string>();
            ImageBuilder.blacklistMods = new List<string>();
            modListView.Items.Clear();
            // Set version
            CurrentVersion = "6";
            if (File.Exists(Path.Combine(ImageBuilder.currentDirAss, "version.txt")))
                CurrentVersion = File.ReadAllText(Path.Combine(ImageBuilder.currentDirAss, "version.txt"));
            // Load game list
            InitGames();
            RefreshGamesList();
            // Load settings
            ImageBuilder.gameConfigs = new();
            LoadSettings();
            // Check for updates
            if (checkBoxCheckForUpdates.Checked)
                Task.Run(() => CheckUpdate(false));
        }

        // Reload mods when a different GDI is selected
        private void SwitchGame()
        {
            ReloadMods();
            InitializeCodes();
            modListView.Items.Clear();
            CheckMods();
            PutCheckedModsFirst();
        }

        // Load the settings ini file
        private void LoadSettings()
        {
            // Load INI
            var ini = new FileIniDataParser();
            if (!File.Exists(Path.Combine(Environment.CurrentDirectory, "settings.ini")))
                ImageBuilder.settings = new IniData();
            else
                ImageBuilder.settings = ini.ReadFile(Path.Combine(Environment.CurrentDirectory, "settings.ini"));

            // Set binhack settings
            uint binhackOffset = 0x5500;
            uint binhackBuffer = 0x8CDE0000;
            if (uint.TryParse(ImageBuilder.settings.Global["BinhackOffset"], System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out uint res_bho))
            {
                binhackOffset = res_bho;
            }
            if (uint.TryParse(ImageBuilder.settings.Global["BinhackBuffer"], System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out uint res_bhb))
            {
                binhackBuffer = res_bhb;
            }
            binhackOffset = Math.Max(Math.Min(binhackOffset, (uint)numericUpDownBinhackOffset.Maximum), (uint)numericUpDownBinhackOffset.Minimum);
            numericUpDownBinhackOffset.Value = binhackOffset;
            numericUpDownBinhackBuffer.Value = binhackBuffer;
            UpdateBinhackOffset();
            UpdateBinhackRAM();

            // Set emulator settings
            if (bool.TryParse(ImageBuilder.settings.Global["EmuAlt"], out bool emu_res))
            {
                radioButtonEmulator1.Checked = !emu_res;
                radioButtonEmulator2.Checked = emu_res;
            }
            else
            {
                radioButtonEmulator1.Checked = true;
                radioButtonEmulator2.Checked = false;
            }

            emuLocation1 = ImageBuilder.settings.Global["EmuPath"];
            emuLocation2 = ImageBuilder.settings.Global["EmuPath2"];
            UpdateEmulatorLocation();

            // Set default output path and other settings
            defOutPath = ImageBuilder.settings.Global["DefaultOutputPath"];
            if (string.IsNullOrEmpty(defOutPath))
                defOutPath = Path.Combine(System.Environment.CurrentDirectory, "output");
            textBoxDefaultOutput.Text = defOutPath;

            if (bool.TryParse(ImageBuilder.settings.Global["PauseBeforeBuild"], out bool pause_res))
                checkBoxPauseBeforeBuild.Checked = pause_res;
            if (bool.TryParse(ImageBuilder.settings.Global["AppendGamePath"], out bool append_res))
                checkBoxAddGameName.Checked = append_res;
            if (bool.TryParse(ImageBuilder.settings.Global["KeepWorkDir"], out bool workdir_res))
                checkBoxKeepWorkDataFolder.Checked = workdir_res;
            if (bool.TryParse(ImageBuilder.settings.Global["KeepOrigDir"], out bool origdir_res))
                checkBoxKeepOrigDataFolder.Checked = origdir_res;
            if (bool.TryParse(ImageBuilder.settings.Global["CheckForUpdates"], out bool update_res))
                checkBoxCheckForUpdates.Checked = update_res;
            if (bool.TryParse(ImageBuilder.settings.Global["RegionHack"], out bool region_res))
                checkBoxBinhackRegion.Checked = region_res;
            if (bool.TryParse(ImageBuilder.settings.Global["GdromHack"], out bool gdrom_res))
                checkBoxBinhackCommonProts.Checked = gdrom_res;
            if (bool.TryParse(ImageBuilder.settings.Global["LbaHack"], out bool lba_res))
                checkBoxBinhackAutoLBA.Checked = lba_res;
            if (bool.TryParse(ImageBuilder.settings.Global["VgaFlag"], out bool vga_res))
                checkBoxBinhackVGA.Checked = vga_res;

            // Set individual game settings
            foreach (Game game in ImageBuilder.games)
            {
                bool hasSection = ImageBuilder.settings.Sections.ContainsSection(game.IniName);
                GameConfig gameConfig = new()
                {
                    GameName = game.Name,
                    IniName = game.IniName,
                    InputPath = hasSection ? ImageBuilder.settings[game.IniName]["InputPath"] : "",
                    OutputPath = hasSection ? ImageBuilder.settings[game.IniName]["OutputPath"] : "",
                    BuildCDI = hasSection && bool.Parse(ImageBuilder.settings[game.IniName]["BuildCDI"]),
                    Mods = ImageBuilder.settings[game.IniName]["Mods"]?.Split(','),
                    Codes = ImageBuilder.settings[game.IniName]["Codes"]?.Split(','),
                };
                ImageBuilder.gameConfigs.Add(gameConfig);
            }

            // Set current game
            if (ImageBuilder.settings.Global["CurrentGame"] != null)
            {
                string gameID = ImageBuilder.settings.Global["CurrentGame"];
                if (gameID != null)
                    ImageBuilder.currentGame = GetGameByIniID(gameID);
            }
            else
                ImageBuilder.currentGame = ImageBuilder.games[0];

            // Update edit boxes for current game
            GetCurrentGameConfig();

            // Select current game in the UI
            SelectCurrentGame();
        }

        // Save the settings ini file
        private void SaveSettings()
        {
            // Save binhack settings
            ImageBuilder.settings.Global["BinhackOffset"] = ((uint)numericUpDownBinhackOffset.Value).ToString("X");
            ImageBuilder.settings.Global["BinhackBuffer"] = ((uint)numericUpDownBinhackBuffer.Value).ToString("X");
            ImageBuilder.settings.Global["RegionHack"] = checkBoxBinhackRegion.Checked ? "True" : "False";
            ImageBuilder.settings.Global["GdromHack"] = checkBoxBinhackCommonProts.Checked ? "True" : "False";
            ImageBuilder.settings.Global["VgaFlag"] = checkBoxBinhackVGA.Checked ? "True" : "False";
            ImageBuilder.settings.Global["LbaHack"] = checkBoxBinhackAutoLBA.Checked ? "True" : "False";
            // Save emulator settings
            ImageBuilder.settings.Global["EmuAlt"] = radioButtonEmulator1.Checked ? "False" : "True";
            // Save paths
            ImageBuilder.settings.Global["PauseBeforeBuild"] = checkBoxPauseBeforeBuild.Checked ? "True" : "False";
            ImageBuilder.settings.Global["AppendGamePath"] = checkBoxAddGameName.Checked ? "True" : "False";
            ImageBuilder.settings.Global["KeepWorkDir"] = checkBoxKeepWorkDataFolder.Checked ? "True" : "False";
            ImageBuilder.settings.Global["KeepOrigDir"] = checkBoxKeepOrigDataFolder.Checked ? "True" : "False";
            ImageBuilder.settings.Global["CheckForUpdates"] = checkBoxCheckForUpdates.Checked ? "True" : "False";
            ImageBuilder.settings.Global["DefaultOutputPath"] = defOutPath;
            ImageBuilder.settings.Global["CurrentGame"] = ImageBuilder.currentGame.IniName;
            ImageBuilder.settings.Global["EmuPath"] = emuLocation1;
            ImageBuilder.settings.Global["EmuPath2"] = emuLocation2;
            // Save mods
            ImageBuilder.activeMods.Clear();
            ImageBuilder.activeCodes.Clear();
            for (int c = 0; c < modListView.Items.Count; c++)
            {
                if (modListView.Items[c].Checked)
                    ImageBuilder.activeMods.Add(modListView.Items[c].Text);
            }
            // Save codes
            for (int c = 0; c < codesListView.Items.Count; c++)
            {
                if (codesListView.Items[c].Checked)
                    ImageBuilder.activeCodes.Add(codesListView.Items[c].Text);
            }
            SetCurrentGameConfig();
            SetAllGameConfig();
            // Write INI
            var ini = new FileIniDataParser();
            ini.WriteFile(Path.Combine(Environment.CurrentDirectory, "settings.ini"), ImageBuilder.settings);
            PutCheckedModsFirst();
        }

        // Clear the mods list and readd mods
        private void ReloadMods()
        {
            ImageBuilder.mods.Clear();
            string[] inifiles_external = new string[0];
            if (Directory.Exists(Path.Combine(Environment.CurrentDirectory, "mods")))
                inifiles_external = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "mods"), "mod.ini", SearchOption.AllDirectories);
            string[] inifiles_internal = Directory.GetFiles(Path.Combine(ImageBuilder.currentDirAss, "mods"), "mod.ini", SearchOption.AllDirectories);
            // Add mods from the mods folder
            for (int i = 0; i < inifiles_external.Length; i++)
            {
                if (ImageBuilder.blacklistMods.Contains(inifiles_external[i]))
                    continue;
                try
                {
                    if (Mod.GetCompatibleGameVersions(inifiles_external[i]).Contains(ImageBuilder.currentGame.GameTitle))
                        ImageBuilder.mods.Add(new Mod(inifiles_external[i]));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, string.Format("Mod from {0} failed to load with the following error:\n\n{1}", inifiles_external[i], ex.Message.ToString() + "\n\nThis mod will be ignored until the next time the program starts."), "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ImageBuilder.blacklistMods.Add(inifiles_external[i]);
                    continue;
                }
            }
            // Add built-in mods
            for (int i = 0; i < inifiles_internal.Length; i++)
            {
                Mod mod = new Mod(inifiles_internal[i], true);
                if (mod.GameVersions.Contains(ImageBuilder.currentGame.GameTitle))
                {
                    // Check if the built-in mod is overwritten by a mod from the mods folder
                    bool already = false;
                    foreach (Mod modInternal in ImageBuilder.mods)
                        if (modInternal.Name == mod.Name)
                            already = true;
                    if (!already)
                        ImageBuilder.mods.Add(mod);
                }
            }
        }

        // Clear and readd codes
        private void InitializeCodes()
        {
            // Clear codes
            ImageBuilder.activeCodes.Clear();
            codesListView.Items.Clear();

            // Merge internal and internal codes
            string[] codesLocationExternal = new string[0];
            if (Directory.Exists(Path.Combine(Environment.CurrentDirectory, "games")))
                codesLocationExternal = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "games"), ImageBuilder.currentGame.IniName + ".cht", SearchOption.AllDirectories);
            string[] codesLocationInternal = Directory.GetFiles(Path.Combine(ImageBuilder.currentDirAss, "games"), ImageBuilder.currentGame.IniName + ".cht", SearchOption.AllDirectories);
            string[] codesLocation = new string[codesLocationExternal.Length + codesLocationInternal.Length];
            Array.Copy(codesLocationExternal, codesLocation, codesLocationExternal.Length);
            Array.Copy(codesLocationInternal, 0, codesLocation, codesLocationExternal.Length, codesLocationInternal.Length);

            // If the codes file doesn't exist, remove the Codes tab
            if (codesLocation.Length == 0 || !File.Exists(codesLocation[0]))
            {
                tabControl1.TabPages.Remove(tabCodes);
                return;
            }
            // Otherwise add the Codes tab if it's missing 
            else
            {
                if (!tabControl1.TabPages.Contains(tabCodes))
                    tabControl1.TabPages.Insert(1, tabCodes);
            }
            // Load the codes file
            ImageBuilder.codes = new FileIniDataParser().ReadFile(codesLocation[0]);
            // Parse codes and add them to the listbox
            for (int c = 0; c < 99; c++)
            {
                string keyName = "cheat" + c.ToString() + "_desc";
                if (ImageBuilder.codes.Global.ContainsKey(keyName))
                {
                    string codeDesc = ImageBuilder.codes.Global[keyName].Replace("\"", "");
                    //MessageBox.Show(codes.Global[keyName]);
                    codesListView.Items.Add(new ListViewItem(new[] { codeDesc }) { Checked = ImageBuilder.activeCodes.Contains(codeDesc) ? true : false });
                }
                else
                    break;
            }
        }

        // Check if the mods exists and add checkboxes to the ListView
        private void CheckMods()
        {
            // Check mods
            foreach (Mod mod in ImageBuilder.mods)
            {
                bool validDisc = mod.Discs == null || mod.Discs.Contains(ImageBuilder.currentGame.Disc);
                if (mod.GameVersions.Contains(ImageBuilder.currentGame.GameTitle))
                {
                    ListViewItem modItem = new ListViewItem(new[] { mod.Name, mod.Author, mod.Version, ImageBuilder.activeMods.Contains(mod.Name) ? "Yes" : "", mod.BuiltIn ? "Yes" : "" });
                    if (!validDisc)
                        modItem.ForeColor = System.Drawing.SystemColors.GrayText;
                    modItem.Checked = validDisc && ImageBuilder.activeMods.Contains(mod.Name) ? true : false;
                    modListView.Items.Add(modItem);
                }
            }
            // Initialize and check codes
            InitializeCodes();
        }

        // Check if the currently selected GDI matches the currently selected game
        private bool CheckGameVersion(string path)
        {
            lastCheckedGameVersion = Game.GetGameVersion(path);
            bool correct = ImageBuilder.currentGame.GameTitle == lastCheckedGameVersion;
            string correctstring = correct ? " (correct)" : " (wrong)";
            if (correct)
                ImageBuilder.currentGame.Disc = Game.GetGameDisc(path);
            labelGameCheckResult.Text = "Game ID: " + lastCheckedGameVersion + correctstring + " / Disc " + ImageBuilder.currentGame.Disc.ToString();
            return correct;
        }

        // Enable or disable "Build" and "Build and Run" buttons

        private void CheckBuildButton()
        {
            if (buttonBuild.InvokeRequired)
            {
                buttonBuild.Invoke(new MethodInvoker(() => { CheckBuildButton(); }));
            }
            if (CheckGameVersion(textBoxOriginalPath.Text) && textBoxOutputPath.Text != "")
            {
                buttonBuild.Enabled = true;
                ImageBuilder.workdir_main = textBoxOutputPath.Text;
                CheckEmulator();
            }
            else
                buttonBuild.Enabled = buttonBuildRun.Enabled = false;
        }

        // Set a code as active
        private void EnableCode(string code)
        {
            ImageBuilder.activeCodes.Add(code);
            for (int c = 0; c < codesListView.Items.Count; c++)
            {
                if (codesListView.Items[c].Text == code)
                {
                    codesListView.Items[c].Checked = true;
                    break;
                }
            }
        }

        // Recheck mods and codes before building
        private void RearrangeActiveModsAndCodes()
        {
            // Recheck current mods
            ImageBuilder.activeMods.Clear();
            for (int c = 0; c < modListView.Items.Count; c++)
            {
                if (modListView.Items[c].Checked)
                    ImageBuilder.activeMods.Add(modListView.Items[c].Text);
            }
            // Recheck current codes
            ImageBuilder.activeCodes.Clear();
            for (int c = 0; c < codesListView.Items.Count; c++)
            {
                if (codesListView.Items[c].Checked)
                    ImageBuilder.activeCodes.Add(codesListView.Items[c].Text);
            }
        }

        // Start build process
        private async void buttonBuild_Click(object sender, EventArgs e)
        {
            string gdi = GDITools.GetGDIPath(textBoxOriginalPath.Text);
            if (string.IsNullOrEmpty(gdi))
            {
                MessageBox.Show(this, "Cannot proceed because the source folder contains multiple GDI/CUE files or none.", "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            ImageBuilder.filesToPatch = new();
            SaveSettings();
            if (Path.GetFullPath(textBoxOriginalPath.Text) == Path.GetFullPath(textBoxOutputPath.Text))
            {
                MessageBox.Show(this, "Source and destination paths cannot be the same.", "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            tabControl1.SelectTab(tabBuild);
            textBoxLog.Text = "";
            buttonBuild.Enabled = buttonBuildRun.Enabled = buttonRunEmu.Enabled = false;
            // Reload mods in case they were modified while the program was running
            ReloadMods();
            RearrangeActiveModsAndCodes();
            // Force enable codes
            List<Mod> applyMods = new List<Mod>();
            for (int i = 0; i < ImageBuilder.mods.Count; i++)
                for (int u = 0; u < ImageBuilder.activeMods.Count; u++)
                    if (ImageBuilder.mods[i].Name == ImageBuilder.activeMods[u])
                        applyMods.Add(ImageBuilder.mods[i]);
            foreach (Mod data in applyMods)
                if (data.RequiredCodes != null)
                    for (int c = 0; c < data.RequiredCodes.Length; c++)
                        EnableCode(data.RequiredCodes[c]);
            writer = new TextBoxWriter(textBoxLog);
            timer1.Enabled = true;
            Console.SetOut(writer);
            Logger.WriteMessage("Build process started");
            Logger.WriteMessage(string.Format("Game: {0} / {1}", ImageBuilder.currentGame.Name, ImageBuilder.currentGame.GameTitle));
            GetLastTrackIDFromGDI(gdi);
            ImageBuilder.binhackBuffer = (uint)numericUpDownBinhackBuffer.Value;
            ImageBuilder.binhackOffset = (uint)numericUpDownBinhackOffset.Value;
            ImageBuilder.emuPath = textBoxEmuLocation.Text;
            ImageBuilder.workdir_main = textBoxOutputPath.Text;
            ImageBuilder.keepWorkDataFolder = checkBoxKeepWorkDataFolder.Checked;
            ImageBuilder.keepOriginalDataFolder = checkBoxKeepOrigDataFolder.Checked;
            ImageBuilder.pauseBeforeBuild = checkBoxPauseBeforeBuild.Checked;
            ImageBuilder.sourcePath = textBoxOriginalPath.Text;
            ImageBuilder.sourceForm = this;
            ImageBuilder.regionHack = checkBoxBinhackRegion.Checked;
            ImageBuilder.gdromProts = checkBoxBinhackCommonProts.Checked;
            ImageBuilder.autoLBA = ImageBuilder.currentGame.BootLBA != 45000 && checkBoxBinhackAutoLBA.Checked;
            ImageBuilder.vga = checkBoxBinhackVGA.Checked;
            await Task.Run(() => ImageBuilder.StartBuildAsync());
        }

        #region UI

        // Select default output path
        private void buttonBrowseDefaultOutput_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (textBoxDefaultOutput.Text != "")
                dlg.SelectedPath = textBoxDefaultOutput.Text;
            else
                dlg.SelectedPath = Environment.CurrentDirectory;
            dlg.ShowNewFolderButton = false;
            if (dlg.ShowDialog() == DialogResult.OK)
                textBoxDefaultOutput.Text = dlg.SelectedPath;
        }

        // Update default output path
        private void textBoxDefaultOutput_TextChanged(object sender, EventArgs e)
        {
            defOutPath = textBoxDefaultOutput.Text;
        }

        // Check if the supplied path for the GDI is valid
        private void textBoxOriginalPath_TextChanged(object sender, EventArgs e)
        {
            CheckBuildButton();
        }

        private void textBoxOutputPath_TextChanged(object sender, EventArgs e)
        {
            CheckBuildButton();
        }

        // Locate the GDI using the dialog
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (textBoxOriginalPath.Text != "")
                dlg.SelectedPath = textBoxOriginalPath.Text;
            else
                dlg.SelectedPath = Environment.CurrentDirectory;
            dlg.ShowNewFolderButton = false;
            if (dlg.ShowDialog() == DialogResult.OK)
                textBoxOriginalPath.Text = dlg.SelectedPath;
        }

        // Select output folder
        private void buttonBrowseOutput_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            if (textBoxOutputPath.Text != "")
                dlg.SelectedPath = textBoxOutputPath.Text;
            else
                dlg.SelectedPath = Environment.CurrentDirectory;
            dlg.ShowNewFolderButton = true;
            if (dlg.ShowDialog() == DialogResult.OK)
                textBoxOutputPath.Text = dlg.SelectedPath;
            CheckBuildButton();
        }


        // Find mod by name to set the currently selected mod

        private Mod FindModByName(string name)
        {
            foreach (Mod mod in ImageBuilder.mods)
            {
                if (mod.Name == name)
                    return mod;
            }
            return null;
        }

        private void modListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (modListView.SelectedItems.Count <= 0 || modListView.SelectedIndices[0] == -1)
                return;
            ImageBuilder.currentMod = FindModByName(modListView.SelectedItems[0].Text);
            if (ImageBuilder.currentMod == null)
            {
                modDescription.Text = "Mod description.";
                buttonDeleteMod.Enabled = buttonModConfig.Visible = false;
            }
            else
            {
                if (ImageBuilder.currentMod.Description != null)
                    modDescription.Text = ImageBuilder.currentMod.Description.Replace("\\n", System.Environment.NewLine);
                else
                    modDescription.Text = "No description.";
                bool validDisc = ImageBuilder.currentMod.Discs == null || ImageBuilder.currentMod.Discs.Contains(ImageBuilder.currentGame.Disc);
                buttonModConfig.Visible = (validDisc && ImageBuilder.currentMod.SubMods != null && ImageBuilder.currentMod.SubMods.Count > 0);
                buttonDeleteMod.Enabled = true;
                if (!validDisc)
                {
                    modDescription.Text = "This mod is meant to be used on discs: " +
                        string.Join<int>(", ", ImageBuilder.currentMod.Discs) +
                        ". The current game disc is " + ImageBuilder.currentGame.Disc.ToString() + "." +
                        System.Environment.NewLine + modDescription.Text;
                }
            }

        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (ImageBuilder.status == ImageBuilder.BuildStatus.Building)
            {
                ImageBuilder.status = ImageBuilder.BuildStatus.Cancelling;
                if (ImageBuilder.tokenSource != null)
                    ImageBuilder.tokenSource.Cancel();
                // Terminate processes if the build was cancelled
                string[] processes = { "xdelta3", "mkisofs", "cdi4dc" };
                foreach (string process in processes)
                    ProcessStuff.TerminateProcess(process);
            }
            ImageBuilder.TerminateEmu();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ImageBuilder.status == ImageBuilder.BuildStatus.Building)
            {
                ImageBuilder.status = ImageBuilder.BuildStatus.Cancelling;
                if (ImageBuilder.tokenSource != null)
                    ImageBuilder.tokenSource.Cancel();
                // Terminate processes if the build was cancelled
                string[] processes = { "xdelta3", "mkisofs", "cdi4dc" };
                foreach (string process in processes)
                    ProcessStuff.TerminateProcess(process);
            }
            ImageBuilder.TerminateEmu();
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            ReloadMods();
            SwitchGame();
        }

        // Write the console and check for cancellation
        private void timer1_Tick(object sender, EventArgs e)
        {
            writer?.WriteOut();
            if (ImageBuilder.status == ImageBuilder.BuildStatus.Cancelled)
            {
                CheckBuildButton();
                CheckEmulator();
                ImageBuilder.status = ImageBuilder.BuildStatus.Stopped;
            }
        }

        private void buttonSaveSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }


        private void radioButtonCDI_CheckedChanged(object sender, EventArgs e)
        {
            ImageBuilder.useCDI = checkBoxCDI.Checked;
            CheckEmulator();
        }

        #endregion

        #region Emulator

        // Toggle Emulator 1/2
        private void radioButtonEmulator1_CheckedChanged(object sender, EventArgs e)
        {
            UpdateEmulatorLocation();
        }

        // Set emulator path
        private void UpdateEmulatorLocation()
        {
            textBoxEmuLocation.Text = radioButtonEmulator1.Checked ? emuLocation1 : emuLocation2;
            CheckEmulator();
        }

        // Run emulator
        private void buttonRunEmu_Click(object sender, EventArgs e)
        {
            ImageBuilder.workdir_main = textBoxOutputPath.Text;
            ImageBuilder.RunEmulator();
        }

        // Check if the emulator path is valid and enable the button
        private void CheckEmulator()
        {
            if (!File.Exists(textBoxEmuLocation.Text))
            {
                buttonBuildRun.Enabled = buttonRunEmu.Enabled = false;
                return;
            }
            else
            {
                if (radioButtonEmulator1.Checked)
                    emuLocation1 = textBoxEmuLocation.Text;
                else
                    emuLocation2 = textBoxEmuLocation.Text;
            }
            buttonRunEmu.Enabled = File.Exists(Path.Combine(textBoxOutputPath.Text, "disc." + (checkBoxCDI.Checked ? "cdi" : "gdi")));
            buttonBuildRun.Enabled = buttonBuild.Enabled && textBoxOutputPath.Text != "";
            if (buttonRunEmu.Enabled)
                ImageBuilder.emuPath = textBoxEmuLocation.Text;
        }

        private void textBoxEmuLocation_TextChanged(object sender, EventArgs e)
        {
            CheckEmulator();
        }

        private void buttonBrowseEmulator_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog { Filter = "EXE files|*.exe" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    textBoxEmuLocation.Text = ofd.FileName;
                }
            }
        }

        private void buttonBuildRun_Click(object sender, EventArgs e)
        {
            if (Path.GetFullPath(textBoxOriginalPath.Text) == Path.GetFullPath(textBoxOutputPath.Text))
            {
                MessageBox.Show(this, "Source and destination paths cannot be the same.", "SA1 Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            buttonBuildRun.Enabled = false;
            ImageBuilder.autoRun = true;
            buttonBuild_Click(sender, e);
        }
        #endregion

        #region Mod Sorting

        // Check mod disc ID and set "Checked" column 
        private void modListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            Mod checkMod = FindModByName(e.Item.Text);
            bool validDisc = checkMod.Discs == null || checkMod.Discs.Contains(ImageBuilder.currentGame.Disc);
            if (!validDisc)
                e.Item.Checked = false;
            e.Item.SubItems[3].Text = e.Item.Checked ? "Yes" : "";
        }

        private void PutCheckedModsFirst()
        {
            lvwColumnSorter.Order = SortOrder.Descending;
            lvwColumnSorter.SortColumn = 3;
            modListView.Sort();
            for (int m = ImageBuilder.activeMods.Count - 1; m >= 0; m--)
            {
                string modName = ImageBuilder.activeMods[m];
                //MessageBox.Show(modName);
                for (int item = 0; item < modListView.Items.Count; item++)
                    if (modListView.Items[item].Text == modName)
                    {
                        var listitem = modListView.Items[item];
                        modListView.Items.RemoveAt(item);
                        modListView.Items.Insert(0, listitem);
                    }
            }
        }

        private void modListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Sort mods by checked state if the "Checked" column has been clicked.
            if (e.Column == 3)
            {
                PutCheckedModsFirst();
                return;
            }
            // https://learn.microsoft.com/en-us/troubleshoot/developer/visualstudio/csharp/language-compilers/sort-listview-by-column
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {

                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            modListView.Sort();
        }

        private void buttonTop_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;
            List<ListViewItem> selItems = new();
            foreach (ListViewItem selItem in modListView.SelectedItems)
            {
                selItems.Add(selItem);
                modListView.Items.Remove(selItem);
            }
            for (int u = selItems.Count - 1; u >= 0; u--)
            {
                modListView.Items.Insert(0, selItems[u]);
                modListView.Items[0].EnsureVisible();
            }
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;
            List<ListViewItem> selItems = new();
            int newIndex = Math.Max(0, modListView.SelectedIndices[0] - 1);
            foreach (ListViewItem selItem in modListView.SelectedItems)
            {
                selItems.Add(selItem);
                modListView.Items.Remove(selItem);
            }
            for (int u = selItems.Count - 1; u >= 0; u--)
            {
                modListView.Items.Insert(newIndex, selItems[u]);
                modListView.Items[newIndex].EnsureVisible();
            }
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;

            List<ListViewItem> selItems = new();
            int newIndex = Math.Min(modListView.Items.Count, modListView.SelectedIndices[0] + 1);
            foreach (ListViewItem selItem in modListView.SelectedItems)
            {
                selItems.Add(selItem);
                modListView.Items.Remove(selItem);
            }
            for (int u = selItems.Count - 1; u >= 0; u--)
            {
                modListView.Items.Insert(newIndex, selItems[u]);
                modListView.Items[newIndex].EnsureVisible();
            }

        }

        private void buttonBottom_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;
            List<ListViewItem> selItems = new();
            foreach (ListViewItem selItem in modListView.SelectedItems)
            {
                selItems.Add(selItem);
                modListView.Items.Remove(selItem);
            }
            for (int u = 0; u < selItems.Count; u++)
            {
                modListView.Items.Add(selItems[u]);
            }
        }
        #endregion

        #region Games

        // Copy current game ID to clipboard
        private void buttonCopyGameID_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lastCheckedGameVersion);
        }

        // Get number of tracks (also last data track ID) from a GDI file for current game
        void GetLastTrackIDFromGDI(string gdifile)
        {
            ImageBuilder.currentGame.NumTracks = Game.GetNumTracksFromGDI(gdifile);
            Logger.WriteMessage("Number of tracks: " + ImageBuilder.currentGame.NumTracks.ToString(), false, true);
        }

        // Load games list
        void InitGames()
        {
            ImageBuilder.games = new List<Game>();
            string[] gamefiles_external = new string[0];
            if (Directory.Exists(Path.Combine(Environment.CurrentDirectory, "games")))
                gamefiles_external = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "games"), "*.ini", SearchOption.AllDirectories);
            string[] gamefiles_internal = Directory.GetFiles(Path.Combine(ImageBuilder.currentDirAss, "games"), "*.ini", SearchOption.AllDirectories);
            List<string> gamefiles_s = new();
            // Add files from the games folder first
            foreach (string gfe in gamefiles_external)
                gamefiles_s.Add(gfe);
            // Add built-in games
            foreach (string gfi in gamefiles_internal)
            {
                bool already = false;
                foreach (string gfe in gamefiles_s)
                {
                    if (Path.GetFileNameWithoutExtension(gfe).ToLowerInvariant() == Path.GetFileNameWithoutExtension(gfi).ToLowerInvariant())
                        already = true;
                }
                if (!already)
                    gamefiles_s.Add(gfi);
            }
            string[] gamefiles = gamefiles_s.ToArray();
            foreach (string file in gamefiles)
            {
                if (gamefiles_internal.Contains(file))
                {
                    Game game = new Game(file);
                    game.Name += " (built-in)";
                    game.BuiltIn = true;
                    ImageBuilder.games.Insert(0, game);
                }
                else
                    ImageBuilder.games.Add(new Game(file));
            }
            if (ImageBuilder.games.Count == 0)
            {
                MessageBox.Show(this, "Unable to load game list. Make sure the 'games' folder contains game metadata.", "Dreamcast Image Builder error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }

        // Refresh games list in the UI
        void RefreshGamesList()
        {
            listBoxGames.Items.Clear();
            foreach (Game game in ImageBuilder.games)
                listBoxGames.Items.Add(game.Name);
        }

        // Highlight the currenty selected game in the UI
        private void SelectCurrentGame()
        {
            for (int i = 0; i < listBoxGames.Items.Count; i++)
            {
                if (listBoxGames.Items[i].ToString() == ImageBuilder.currentGame.Name)
                {
                    listBoxGames.SelectedIndex = i;
                    break;
                }
            }
        }

        // Set edit boxes for the current game
        private void GetCurrentGameConfig()
        {
            if (ImageBuilder.currentGame == null)
                return;
            if (ImageBuilder.currentGame.Icon != null)
                Icon = ImageBuilder.currentGame.Icon;
            else
                Icon = Resources.icon;
            Text = "Dreamcast Image Builder Version " + CurrentVersion + " / " + ImageBuilder.currentGame.Name;
            if (ImageBuilder.currentGame.BootLBA == -1)
                checkBoxCDI.Enabled = checkBoxCDI.Checked = false;
            else
                checkBoxCDI.Enabled = true;
            foreach (var item in ImageBuilder.gameConfigs)
            {
                if (item.IniName == ImageBuilder.currentGame.IniName)
                {
                    //MessageBox.Show(item.InputPath);
                    textBoxOriginalPath.Text = item.InputPath != null ? item.InputPath : "";
                    textBoxOutputPath.Text = item.OutputPath != null ? item.OutputPath : "";
                    if (checkBoxCDI.Enabled)
                        checkBoxCDI.Checked = item.BuildCDI;
                    ImageBuilder.activeMods = item.Mods != null ? item.Mods.ToList() : new();
                    ImageBuilder.activeCodes = item.Codes != null ? item.Codes.ToList() : new();
                    return;
                }
            }
        }

        // Set game config for the current game using the edit boxes
        private void SetCurrentGameConfig()
        {
            if (ImageBuilder.currentGame == null || ImageBuilder.gameConfigs.Count < 1)
                return;
            foreach (var item in ImageBuilder.gameConfigs)
                if (item.IniName == ImageBuilder.currentGame.IniName)
                {
                    item.InputPath = textBoxOriginalPath.Text;
                    item.OutputPath = textBoxOutputPath.Text;
                    item.BuildCDI = (checkBoxCDI.Enabled && checkBoxCDI.Checked);
                    item.Mods = ImageBuilder.activeMods.ToArray();
                    item.Codes = ImageBuilder.activeCodes.ToArray();
                    return;
                }
        }

        // Save game config for all games
        private void SetAllGameConfig()
        {
            if (ImageBuilder.gameConfigs.Count < 1)
                return;
            foreach (GameConfig item in ImageBuilder.gameConfigs)
            {
                ImageBuilder.settings.Sections.RemoveSection(item.IniName);
                ImageBuilder.settings.Sections.AddSection(item.IniName);
                ImageBuilder.settings.Sections[item.IniName].AddKey("InputPath", item.InputPath);
                ImageBuilder.settings.Sections[item.IniName].AddKey("OutputPath", item.OutputPath);
                ImageBuilder.settings.Sections[item.IniName].AddKey("BuildCDI", item.BuildCDI.ToString());
                ImageBuilder.settings.Sections[item.IniName].AddKey("Mods", item.Mods != null ? string.Join(',', item.Mods) : "");
                ImageBuilder.settings.Sections[item.IniName].AddKey("Codes", item.Codes != null ? string.Join(',', item.Codes) : "");
            }
        }

        // Retrieve a game from the games list by its name
        private Game GetGameByName(string gameName)
        {
            foreach (Game item in ImageBuilder.games)
                if (item.Name == gameName)
                    return item;
            return null;
        }

        // Retrieve a game from the games list by its INI name
        private Game GetGameByIniID(string iniID)
        {
            foreach (Game item in ImageBuilder.games)
                if (item.IniName == iniID)
                    return item;
            return ImageBuilder.games[0];
        }

        private void listBoxGames_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxGames.Items.Count < 1 || listBoxGames.SelectedIndex <= -1)
                return;
            SetCurrentGameConfig();
            ImageBuilder.currentGame = GetGameByName(listBoxGames.SelectedItem.ToString());
            GetCurrentGameConfig();
            SwitchGame();
            buttonGameEdit.Enabled = buttonGameRemove.Enabled = (ImageBuilder.currentGame != null && ImageBuilder.currentGame.BuiltIn == false);
        }

        #endregion

        #region Adding, Editing and Removing Games

        private void buttonGameAdd_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "GDI or CUE Files|*.gdi;*.cue" })
            {
                DialogResult result = openFileDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    List<string> existingGames = new List<string>();
                    foreach (string item in listBoxGames.Items)
                        existingGames.Add(item);
                    using (NewGameDialog ngd = new NewGameDialog(openFileDialog.FileName, true, existingGames.ToArray()))
                    {
                        if (ngd.ShowDialog() == DialogResult.Cancel)
                            return;
                        InitGames();
                        RefreshGamesList();
                        ImageBuilder.currentGame = GetGameByName(ngd.FinalName);
                        SelectCurrentGame();
                        textBoxOriginalPath.Text = Path.GetFullPath(Path.GetDirectoryName(openFileDialog.FileName));
                        textBoxOutputPath.Text = "";
                        // Set default path if it exists
                        if (!string.IsNullOrEmpty(defOutPath))
                            textBoxOutputPath.Text = Path.Combine(defOutPath, checkBoxAddGameName.Checked ? ngd.FinalName : "");
                        // Check if there's a config entry already
                        bool gameConfigExists = false;
                        foreach (GameConfig gameConfig in ImageBuilder.gameConfigs)
                        {
                            if (gameConfig.IniName == ImageBuilder.currentGame.IniName)
                            {
                                gameConfig.BuildCDI = false;
                                gameConfig.GameName = ImageBuilder.currentGame.Name;
                                gameConfig.InputPath = textBoxOriginalPath.Text;
                                textBoxOutputPath.Text = gameConfig.OutputPath;
                                gameConfigExists = true;
                                break;
                            }
                        }
                        // If there isn't, create a new one
                        if (!gameConfigExists)
                        {
                            ImageBuilder.gameConfigs.Add(new GameConfig
                            {
                                BuildCDI = false,
                                GameName = ImageBuilder.currentGame.Name,
                                IniName = ImageBuilder.currentGame.IniName,
                                InputPath = textBoxOriginalPath.Text,
                                OutputPath = textBoxOutputPath.Text
                            });
                        }
                    }
                }
            }
        }

        private void buttonGameEdit_Click(object sender, EventArgs e)
        {
            int index = listBoxGames.SelectedIndex;
            string gameIniPath = Path.Combine(ImageBuilder.currentGame.Location, ImageBuilder.currentGame.IniName + ".ini");
            if (!File.Exists(gameIniPath))
            {
                MessageBox.Show(this, "Unable to find game file to edit: " + gameIniPath, "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            List<string> existingGames = new List<string>();
            foreach (string item in listBoxGames.Items)
                existingGames.Add(item);
            using (NewGameDialog ngd = new NewGameDialog(gameIniPath, false, existingGames.ToArray()))
            {
                if (ngd.ShowDialog() == DialogResult.OK)
                {
                    InitGames();
                    RefreshGamesList();
                    listBoxGames.SelectedIndex = index;
                }
            }
        }

        private void buttonGameRemove_Click(object sender, EventArgs e)
        {
            string gameIniPath = Path.Combine(ImageBuilder.currentGame.Location, ImageBuilder.currentGame.IniName + ".ini");
            if (!File.Exists(gameIniPath))
            {
                MessageBox.Show(this, "Unable to find game file to remove: " + gameIniPath, "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (MessageBox.Show(this, "This action cannot be undone. Are you sure you want to delete game '" + ImageBuilder.currentGame.IniName + "'?", "Dreamcast Image Builder", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                // Save listbox index
                int index = listBoxGames.SelectedIndex;
                // Delete game files
                string[] gamefiles = Directory.GetFiles(ImageBuilder.currentGame.Location, ImageBuilder.currentGame.IniName + ".*", SearchOption.TopDirectoryOnly);
                foreach (string file in gamefiles)
                {
                    File.Delete(file);
                }
                // If the game folder is empty now, delete it
                if (Directory.GetFiles(ImageBuilder.currentGame.Location, ImageBuilder.currentGame.IniName + ".*", SearchOption.AllDirectories).Length == 0)
                {
                    try
                    {
                        Directory.Delete(ImageBuilder.currentGame.Location, true);
                    }
                    catch
                    {
                        MessageBox.Show(this, "Unable to delete directory " + ImageBuilder.currentGame.Location, "Dreamcast Image Builder Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                InitGames();
                RefreshGamesList();
                listBoxGames.SelectedIndex = Math.Max(0, Math.Min(listBoxGames.Items.Count - 1, index));
                ImageBuilder.currentGame = GetGameByName(listBoxGames.SelectedItem.ToString());
            }
        }
        #endregion

        #region Mod Configuration

        private void buttonModConfig_Click(object sender, EventArgs e)
        {
            if (ImageBuilder.currentMod == null || ImageBuilder.currentMod.SubMods == null || ImageBuilder.currentMod.SubMods.Count < 1)
            {
                buttonModConfig.Visible = false;
                return;
            }
            List<string> activeSubMods = null;
            if (ImageBuilder.settings["Submods"] != null && ImageBuilder.settings["Submods"].ContainsKey(ImageBuilder.currentMod.Name))
                activeSubMods = ImageBuilder.settings["Submods"].GetKeyData(ImageBuilder.currentMod.Name).Value.Split(',').ToList();
            else if (ImageBuilder.currentMod.DefaultSubMods != null)
                activeSubMods = ImageBuilder.currentMod.DefaultSubMods;
            using (ModConfigDialog modConfigDialog = new ModConfigDialog(ImageBuilder.currentMod, activeSubMods, Icon))
            {
                if (modConfigDialog.ShowDialog() == DialogResult.OK)
                {
                    KeyData submodKeyData = new KeyData(ImageBuilder.currentMod.Name);
                    submodKeyData.Value = string.Join(',', modConfigDialog.activeSubMods);
                    ImageBuilder.settings["Submods"].RemoveKey(ImageBuilder.currentMod.Name);
                    ImageBuilder.settings["Submods"].AddKey(submodKeyData);
                }
            }
        }
        #endregion

        #region Creating and Deleting Mods

        private void buttonNewMod_Click(object sender, EventArgs e)
        {
            using (NewModDialog newModDialog = new NewModDialog(ImageBuilder.currentGame.GameTitle, Icon))
            {
                if (newModDialog.ShowDialog() == DialogResult.Cancel)
                    return;
                // Create mod folder
                string modFolder = Path.Combine(Environment.CurrentDirectory, "mods", newModDialog.ModName);
                Directory.CreateDirectory(modFolder);
                // Create mod.ini
                using (TextWriter writer = File.CreateText(Path.Combine(modFolder, "mod.ini")))
                {
                    writer.WriteLine(string.Format("Name={0}", newModDialog.ModName));
                    if (!string.IsNullOrEmpty(newModDialog.ModAuthor))
                        writer.WriteLine(string.Format("Author={0}", newModDialog.ModAuthor));
                    if (!string.IsNullOrEmpty(newModDialog.ModVersion))
                        writer.WriteLine(string.Format("Version={0}", newModDialog.ModVersion));
                    if (!string.IsNullOrEmpty(newModDialog.ModDescription))
                        writer.WriteLine(string.Format("Description={0}", newModDialog.ModDescription));
                    writer.WriteLine(string.Format("GameVersions={0}", ImageBuilder.currentGame.GameTitle));
                    if (newModDialog.Discs != null)
                    {
                        StringBuilder discList = new StringBuilder();
                        discList.Append("Discs=");
                        for (int i = 0; i < newModDialog.Discs.Count; i++)
                        {
                            discList.Append(newModDialog.Discs[i].ToString());
                            if (i < newModDialog.Discs.Count - 1)
                                discList.Append(",");
                        }
                        writer.WriteLine(discList.ToString());
                    }
                    if (newModDialog.SubMods != null && newModDialog.SubMods.Length > 0)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("Submods=");
                        for (int i = 0; i < newModDialog.SubMods.Length; i++)
                        {
                            string submodini = newModDialog.SubMods[i] + "\\" + newModDialog.SubMods[i] + ".ini";
                            stringBuilder.Append(submodini);
                            if (i < newModDialog.SubMods.Length - 1)
                                stringBuilder.Append(",");
                            Directory.CreateDirectory(Path.Combine(modFolder, newModDialog.SubMods[i]));
                            using (TextWriter writer_sm = File.CreateText(Path.Combine(modFolder, submodini)))
                            {
                                writer_sm.WriteLine(string.Format("Name={0}", newModDialog.SubMods[i]));
                                if (!string.IsNullOrEmpty(newModDialog.ModAuthor))
                                    writer_sm.WriteLine(string.Format("Author={0}", newModDialog.ModAuthor));
                                writer_sm.WriteLine(string.Format("GameVersions={0}", ImageBuilder.currentGame.GameTitle));
                                writer_sm.Flush();
                                writer_sm.Close();
                            }
                        }
                        writer.WriteLine(stringBuilder.ToString());
                    }
                    writer.Flush();
                    writer.Close();
                }
                if (newModDialog.CreateGdroot)
                    Directory.CreateDirectory(Path.Combine(modFolder, "gdroot"));
                if (newModDialog.CreateInclude)
                    Directory.CreateDirectory(Path.Combine(modFolder, "include"));
                // Import DCP
                if (!string.IsNullOrEmpty(newModDialog.DcpPath))
                {
                    if (!File.Exists(newModDialog.DcpPath))
                        MessageBox.Show(this, "Error importing DCP file: file doesn't exist", "Dreamcast Image Builder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        ZipArchive archive = ZipFile.OpenRead(newModDialog.DcpPath);
                        foreach (ZipArchiveEntry entry in archive.Entries)
                        {
                            if (entry.Name.ToUpperInvariant() == "IP.BIN")
                            {
                                entry.ExtractToFile(Path.Combine(modFolder, "IP.BIN"));
                            }
                            else
                            {
                                string fname = Path.Combine(modFolder, "gdroot", entry.FullName);
                                Directory.CreateDirectory(Path.GetDirectoryName(fname));
                                entry.ExtractToFile(fname, true);
                            }
                        }
                    }
                }
                SwitchGame();
                Process.Start("explorer.exe", @modFolder);
            }
        }

        private void buttonDeleteMod_Click(object sender, EventArgs e)
        {
            if (ImageBuilder.currentMod == null)
                return;
            if (MessageBox.Show(this, "This operation cannot be undone. Are you sure you want to delete mod '" + ImageBuilder.currentMod.Name + "'?", "Dreamcast Image Builder", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Directory.Delete(ImageBuilder.currentMod.ModPath, true);
                ImageBuilder.currentMod = null;
                SwitchGame();
            }
        }
        #endregion

        #region Binhack Settings

        private void numericUpDownBinhackBuffer_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDownBinhackBuffer.Value < 0)
                numericUpDownBinhackBuffer.Value = unchecked((uint)int.Parse(numericUpDownBinhackBuffer.Value.ToString()));
            UpdateBinhackRAM();
        }

        private void numericUpDownBinhackOffset_ValueChanged(object sender, EventArgs e)
        {
            UpdateBinhackOffset();
        }

        void UpdateBinhackRAM()
        {
            uint from = (uint)numericUpDownBinhackBuffer.Value;
            uint to = (uint)numericUpDownBinhackBuffer.Value + 0x20000;
            labelBinhackUsed.Text = "Used RAM: " + from.ToString("X8") + " - " + to.ToString("X8");
        }

        void UpdateBinhackOffset()
        {
            labelBinhackRam.Text = "RAM Address: " + (0x8C008000 + (uint)numericUpDownBinhackOffset.Value).ToString("X8");
        }

        private void buttonCopyBinhackRam_Click(object sender, EventArgs e)
        {
            Clipboard.SetText((0x8C008000 + (uint)numericUpDownBinhackOffset.Value).ToString("X8"));
        }

        private void buttonBinhackReset_Click(object sender, EventArgs e)
        {
            numericUpDownBinhackOffset.Value = 0x5500;
            numericUpDownBinhackBuffer.Value = 0x8CDE0000;
        }
        #endregion

        #region Updates

        private void CheckUpdate(bool fromUI)
        {
            buttonCheckUpdateNow.Enabled = false;
            bool update = false; // Initiate the update if true
            Task<string> t = UpdateChecker.GetStringAsync("https://gitlab.com/PiKeyAr/dreamcast-image-builder/-/raw/main/dreamcast-image-builder/version.txt");
            string latestVersion = t.Result;
            // If the check worked, set true if the values are different
            if (!string.IsNullOrEmpty(latestVersion) && latestVersion.Length < 256 && !string.IsNullOrEmpty(CurrentVersion))
            {
                if (CurrentVersion != latestVersion)
                {
                    DialogResult result = MessageBox.Show(this, "New version found! Would you like to download it?", "Dreamcast Image Builder", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    update = result == DialogResult.Yes;
                }
                else if (fromUI)
                    MessageBox.Show(this, "You are running the latest version.", "Dreamcast Image Builder", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            // If the check didn't work, show a prompt to download the update manually but only if the check was done from the UI
            else if (fromUI)
            {
                DialogResult result = MessageBox.Show(this, "Unable to retrieve version data. Would you like to download the latest version?", "Dreamcast Image Builder", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                update = result == DialogResult.Yes;
            }
            // Otherwise just fail silently
            else
                update = false;
            // Perform the update
            if (update)
            {
                using (ProgramDownloadDialog dlg = new ProgramDownloadDialog())
                {
                    // If download finished
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        // If the downloaded file exists, run the update
                        if (File.Exists(Path.Combine(Environment.CurrentDirectory, "DreamcastImageBuilder_update.exe")))
                        {
                            _ = ProcessStuff.StartProcess(Path.Combine(Environment.CurrentDirectory, "DreamcastImageBuilder_update.exe"), "", null, System.Environment.CurrentDirectory);
                            Environment.Exit(1);
                        }
                        else
                            MessageBox.Show(this, "Unable to locate the downloaded file.", "Dreamcast Image Builder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    // If download was cancelled, clean up the leftovers
                    else
                        CleanupUpdate();
                }
            }
            buttonCheckUpdateNow.Enabled = true;
        }

        private void FinalizeUpdate()
        {
            string origPath = Path.Combine(Environment.CurrentDirectory, "DreamcastImageBuilder.exe");
            ProcessStuff.TerminateProcess("DreamcastImageBuilder.exe");
            bool error = false;
            int retries = 0;
            while (true)
                try
                {
                    File.Copy(Application.ExecutablePath, origPath, true);
                    break;
                }
                catch
                {
                    if (retries > 19)
                    {
                        error = true;
                        goto end;
                    }
                    retries++;
                    Thread.Sleep(1000);
                }
            end:
            if (error)
            {
                MessageBox.Show(this, "Unable to apply the update.", "Dreamcast Image Builder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            _ = ProcessStuff.StartProcess(origPath, "", null, System.Environment.CurrentDirectory);
            Environment.Exit(1);
        }

        private void CleanupUpdate()
        {
            bool error = false;
            foreach (string leftover in Directory.GetFiles(System.Environment.CurrentDirectory, "DreamcastImageBuilder_update.*", SearchOption.TopDirectoryOnly))
            {
                int retries = 0;
                while (true)
                    try
                    {
                        File.Delete(leftover);
                        break;
                    }
                    catch
                    {
                        if (retries > 19)
                        {
                            error = true;
                            goto end;
                        }
                        retries++;
                        Thread.Sleep(1000);
                    }
            }
        end:
            if (error)
                MessageBox.Show(this, "Unable to delete temporary update files.", "Dreamcast Image Builder", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonCheckUpdateNow_Click(object sender, EventArgs e)
        {
            CheckUpdate(true);
        }

        #endregion
    }
}