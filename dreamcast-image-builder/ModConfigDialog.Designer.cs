﻿namespace DreamcastImageBuilder
{
    partial class ModConfigDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModConfigDialog));
            label1 = new System.Windows.Forms.Label();
            listViewSubMods = new System.Windows.Forms.ListView();
            columnHeaderName = new System.Windows.Forms.ColumnHeader();
            columnHeaderAuthor = new System.Windows.Forms.ColumnHeader();
            columnHeaderVersion = new System.Windows.Forms.ColumnHeader();
            textBoxSubmodDesc = new System.Windows.Forms.TextBox();
            buttonSubmodSave = new System.Windows.Forms.Button();
            buttonSubmodCancel = new System.Windows.Forms.Button();
            buttonSubmodReset = new System.Windows.Forms.Button();
            buttonSubmodSelectAll = new System.Windows.Forms.Button();
            buttonClearAll = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(10, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(245, 15);
            label1.TabIndex = 0;
            label1.Text = "Select the submods you would like to enable:";
            // 
            // listViewSubMods
            // 
            listViewSubMods.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            listViewSubMods.CheckBoxes = true;
            listViewSubMods.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { columnHeaderName, columnHeaderAuthor, columnHeaderVersion });
            listViewSubMods.FullRowSelect = true;
            listViewSubMods.Location = new System.Drawing.Point(10, 31);
            listViewSubMods.Name = "listViewSubMods";
            listViewSubMods.Size = new System.Drawing.Size(499, 301);
            listViewSubMods.TabIndex = 0;
            listViewSubMods.UseCompatibleStateImageBehavior = false;
            listViewSubMods.View = System.Windows.Forms.View.Details;
            listViewSubMods.SelectedIndexChanged += listViewSubMods_SelectedIndexChanged;
            // 
            // columnHeaderName
            // 
            columnHeaderName.Text = "Name";
            columnHeaderName.Width = 250;
            // 
            // columnHeaderAuthor
            // 
            columnHeaderAuthor.Text = "Author";
            columnHeaderAuthor.Width = 180;
            // 
            // columnHeaderVersion
            // 
            columnHeaderVersion.Text = "Version";
            columnHeaderVersion.Width = 99;
            // 
            // textBoxSubmodDesc
            // 
            textBoxSubmodDesc.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxSubmodDesc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            textBoxSubmodDesc.Location = new System.Drawing.Point(10, 338);
            textBoxSubmodDesc.Multiline = true;
            textBoxSubmodDesc.Name = "textBoxSubmodDesc";
            textBoxSubmodDesc.ReadOnly = true;
            textBoxSubmodDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            textBoxSubmodDesc.Size = new System.Drawing.Size(499, 132);
            textBoxSubmodDesc.TabIndex = 1;
            textBoxSubmodDesc.Text = "Submod description.";
            // 
            // buttonSubmodSave
            // 
            buttonSubmodSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonSubmodSave.Location = new System.Drawing.Point(359, 476);
            buttonSubmodSave.Name = "buttonSubmodSave";
            buttonSubmodSave.Size = new System.Drawing.Size(72, 28);
            buttonSubmodSave.TabIndex = 5;
            buttonSubmodSave.Text = "Save";
            buttonSubmodSave.UseVisualStyleBackColor = true;
            buttonSubmodSave.Click += buttonSubmodSave_Click;
            // 
            // buttonSubmodCancel
            // 
            buttonSubmodCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonSubmodCancel.Location = new System.Drawing.Point(437, 476);
            buttonSubmodCancel.Name = "buttonSubmodCancel";
            buttonSubmodCancel.Size = new System.Drawing.Size(72, 28);
            buttonSubmodCancel.TabIndex = 6;
            buttonSubmodCancel.Text = "Cancel";
            buttonSubmodCancel.UseVisualStyleBackColor = true;
            buttonSubmodCancel.Click += buttonSubmodCancel_Click;
            // 
            // buttonSubmodReset
            // 
            buttonSubmodReset.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonSubmodReset.Location = new System.Drawing.Point(168, 476);
            buttonSubmodReset.Name = "buttonSubmodReset";
            buttonSubmodReset.Size = new System.Drawing.Size(72, 28);
            buttonSubmodReset.TabIndex = 4;
            buttonSubmodReset.Text = "Reset";
            buttonSubmodReset.UseVisualStyleBackColor = true;
            buttonSubmodReset.Click += buttonSubmodReset_Click;
            // 
            // buttonSubmodSelectAll
            // 
            buttonSubmodSelectAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonSubmodSelectAll.Location = new System.Drawing.Point(12, 476);
            buttonSubmodSelectAll.Name = "buttonSubmodSelectAll";
            buttonSubmodSelectAll.Size = new System.Drawing.Size(72, 28);
            buttonSubmodSelectAll.TabIndex = 2;
            buttonSubmodSelectAll.Text = "Select All";
            buttonSubmodSelectAll.UseVisualStyleBackColor = true;
            buttonSubmodSelectAll.Click += buttonSubmodSelectAll_Click;
            // 
            // buttonClearAll
            // 
            buttonClearAll.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonClearAll.Location = new System.Drawing.Point(90, 476);
            buttonClearAll.Name = "buttonClearAll";
            buttonClearAll.Size = new System.Drawing.Size(72, 28);
            buttonClearAll.TabIndex = 3;
            buttonClearAll.Text = "Clear All";
            buttonClearAll.UseVisualStyleBackColor = true;
            buttonClearAll.Click += buttonClearAll_Click;
            // 
            // ModConfigDialog
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(521, 511);
            Controls.Add(buttonClearAll);
            Controls.Add(buttonSubmodSelectAll);
            Controls.Add(buttonSubmodReset);
            Controls.Add(buttonSubmodCancel);
            Controls.Add(buttonSubmodSave);
            Controls.Add(textBoxSubmodDesc);
            Controls.Add(listViewSubMods);
            Controls.Add(label1);
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Name = "ModConfigDialog";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "Configure Submods";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listViewSubMods;
        private System.Windows.Forms.TextBox textBoxSubmodDesc;
        private System.Windows.Forms.Button buttonSubmodSave;
        private System.Windows.Forms.Button buttonSubmodCancel;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderAuthor;
        private System.Windows.Forms.ColumnHeader columnHeaderVersion;
        private System.Windows.Forms.Button buttonSubmodReset;
        private System.Windows.Forms.Button buttonSubmodSelectAll;
        private System.Windows.Forms.Button buttonClearAll;
    }
}