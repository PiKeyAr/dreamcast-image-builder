﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace DreamcastImageBuilder
{
    public partial class ModConfigDialog : Form
    {
        private Mod currentMod;
        private List<Mod> subMods;
        public List<string> activeSubMods;
        
        public ModConfigDialog()
        {
            InitializeComponent();
        }

        public ModConfigDialog(Mod mod, List<string> activeSubModNames, System.Drawing.Icon icon)
        {
            InitializeComponent();
            currentMod = mod;
            Icon = icon;
            Text = "Configure Submods for " + currentMod.Name;
            subMods = new();
            activeSubMods = activeSubModNames;
            bool newsubmods = false;
            if (activeSubMods == null)
            {
                activeSubMods = new List<string>();
                newsubmods = true;
            }
            foreach (string subModRelPath in currentMod.SubMods)
            {
                string subModIniPath = Path.Combine(currentMod.ModPath, subModRelPath);
                if (File.Exists(subModIniPath))
                {
                    try
                    {
                        Mod subMod = new Mod(subModIniPath);
                        subMods.Add(subMod);
                        if (newsubmods)
                            activeSubMods.Add(subMod.Name);
                        listViewSubMods.Items.Add(new ListViewItem(new[] { subMod.Name, subMod.Author, subMod.Version }) { Checked = activeSubMods.Contains(subMod.Name) ? true : false });
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(this, "Error loading submod from: " + subModIniPath + ": " + ex.ToString(), "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void buttonSubmodSave_Click(object sender, System.EventArgs e)
        {
            // Set active submods
            activeSubMods.Clear();
            foreach (ListViewItem item in listViewSubMods.Items)
            {
                if (item.Checked)
                    activeSubMods.Add(item.Text);
            }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonSubmodCancel_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private Mod FindSubModByName(string name)
        {
            foreach (Mod mod in subMods)
            {
                if (mod.Name == name)
                    return mod;
            }
            return null;
        }


        private void listViewSubMods_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (listViewSubMods.SelectedItems.Count < 1 || listViewSubMods.SelectedIndices[0] == -1)
                textBoxSubmodDesc.Text = "Submod description.";
            else
            {
                string desc = FindSubModByName(listViewSubMods.SelectedItems[0].Text).Description;
                if (!string.IsNullOrEmpty(desc))
                    textBoxSubmodDesc.Text = desc.Replace("\\n", System.Environment.NewLine);
                else
                    textBoxSubmodDesc.Text = "No description.";
            }
        }

        private void buttonSubmodSelectAll_Click(object sender, System.EventArgs e)
        {
            foreach (ListViewItem item in listViewSubMods.Items)
            {
                item.Checked = true;
            }
        }

        private void buttonClearAll_Click(object sender, System.EventArgs e)
        {
            foreach (ListViewItem item in listViewSubMods.Items)
            {
                item.Checked = false;
            }
        }

        private void buttonSubmodReset_Click(object sender, System.EventArgs e)
        {
            foreach (ListViewItem item in listViewSubMods.Items)
            {
                item.Checked = false;
                if (currentMod.DefaultSubMods == null || (currentMod.DefaultSubMods != null && currentMod.DefaultSubMods.Contains(item.Text)))
                    item.Checked = true;
            }
        }
    }
}
