﻿namespace DreamcastImageBuilder
{
    partial class NewGameDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewGameDialog));
            buttonSave = new System.Windows.Forms.Button();
            buttonCancel = new System.Windows.Forms.Button();
            textBoxIniLocation = new System.Windows.Forms.TextBox();
            textBoxGameName = new System.Windows.Forms.TextBox();
            numericUpDownLBA = new System.Windows.Forms.NumericUpDown();
            textBoxPatches = new System.Windows.Forms.TextBox();
            labelIniLocation = new System.Windows.Forms.Label();
            labelGameName = new System.Windows.Forms.Label();
            labelGameID = new System.Windows.Forms.Label();
            labelLBA = new System.Windows.Forms.Label();
            labelPatches = new System.Windows.Forms.Label();
            groupBoxOptional = new System.Windows.Forms.GroupBox();
            pictureBoxIcon = new System.Windows.Forms.PictureBox();
            labelSortList = new System.Windows.Forms.Label();
            buttonAddSortList = new System.Windows.Forms.Button();
            buttonAddCheat = new System.Windows.Forms.Button();
            labelCheatTable = new System.Windows.Forms.Label();
            labelIcon = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)numericUpDownLBA).BeginInit();
            groupBoxOptional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxIcon).BeginInit();
            SuspendLayout();
            // 
            // buttonSave
            // 
            buttonSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonSave.Location = new System.Drawing.Point(224, 449);
            buttonSave.Name = "buttonSave";
            buttonSave.Size = new System.Drawing.Size(84, 28);
            buttonSave.TabIndex = 7;
            buttonSave.Text = "Save";
            buttonSave.UseVisualStyleBackColor = true;
            buttonSave.Click += buttonSave_Click;
            // 
            // buttonCancel
            // 
            buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCancel.Location = new System.Drawing.Point(314, 449);
            buttonCancel.Name = "buttonCancel";
            buttonCancel.Size = new System.Drawing.Size(84, 28);
            buttonCancel.TabIndex = 8;
            buttonCancel.Text = "Cancel";
            buttonCancel.UseVisualStyleBackColor = true;
            buttonCancel.Click += buttonCancel_Click;
            // 
            // textBoxIniLocation
            // 
            textBoxIniLocation.Location = new System.Drawing.Point(117, 38);
            textBoxIniLocation.Name = "textBoxIniLocation";
            textBoxIniLocation.Size = new System.Drawing.Size(183, 23);
            textBoxIniLocation.TabIndex = 0;
            // 
            // textBoxGameName
            // 
            textBoxGameName.Location = new System.Drawing.Point(117, 72);
            textBoxGameName.Name = "textBoxGameName";
            textBoxGameName.Size = new System.Drawing.Size(269, 23);
            textBoxGameName.TabIndex = 1;
            // 
            // numericUpDownLBA
            // 
            numericUpDownLBA.Location = new System.Drawing.Point(96, 141);
            numericUpDownLBA.Maximum = new decimal(new int[] { 99999, 0, 0, 0 });
            numericUpDownLBA.Name = "numericUpDownLBA";
            numericUpDownLBA.Size = new System.Drawing.Size(120, 23);
            numericUpDownLBA.TabIndex = 5;
            numericUpDownLBA.Value = new decimal(new int[] { 45000, 0, 0, 0 });
            // 
            // textBoxPatches
            // 
            textBoxPatches.Location = new System.Drawing.Point(96, 183);
            textBoxPatches.Margin = new System.Windows.Forms.Padding(3, 3, 3, 6);
            textBoxPatches.Multiline = true;
            textBoxPatches.Name = "textBoxPatches";
            textBoxPatches.Size = new System.Drawing.Size(285, 137);
            textBoxPatches.TabIndex = 6;
            // 
            // labelIniLocation
            // 
            labelIniLocation.AutoSize = true;
            labelIniLocation.Location = new System.Drawing.Point(12, 41);
            labelIniLocation.Name = "labelIniLocation";
            labelIniLocation.Size = new System.Drawing.Size(59, 15);
            labelIniLocation.TabIndex = 6;
            labelIniLocation.Text = "Brief Title:";
            // 
            // labelGameName
            // 
            labelGameName.AutoSize = true;
            labelGameName.Location = new System.Drawing.Point(12, 75);
            labelGameName.Name = "labelGameName";
            labelGameName.Size = new System.Drawing.Size(73, 15);
            labelGameName.TabIndex = 7;
            labelGameName.Text = "Display Title:";
            // 
            // labelGameID
            // 
            labelGameID.AutoSize = true;
            labelGameID.Location = new System.Drawing.Point(12, 9);
            labelGameID.Name = "labelGameID";
            labelGameID.Size = new System.Drawing.Size(58, 15);
            labelGameID.TabIndex = 8;
            labelGameID.Text = "Game ID: ";
            // 
            // labelLBA
            // 
            labelLBA.AutoSize = true;
            labelLBA.Location = new System.Drawing.Point(6, 145);
            labelLBA.Name = "labelLBA";
            labelLBA.Size = new System.Drawing.Size(59, 15);
            labelLBA.TabIndex = 11;
            labelLBA.Text = "Boot LBA:";
            // 
            // labelPatches
            // 
            labelPatches.AutoSize = true;
            labelPatches.Location = new System.Drawing.Point(6, 183);
            labelPatches.Name = "labelPatches";
            labelPatches.Size = new System.Drawing.Size(73, 15);
            labelPatches.TabIndex = 12;
            labelPatches.Text = "CDI Patches:";
            // 
            // groupBoxOptional
            // 
            groupBoxOptional.Controls.Add(pictureBoxIcon);
            groupBoxOptional.Controls.Add(labelSortList);
            groupBoxOptional.Controls.Add(buttonAddSortList);
            groupBoxOptional.Controls.Add(buttonAddCheat);
            groupBoxOptional.Controls.Add(labelCheatTable);
            groupBoxOptional.Controls.Add(labelIcon);
            groupBoxOptional.Controls.Add(numericUpDownLBA);
            groupBoxOptional.Controls.Add(labelPatches);
            groupBoxOptional.Controls.Add(textBoxPatches);
            groupBoxOptional.Controls.Add(labelLBA);
            groupBoxOptional.Location = new System.Drawing.Point(12, 101);
            groupBoxOptional.Name = "groupBoxOptional";
            groupBoxOptional.Size = new System.Drawing.Size(387, 339);
            groupBoxOptional.TabIndex = 2;
            groupBoxOptional.TabStop = false;
            groupBoxOptional.Text = "Optional Settings";
            // 
            // pictureBoxIcon
            // 
            pictureBoxIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            pictureBoxIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            pictureBoxIcon.Location = new System.Drawing.Point(47, 22);
            pictureBoxIcon.Name = "pictureBoxIcon";
            pictureBoxIcon.Size = new System.Drawing.Size(32, 32);
            pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            pictureBoxIcon.TabIndex = 22;
            pictureBoxIcon.TabStop = false;
            pictureBoxIcon.Click += pictureBoxIcon_Click;
            // 
            // labelSortList
            // 
            labelSortList.AutoSize = true;
            labelSortList.Location = new System.Drawing.Point(96, 109);
            labelSortList.Name = "labelSortList";
            labelSortList.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            labelSortList.Size = new System.Drawing.Size(84, 27);
            labelSortList.TabIndex = 18;
            labelSortList.Text = "Sort List: None";
            // 
            // buttonAddSortList
            // 
            buttonAddSortList.Location = new System.Drawing.Point(6, 103);
            buttonAddSortList.Name = "buttonAddSortList";
            buttonAddSortList.Size = new System.Drawing.Size(84, 28);
            buttonAddSortList.TabIndex = 3;
            buttonAddSortList.Text = "Sort List...";
            buttonAddSortList.UseVisualStyleBackColor = true;
            buttonAddSortList.Click += buttonAddSortList_Click;
            // 
            // buttonAddCheat
            // 
            buttonAddCheat.Location = new System.Drawing.Point(6, 69);
            buttonAddCheat.Name = "buttonAddCheat";
            buttonAddCheat.Size = new System.Drawing.Size(84, 28);
            buttonAddCheat.TabIndex = 2;
            buttonAddCheat.Text = "Cheats...";
            buttonAddCheat.UseVisualStyleBackColor = true;
            buttonAddCheat.Click += buttonAddCheat_Click;
            // 
            // labelCheatTable
            // 
            labelCheatTable.AutoSize = true;
            labelCheatTable.Location = new System.Drawing.Point(96, 75);
            labelCheatTable.Name = "labelCheatTable";
            labelCheatTable.Padding = new System.Windows.Forms.Padding(0, 0, 0, 12);
            labelCheatTable.Size = new System.Drawing.Size(103, 27);
            labelCheatTable.TabIndex = 14;
            labelCheatTable.Text = "Cheat Table: None";
            // 
            // labelIcon
            // 
            labelIcon.AutoSize = true;
            labelIcon.Location = new System.Drawing.Point(6, 30);
            labelIcon.Name = "labelIcon";
            labelIcon.Size = new System.Drawing.Size(33, 15);
            labelIcon.TabIndex = 13;
            labelIcon.Text = "Icon:";
            // 
            // NewGameDialog
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            ClientSize = new System.Drawing.Size(410, 489);
            Controls.Add(groupBoxOptional);
            Controls.Add(labelGameID);
            Controls.Add(labelGameName);
            Controls.Add(labelIniLocation);
            Controls.Add(textBoxGameName);
            Controls.Add(textBoxIniLocation);
            Controls.Add(buttonCancel);
            Controls.Add(buttonSave);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            Name = "NewGameDialog";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "Add a New Game";
            ((System.ComponentModel.ISupportInitialize)numericUpDownLBA).EndInit();
            groupBoxOptional.ResumeLayout(false);
            groupBoxOptional.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxIcon).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxIniLocation;
        private System.Windows.Forms.TextBox textBoxGameName;
        private System.Windows.Forms.NumericUpDown numericUpDownLBA;
        private System.Windows.Forms.TextBox textBoxPatches;
        private System.Windows.Forms.Label labelIniLocation;
        private System.Windows.Forms.Label labelGameName;
        private System.Windows.Forms.Label labelGameID;
        private System.Windows.Forms.Label labelLBA;
        private System.Windows.Forms.Label labelPatches;
        private System.Windows.Forms.GroupBox groupBoxOptional;
        private System.Windows.Forms.Label labelSortList;
        private System.Windows.Forms.Button buttonAddSortList;
        private System.Windows.Forms.Button buttonAddCheat;
        private System.Windows.Forms.Label labelCheatTable;
        private System.Windows.Forms.Label labelIcon;
        private System.Windows.Forms.PictureBox pictureBoxIcon;
    }
}