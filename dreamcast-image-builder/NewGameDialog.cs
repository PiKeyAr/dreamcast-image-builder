﻿using System;
using System.Globalization;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace DreamcastImageBuilder
{
    public partial class NewGameDialog : Form
    {
        private Icon GameIcon;
        private string[] SortList;
        private string[] CheatList;
        private string GameVersion;
        private int NumTracks;
        private string[] GameNames;
        private string OriginalTitle; // Null if adding a new game from GDI
        private string GameLocation;
        public string FinalName; // Game name to select on the list

        public NewGameDialog()
        {
            InitializeComponent();
        }

        public NewGameDialog(string file, bool fromGDI, string[] alreadyExistingNames)
        {
            InitializeComponent();
            GameNames = alreadyExistingNames;
            OriginalTitle = null;
            // Adding a new game from a GDI file
            if (fromGDI)
            {
                Text = "Add New Game";
                string path = Path.GetDirectoryName(file);
                NumTracks = Game.GetNumTracksFromGDI(file);
                textBoxGameName.Text = ToTitleCase(Game.GetGameName(path)).Replace('/', '-');
                GameVersion = Game.GetGameVersion(path);
                labelGameID.Text = "Game ID: " + GameVersion;
                textBoxIniLocation.Text = textBoxGameName.Text.Replace(" ", "").ToLowerInvariant();
            }
            else
            // Editing an existing game's INI file
            {
                SortList = null;
                CheatList = null;
                GameIcon = null;
                Text = "Edit Game";
                // Game folder
                GameLocation = Path.GetDirectoryName(file);
                // INI and othef filenames
                string briefName = Path.GetFileNameWithoutExtension(file);
                textBoxIniLocation.Text = briefName;
                // Game data in the INI file
                string[] gameLines = File.ReadAllLines(file);
                // Name
                textBoxGameName.Text = gameLines[0];
                OriginalTitle = gameLines[0];
                // Version
                GameVersion = gameLines[1];
                // Number of tracks
                if (gameLines.Length < 3 || !int.TryParse(gameLines[2], out NumTracks))
                    NumTracks = 3;
                // Boot LBA
                if (gameLines.Length < 4 || !int.TryParse(gameLines[3], out int lba))
                    numericUpDownLBA.Value = 11702;
                else
                    numericUpDownLBA.Value = lba;
                // Patches
                textBoxPatches.Clear();
                if (gameLines.Length > 4)
                {
                    List<string> patchLines = new();
                    for (int i = 4; i < gameLines.Length; i++)
                    {
                        if (gameLines[i].Length < 5)
                            continue;
                        patchLines.Add(gameLines[i]);
                    }
                    textBoxPatches.Lines = patchLines.ToArray();
                }
                // Sort List
                if (File.Exists(Path.Combine(GameLocation, briefName + ".str")))
                    SortList = File.ReadAllLines(Path.Combine(GameLocation, briefName + ".str"));
                // Cheat List
                if (File.Exists(Path.Combine(GameLocation, briefName + ".cht")))
                    CheatList = File.ReadAllLines(Path.Combine(GameLocation, briefName + ".cht"));
                // Icon
                if (File.Exists(Path.Combine(GameLocation, briefName + ".ico")))
                    GameIcon = new System.Drawing.Icon(Path.Combine(GameLocation, briefName + ".ico"));
            }
            labelGameID.Text = "Game ID: " + GameVersion;
            labelCheatTable.Text = "Cheat Table: " + (CheatList == null ? "None" : "Loaded");
            labelSortList.Text = "Sort List: " + (SortList == null ? "None" : "Loaded");
            pictureBoxIcon.Image = GameIcon == null ? null : GameIcon.ToBitmap();
        }

        // Convert title to capitalized
        public static string ToTitleCase(string title)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower());
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private bool ValidateName(string name)
        {
            if (GameNames == null || GameNames.Length == 0)
                return true;
            foreach (string gameName in GameNames)
                if (gameName == name)
                    return false;
            return true;
        }

        private bool ValidatePatches(out string error)
        {
            if (textBoxPatches.Lines == null || textBoxPatches.Lines.Length == 0)
            {
                error = null;
                return true;
            }
            for (int l = 0; l < textBoxPatches.Lines.Length; l++)
            {
                string line = textBoxPatches.Lines[l];
                if (line.Length < 5)
                    continue;
                try
                {
                    PatchData patch = new PatchData(line, System.Environment.CurrentDirectory);
                    if (patch.Type == PatchData.PatchType.Error)
                    {
                        error = string.Format("Unable to load patch at line {0}: {1}", l, patch.Description);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    error = string.Format("Unable to load patch at line {0}: {1}", l, ex.Message);
                    return false;
                }
            }
            error = null;
            return true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!ValidatePatches(out string patchMessage))
            {
                MessageBox.Show(this, patchMessage, "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!ValidateName(textBoxGameName.Text))
            {
                // Check if the game's display title is being edited or a new game is being added
                if ((OriginalTitle != null && textBoxGameName.Text != OriginalTitle) || OriginalTitle == null)
                {
                    MessageBox.Show(this, "A game with this display title already exists. Input a different title.", "Dreamcast Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            // Create path for the INI file
            string briefname = textBoxIniLocation.Text.ToLowerInvariant().Replace(".ini", "");
            if (GameLocation == null)
                GameLocation = Path.Combine(System.Environment.CurrentDirectory, "games", briefname);
            if (!Directory.Exists(GameLocation))
                Directory.CreateDirectory(GameLocation);
            string iniLocation = Path.Combine(GameLocation, briefname + ".ini");
            if (File.Exists(iniLocation) && OriginalTitle == null)
            {
                if (MessageBox.Show(this, "Game data for this game already exists. If you continue, it will be overwritten. You can change the brief name to have multiple data sets for a single game.\n\nOverwrite existing game data?", "Dreamcast Image Builder", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
            }
            // Write the INI file
            using (TextWriter writer = File.CreateText(iniLocation))
            {
                writer.WriteLine(textBoxGameName.Text.TrimEnd());
                writer.WriteLine(GameVersion.TrimEnd());
                writer.WriteLine(NumTracks.ToString());
                writer.WriteLine(numericUpDownLBA.Value.ToString());
                foreach (string line in textBoxPatches.Lines)
                {
                    if (line.Length < 5)
                        continue;
                    writer.WriteLine(line);
                }
            }
            // Write cheat table
            if (CheatList != null)
                File.WriteAllLines(Path.Combine(GameLocation, briefname + ".cht"), CheatList);
            // Write sort list
            if (SortList != null)
                File.WriteAllLines(Path.Combine(GameLocation, briefname + ".str"), SortList);
            // Write icon
            if (GameIcon != null)
            {
                MemoryStream iconStream = new();
                GameIcon.Save(iconStream);
                File.WriteAllBytes(Path.Combine(GameLocation, briefname + ".ico"), iconStream.ToArray());
                iconStream.Dispose();
            }
            // Finish
            DialogResult = DialogResult.OK;
            FinalName = textBoxGameName.Text.TrimEnd();
            Close();
        }

        private void buttonAddCheat_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog { Filter = "RetroArch Cheat Files|*.cht" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    CheatList = File.ReadAllLines(ofd.FileName);
                }
                else
                {
                    CheatList = null;
                }
            }
            labelSortList.Text = "Cheat Table: " + (CheatList == null ? "None" : "Loaded");
        }

        private void buttonAddSortList_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog { Filter = "Sort Files|*.str;*.txt|All Files|*.*" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    SortList = File.ReadAllLines(ofd.FileName);
                }
                else
                {
                    SortList = null;
                }
            }
            labelSortList.Text = "Sort List: " + (SortList == null ? "None" : "Loaded");
        }

        private void pictureBoxIcon_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog { Filter = "Icon and Image Files|*.ico;*.png;*.bmp;*.jpg;*.gif" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    switch (Path.GetExtension(ofd.FileName).ToLowerInvariant())
                    {
                        case ".ico":
                            GameIcon = new System.Drawing.Icon(ofd.FileName);
                            break;
                        case ".png":
                        default:
                            var bitmap = new System.Drawing.Bitmap(ofd.FileName);
                            GameIcon = System.Drawing.Icon.FromHandle(bitmap.GetHicon());
                            pictureBoxIcon.Image = GameIcon.ToBitmap();
                            break;
                    }
                }
                else
                {
                    GameIcon = null;
                    pictureBoxIcon.Image = null;
                }
            }
        }
    }
}