﻿namespace DreamcastImageBuilder
{
    partial class NewModDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            labelModName = new System.Windows.Forms.Label();
            textBoxModName = new System.Windows.Forms.TextBox();
            textBoxModDescription = new System.Windows.Forms.TextBox();
            labelModDesc = new System.Windows.Forms.Label();
            labelGame = new System.Windows.Forms.Label();
            checkBoxReplaceFiles = new System.Windows.Forms.CheckBox();
            checkBoxInclude = new System.Windows.Forms.CheckBox();
            buttonCancel = new System.Windows.Forms.Button();
            buttonCreateMod = new System.Windows.Forms.Button();
            textBoxModAuthor = new System.Windows.Forms.TextBox();
            textBoxModVersion = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            buttonCopyGameVersion = new System.Windows.Forms.Button();
            checkBoxCreateSubmods = new System.Windows.Forms.CheckBox();
            textBoxSubmods = new System.Windows.Forms.TextBox();
            labelGameID = new System.Windows.Forms.Label();
            labelDisc = new System.Windows.Forms.Label();
            buttonImport = new System.Windows.Forms.Button();
            checkBoxDiscAll = new System.Windows.Forms.CheckBox();
            checkBoxDisc1 = new System.Windows.Forms.CheckBox();
            checkBoxDisc2 = new System.Windows.Forms.CheckBox();
            checkBoxDisc3 = new System.Windows.Forms.CheckBox();
            checkBoxDisc4 = new System.Windows.Forms.CheckBox();
            SuspendLayout();
            // 
            // labelModName
            // 
            labelModName.AutoSize = true;
            labelModName.Location = new System.Drawing.Point(27, 49);
            labelModName.Name = "labelModName";
            labelModName.Size = new System.Drawing.Size(30, 21);
            labelModName.TabIndex = 1;
            labelModName.Text = "Title:";
            labelModName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            labelModName.UseCompatibleTextRendering = true;
            // 
            // textBoxModName
            // 
            textBoxModName.Location = new System.Drawing.Point(67, 46);
            textBoxModName.Name = "textBoxModName";
            textBoxModName.Size = new System.Drawing.Size(282, 23);
            textBoxModName.TabIndex = 2;
            textBoxModName.TextChanged += textBoxModName_TextChanged;
            // 
            // textBoxModDescription
            // 
            textBoxModDescription.Location = new System.Drawing.Point(12, 186);
            textBoxModDescription.Multiline = true;
            textBoxModDescription.Name = "textBoxModDescription";
            textBoxModDescription.Size = new System.Drawing.Size(337, 104);
            textBoxModDescription.TabIndex = 10;
            // 
            // labelModDesc
            // 
            labelModDesc.AutoSize = true;
            labelModDesc.Location = new System.Drawing.Point(11, 168);
            labelModDesc.Name = "labelModDesc";
            labelModDesc.Size = new System.Drawing.Size(98, 15);
            labelModDesc.TabIndex = 4;
            labelModDesc.Text = "Mod Description:";
            // 
            // labelGame
            // 
            labelGame.AutoSize = true;
            labelGame.Location = new System.Drawing.Point(18, 19);
            labelGame.Name = "labelGame";
            labelGame.Size = new System.Drawing.Size(39, 21);
            labelGame.TabIndex = 5;
            labelGame.Text = "Game:";
            labelGame.TextAlign = System.Drawing.ContentAlignment.TopRight;
            labelGame.UseCompatibleTextRendering = true;
            // 
            // checkBoxReplaceFiles
            // 
            checkBoxReplaceFiles.AutoSize = true;
            checkBoxReplaceFiles.Checked = true;
            checkBoxReplaceFiles.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxReplaceFiles.Location = new System.Drawing.Point(12, 296);
            checkBoxReplaceFiles.Name = "checkBoxReplaceFiles";
            checkBoxReplaceFiles.Size = new System.Drawing.Size(141, 19);
            checkBoxReplaceFiles.TabIndex = 11;
            checkBoxReplaceFiles.Text = "Create 'gdroot' Folder";
            checkBoxReplaceFiles.UseVisualStyleBackColor = true;
            // 
            // checkBoxInclude
            // 
            checkBoxInclude.AutoSize = true;
            checkBoxInclude.Location = new System.Drawing.Point(12, 321);
            checkBoxInclude.Name = "checkBoxInclude";
            checkBoxInclude.Size = new System.Drawing.Size(144, 19);
            checkBoxInclude.TabIndex = 12;
            checkBoxInclude.Text = "Create 'include' Folder";
            checkBoxInclude.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            buttonCancel.Location = new System.Drawing.Point(267, 464);
            buttonCancel.Name = "buttonCancel";
            buttonCancel.Size = new System.Drawing.Size(84, 28);
            buttonCancel.TabIndex = 17;
            buttonCancel.Text = "Cancel";
            buttonCancel.UseVisualStyleBackColor = true;
            buttonCancel.Click += buttonCancel_Click;
            // 
            // buttonCreateMod
            // 
            buttonCreateMod.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCreateMod.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonCreateMod.Enabled = false;
            buttonCreateMod.Location = new System.Drawing.Point(177, 464);
            buttonCreateMod.Name = "buttonCreateMod";
            buttonCreateMod.Size = new System.Drawing.Size(84, 28);
            buttonCreateMod.TabIndex = 16;
            buttonCreateMod.Text = "Save";
            buttonCreateMod.UseVisualStyleBackColor = true;
            buttonCreateMod.Click += buttonCreateMod_Click;
            // 
            // textBoxModAuthor
            // 
            textBoxModAuthor.Location = new System.Drawing.Point(67, 75);
            textBoxModAuthor.Name = "textBoxModAuthor";
            textBoxModAuthor.Size = new System.Drawing.Size(282, 23);
            textBoxModAuthor.TabIndex = 3;
            // 
            // textBoxModVersion
            // 
            textBoxModVersion.Location = new System.Drawing.Point(67, 104);
            textBoxModVersion.Name = "textBoxModVersion";
            textBoxModVersion.Size = new System.Drawing.Size(129, 23);
            textBoxModVersion.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(12, 78);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(45, 21);
            label1.TabIndex = 9;
            label1.Text = "Author:";
            label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            label1.UseCompatibleTextRendering = true;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(11, 109);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(48, 21);
            label2.TabIndex = 10;
            label2.Text = "Version:";
            label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            label2.UseCompatibleTextRendering = true;
            // 
            // buttonCopyGameVersion
            // 
            buttonCopyGameVersion.Location = new System.Drawing.Point(267, 12);
            buttonCopyGameVersion.Name = "buttonCopyGameVersion";
            buttonCopyGameVersion.Size = new System.Drawing.Size(84, 28);
            buttonCopyGameVersion.TabIndex = 1;
            buttonCopyGameVersion.Text = "Copy ID";
            buttonCopyGameVersion.UseVisualStyleBackColor = true;
            buttonCopyGameVersion.Click += buttonCopyGameVersion_Click;
            // 
            // checkBoxCreateSubmods
            // 
            checkBoxCreateSubmods.AutoSize = true;
            checkBoxCreateSubmods.Location = new System.Drawing.Point(12, 346);
            checkBoxCreateSubmods.Name = "checkBoxCreateSubmods";
            checkBoxCreateSubmods.Size = new System.Drawing.Size(274, 19);
            checkBoxCreateSubmods.TabIndex = 13;
            checkBoxCreateSubmods.Text = "Create Submods (each line is a submod name):";
            checkBoxCreateSubmods.UseVisualStyleBackColor = true;
            checkBoxCreateSubmods.CheckedChanged += checkBoxCreateSubmods_CheckedChanged;
            // 
            // textBoxSubmods
            // 
            textBoxSubmods.Enabled = false;
            textBoxSubmods.Location = new System.Drawing.Point(12, 371);
            textBoxSubmods.Multiline = true;
            textBoxSubmods.Name = "textBoxSubmods";
            textBoxSubmods.Size = new System.Drawing.Size(337, 87);
            textBoxSubmods.TabIndex = 14;
            textBoxSubmods.TextChanged += textBoxSubmods_TextChanged;
            // 
            // labelGameID
            // 
            labelGameID.AutoSize = true;
            labelGameID.Location = new System.Drawing.Point(70, 19);
            labelGameID.Name = "labelGameID";
            labelGameID.Size = new System.Drawing.Size(0, 15);
            labelGameID.TabIndex = 12;
            // 
            // labelDisc
            // 
            labelDisc.AutoSize = true;
            labelDisc.Location = new System.Drawing.Point(22, 136);
            labelDisc.Name = "labelDisc";
            labelDisc.Size = new System.Drawing.Size(35, 21);
            labelDisc.TabIndex = 15;
            labelDisc.Text = "Discs:";
            labelDisc.TextAlign = System.Drawing.ContentAlignment.TopRight;
            labelDisc.UseCompatibleTextRendering = true;
            // 
            // buttonImport
            // 
            buttonImport.Location = new System.Drawing.Point(87, 464);
            buttonImport.Name = "buttonImport";
            buttonImport.Size = new System.Drawing.Size(84, 28);
            buttonImport.TabIndex = 15;
            buttonImport.Text = "Import...";
            buttonImport.UseVisualStyleBackColor = true;
            buttonImport.Click += buttonImport_Click;
            // 
            // checkBoxDiscAll
            // 
            checkBoxDiscAll.AutoSize = true;
            checkBoxDiscAll.Location = new System.Drawing.Point(67, 138);
            checkBoxDiscAll.Name = "checkBoxDiscAll";
            checkBoxDiscAll.Size = new System.Drawing.Size(40, 19);
            checkBoxDiscAll.TabIndex = 5;
            checkBoxDiscAll.Text = "All";
            checkBoxDiscAll.UseVisualStyleBackColor = true;
            checkBoxDiscAll.CheckedChanged += checkBoxDiscAll_CheckedChanged;
            // 
            // checkBoxDisc1
            // 
            checkBoxDisc1.AutoSize = true;
            checkBoxDisc1.Location = new System.Drawing.Point(113, 138);
            checkBoxDisc1.Name = "checkBoxDisc1";
            checkBoxDisc1.Size = new System.Drawing.Size(32, 19);
            checkBoxDisc1.TabIndex = 6;
            checkBoxDisc1.Text = "1";
            checkBoxDisc1.UseVisualStyleBackColor = true;
            checkBoxDisc1.CheckedChanged += checkBoxDisc1_CheckedChanged;
            // 
            // checkBoxDisc2
            // 
            checkBoxDisc2.AutoSize = true;
            checkBoxDisc2.Location = new System.Drawing.Point(159, 138);
            checkBoxDisc2.Name = "checkBoxDisc2";
            checkBoxDisc2.Size = new System.Drawing.Size(32, 19);
            checkBoxDisc2.TabIndex = 7;
            checkBoxDisc2.Text = "2";
            checkBoxDisc2.UseVisualStyleBackColor = true;
            checkBoxDisc2.CheckedChanged += checkBoxDisc2_CheckedChanged;
            // 
            // checkBoxDisc3
            // 
            checkBoxDisc3.AutoSize = true;
            checkBoxDisc3.Location = new System.Drawing.Point(205, 138);
            checkBoxDisc3.Name = "checkBoxDisc3";
            checkBoxDisc3.Size = new System.Drawing.Size(32, 19);
            checkBoxDisc3.TabIndex = 8;
            checkBoxDisc3.Text = "3";
            checkBoxDisc3.UseVisualStyleBackColor = true;
            checkBoxDisc3.CheckedChanged += checkBoxDisc3_CheckedChanged;
            // 
            // checkBoxDisc4
            // 
            checkBoxDisc4.AutoSize = true;
            checkBoxDisc4.Location = new System.Drawing.Point(251, 138);
            checkBoxDisc4.Name = "checkBoxDisc4";
            checkBoxDisc4.Size = new System.Drawing.Size(32, 19);
            checkBoxDisc4.TabIndex = 9;
            checkBoxDisc4.Text = "4";
            checkBoxDisc4.UseVisualStyleBackColor = true;
            checkBoxDisc4.CheckedChanged += checkBoxDisc4_CheckedChanged;
            // 
            // NewModDialog
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(363, 504);
            Controls.Add(checkBoxDisc4);
            Controls.Add(checkBoxDisc3);
            Controls.Add(checkBoxDisc2);
            Controls.Add(checkBoxDisc1);
            Controls.Add(checkBoxDiscAll);
            Controls.Add(buttonImport);
            Controls.Add(labelDisc);
            Controls.Add(labelGameID);
            Controls.Add(textBoxSubmods);
            Controls.Add(checkBoxCreateSubmods);
            Controls.Add(buttonCopyGameVersion);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(textBoxModVersion);
            Controls.Add(textBoxModAuthor);
            Controls.Add(buttonCreateMod);
            Controls.Add(buttonCancel);
            Controls.Add(checkBoxInclude);
            Controls.Add(checkBoxReplaceFiles);
            Controls.Add(labelGame);
            Controls.Add(labelModDesc);
            Controls.Add(textBoxModDescription);
            Controls.Add(textBoxModName);
            Controls.Add(labelModName);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "NewModDialog";
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "New Mod";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.Label labelModName;
        private System.Windows.Forms.TextBox textBoxModName;
        private System.Windows.Forms.TextBox textBoxModDescription;
        private System.Windows.Forms.Label labelModDesc;
        private System.Windows.Forms.Label labelGame;
        private System.Windows.Forms.CheckBox checkBoxReplaceFiles;
        private System.Windows.Forms.CheckBox checkBoxInclude;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonCreateMod;
        private System.Windows.Forms.TextBox textBoxModAuthor;
        private System.Windows.Forms.TextBox textBoxModVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCopyGameVersion;
        private System.Windows.Forms.CheckBox checkBoxCreateSubmods;
        private System.Windows.Forms.TextBox textBoxSubmods;
        private System.Windows.Forms.Label labelGameID;
        private System.Windows.Forms.Label labelDisc;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.CheckBox checkBoxDiscAll;
        private System.Windows.Forms.CheckBox checkBoxDisc1;
        private System.Windows.Forms.CheckBox checkBoxDisc2;
        private System.Windows.Forms.CheckBox checkBoxDisc3;
        private System.Windows.Forms.CheckBox checkBoxDisc4;
    }
}