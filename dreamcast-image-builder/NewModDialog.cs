﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace DreamcastImageBuilder
{
    public partial class NewModDialog : Form
    {
        private readonly string GameID;

        public string ModName;
        public string ModAuthor;
        public string ModVersion;
        public string ModDescription;
        public bool CreateGdroot;
        public bool CreateInclude;
        public string[] SubMods;
        public List<int> Discs;
        public string DcpPath;

        public NewModDialog(string gameID, System.Drawing.Icon icon)
        {
            InitializeComponent();
            GameID = gameID;
            labelGameID.Text = GameID;
            if (icon != null)
                Icon = icon;
            checkBoxDiscAll.Checked = true;
        }

        public NewModDialog()
        {
            InitializeComponent();
        }

        private bool CheckValid()
        {
            if (!checkBoxDisc1.Checked && !checkBoxDisc2.Checked && !checkBoxDisc3.Checked && !checkBoxDisc4.Checked)
                return false;
            if (checkBoxCreateSubmods.Checked)
            {
                int cnt = 0;
                for (int i = 0; i < textBoxSubmods.Lines.Length; i++)
                {
                    if (!string.IsNullOrEmpty(textBoxSubmods.Lines[i]) && textBoxSubmods.Lines[i] != "\n")
                        cnt++;
                }
                if (cnt == 0)
                    return false;
            }
            return textBoxModName.Text.Length > 0;
        }

        private void buttonCopyGameVersion_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(GameID);
        }

        private void checkBoxCreateSubmods_CheckedChanged(object sender, EventArgs e)
        {
            textBoxSubmods.Enabled = checkBoxCreateSubmods.Checked;
        }

        private void textBoxModName_TextChanged(object sender, EventArgs e)
        {
            buttonCreateMod.Enabled = CheckValid();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonCreateMod_Click(object sender, EventArgs e)
        {
            ModName = textBoxModName.Text;
            ModDescription = textBoxModDescription.Text;
            ModVersion = textBoxModVersion.Text;
            ModAuthor = textBoxModAuthor.Text;
            CreateGdroot = checkBoxReplaceFiles.Checked;
            CreateInclude = checkBoxInclude.Checked;
            if (checkBoxCreateSubmods.Checked)
            {
                List<string> subs = new List<string>();
                for (int i = 0; i < textBoxSubmods.Lines.Length; i++)
                {
                    if (!string.IsNullOrEmpty(textBoxSubmods.Lines[i]) && textBoxSubmods.Lines[i].Length > 0)
                        subs.Add(textBoxSubmods.Lines[i]);
                }
                SubMods = subs.ToArray();
            }
            if (!checkBoxDiscAll.Checked)
            {
                Discs = new List<int>();
                if (checkBoxDisc1.Checked)
                    Discs.Add(1);
                if (checkBoxDisc2.Checked)
                    Discs.Add(2);
                if (checkBoxDisc3.Checked)
                    Discs.Add(3);
                if (checkBoxDisc4.Checked)
                    Discs.Add(4);
            }
            else
                Discs = null;
            Close();
        }

        private void textBoxSubmods_TextChanged(object sender, EventArgs e)
        {
            buttonCreateMod.Enabled = CheckValid();
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            DcpPath = null;
            using (OpenFileDialog openFileDialog = new OpenFileDialog { AutoUpgradeEnabled = true, Filter = "Universal Dreamcast Patcher|*.dcp", Multiselect = false, Title = "Import a DCP file" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    DcpPath = openFileDialog.FileName;
                }
                if (string.IsNullOrEmpty(textBoxModDescription.Text))
                    textBoxModDescription.Text = "Imported from: " + Path.GetFileName(openFileDialog.FileName);
                if (string.IsNullOrEmpty(textBoxModName.Text))
                    textBoxModName.Text = Path.GetFileNameWithoutExtension(openFileDialog.FileName);
            }
        }

        private void checkBoxDiscAll_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxDiscAll.Checked)
            {
                checkBoxDisc1.Checked = checkBoxDisc2.Checked = checkBoxDisc3.Checked = checkBoxDisc4.Checked = true;
                checkBoxDisc1.Enabled = checkBoxDisc2.Enabled = checkBoxDisc3.Enabled = checkBoxDisc4.Enabled = false;
            }
            else
            {
                checkBoxDisc1.Enabled = checkBoxDisc2.Enabled = checkBoxDisc3.Enabled = checkBoxDisc4.Enabled = true;
                buttonCreateMod.Enabled = CheckValid();
            }
        }

        private void checkBoxDisc1_CheckedChanged(object sender, EventArgs e)
        {
            buttonCreateMod.Enabled = CheckValid();
        }

        private void checkBoxDisc2_CheckedChanged(object sender, EventArgs e)
        {
            buttonCreateMod.Enabled = CheckValid();
        }

        private void checkBoxDisc3_CheckedChanged(object sender, EventArgs e)
        {
            buttonCreateMod.Enabled = CheckValid();
        }

        private void checkBoxDisc4_CheckedChanged(object sender, EventArgs e)
        {
            buttonCreateMod.Enabled = CheckValid();
        }
    }
}
