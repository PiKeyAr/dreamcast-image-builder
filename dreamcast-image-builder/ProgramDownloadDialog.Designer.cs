﻿namespace DreamcastImageBuilder
{
    partial class ProgramDownloadDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            labelDownloading = new System.Windows.Forms.Label();
            progressBar1 = new System.Windows.Forms.ProgressBar();
            buttonCancel = new System.Windows.Forms.Button();
            timerDownload = new System.Windows.Forms.Timer(components);
            SuspendLayout();
            // 
            // labelDownloading
            // 
            labelDownloading.AutoSize = true;
            labelDownloading.Location = new System.Drawing.Point(12, 9);
            labelDownloading.Name = "labelDownloading";
            labelDownloading.Padding = new System.Windows.Forms.Padding(0, 0, 0, 4);
            labelDownloading.Size = new System.Drawing.Size(128, 19);
            labelDownloading.TabIndex = 0;
            labelDownloading.Text = "Downloading Update...";
            // 
            // progressBar1
            // 
            progressBar1.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            progressBar1.Location = new System.Drawing.Point(12, 31);
            progressBar1.Name = "progressBar1";
            progressBar1.Size = new System.Drawing.Size(348, 23);
            progressBar1.TabIndex = 1;
            // 
            // buttonCancel
            // 
            buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonCancel.Location = new System.Drawing.Point(276, 66);
            buttonCancel.Name = "buttonCancel";
            buttonCancel.Size = new System.Drawing.Size(84, 28);
            buttonCancel.TabIndex = 2;
            buttonCancel.Text = "Cancel";
            buttonCancel.UseVisualStyleBackColor = true;
            buttonCancel.Click += buttonCancel_Click;
            // 
            // timerDownload
            // 
            timerDownload.Enabled = true;
            timerDownload.Tick += timerDownload_Tick;
            // 
            // ProgramDownloadDialog
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(372, 106);
            Controls.Add(buttonCancel);
            Controls.Add(progressBar1);
            Controls.Add(labelDownloading);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "ProgramDownloadDialog";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "Download Progress";
            FormClosing += ProgramDownloadDialog_FormClosing;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label labelDownloading;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Timer timerDownload;
    }
}