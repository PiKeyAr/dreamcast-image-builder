﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DreamcastImageBuilder
{
    public partial class ProgramDownloadDialog : Form
    {
        static string DownloadMessage;
        CancellationTokenSource CancelTokenSource;
        CancellationToken CancelToken;
        static double DownloadProgress;
        static bool CanClose;
        static bool Hold;

        public ProgramDownloadDialog()
        {
            InitializeComponent();
            Hold = CanClose = false;
            DownloadProgress = 0;
            DownloadMessage = "Downloading Update...";
            CancelTokenSource = new CancellationTokenSource();
            CancelToken = CancelTokenSource.Token;
            Task Download = Task.Run(() => Downloader(CancelToken));
            timerDownload.Start();
        }

        public static async Task Downloader(CancellationToken cancellationToken)
        {
            Uri DownloadUri = new Uri("https://gitlab.com/PiKeyAr/dreamcast-image-builder/-/raw/main/release/DreamcastImageBuilder.exe");
            MemoryStream downloadStream = new MemoryStream();
            await ClassesHelpers.DownloadHelper.DownloadFileAsync(DownloadUri, downloadStream, YourProgress, cancellationToken);
            File.WriteAllBytes(Path.Combine(System.Environment.CurrentDirectory, "DreamcastImageBuilder_update.exe"), downloadStream.ToArray());
            DownloadMessage = "Finished!";
            CanClose = true;
        }

        private static void YourProgress(long CurrentBytes, long totalBytes)
        {
            DownloadMessage = string.Format("Downloading Update: {0}/{1} KB...", (CurrentBytes / 1024).ToString(), (totalBytes / 1024).ToString());
            DownloadProgress = (float)(CurrentBytes) / (float)(totalBytes) * 100.0f;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Hold = true;
            DialogResult result = MessageBox.Show(this, "Are you sure you want to cancel the update?", "Dreamcast Image Builder", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                CancelTokenSource.Cancel();
                DialogResult = DialogResult.Cancel;
                CanClose = true;
            }
            Hold = false;
        }

        private void timerDownload_Tick(object sender, EventArgs e)
        {
            if (!Hold && CanClose)
            {
                timerDownload.Stop();
                Close();
            }
            labelDownloading.Text = DownloadMessage;
            progressBar1.Value = (int)DownloadProgress;
            if (CanClose)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void ProgramDownloadDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            CancelTokenSource.Dispose();
        }
    }
}
