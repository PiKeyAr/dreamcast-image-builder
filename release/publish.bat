@echo off
taskkill /t /f /im DreamcastImageBuilder.exe /FI "STATUS ne UNKNOWN"
timeout /t 1 /nobreak
del DreamcastImageBuilder.exe
cd..
dotnet publish -p:PublishProfile=FolderProfile
cd release
DreamcastImageBuilder